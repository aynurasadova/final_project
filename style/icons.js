import homeIcon from "../assets/icons/whiteHomeIcon.png";
import homeIconOnPress from "../assets/icons/homeIconOnPress.png";
import bookIcon from "../assets/icons/whiteBookIcon.png";
import bookIconOnPress from "../assets/icons/bookIconOnPress.png";
import blogIcon from "../assets/icons/whiteBlogIcon.png";
import blogIconOnPress from "../assets/icons/blogIconOnPress.png";
import userIcon from "../assets/icons/userIcon.png";
import userIconOnPress from "../assets/icons/userIconOnPress.png";
import favoritesIconOnPress from "../assets/icons/favoritesIconOnPress.png";
import favoritesIcon from "../assets/icons/favoritesIcon.png";
import starIconWhite from "../assets/icons/starIconWhite.png";
import starIconChoosen from "../assets/icons/starIconChoosen.png";
import searchIcon from "../assets/icons/search.png";
import back from "../assets/icons/back.png";
import heartIconGray from "../assets/icons/heartIconGray.png";
import heartIconChoosen from "../assets/icons/heartIconChoosen.png";
import heartIconForFavorites from "../assets/icons/heartIconGray.png";
import heartIconForFavoritesOnChoosen from "../assets/icons/heartIconChoosen.png";
import backArrow from "../assets/icons/backArrow.png";
import addReminder from "../assets/icons/addReminder.png";
import nextArrow from "../assets/icons/next.png";
import appLogo from "../assets/icons/logo.png";
import searchIconWhite from "../assets/icons/searchIcon.png";
import searchIconOnPress from "../assets/icons/searchIconOnPress.png";
import openEye from "../assets/icons/openEye.png";
import closeEye from "../assets/icons/closeEye.png";

export const ICONS = {
  appLogo,
  homeIcon,
  homeIconOnPress,
  bookIcon,
  bookIconOnPress,
  blogIcon,
  blogIconOnPress,
  userIcon,
  userIconOnPress,
  favoritesIcon,
  favoritesIconOnPress,
  starIconWhite,
  starIconChoosen,
  searchIcon,
  back,
  backArrow,
  heartIconGray,
  heartIconChoosen,
  heartIconForFavorites,
  heartIconForFavoritesOnChoosen,
  addReminder,
  nextArrow,
  searchIconWhite,
  searchIconOnPress,
  openEye,
  closeEye,
};
