import * as Font from "expo-font";
import RobotoBold from "../assets/fonts/Roboto-Bold.ttf";
import RobotoLight from "../assets/fonts/Roboto-Light.ttf";
import RobotoMedium from "../assets/fonts/Roboto-Medium.ttf";
import RobotoRegular from "../assets/fonts/Roboto-Regular.ttf";

export const loadFonts = () => {
    return Font.loadAsync({
        RobotoBold,
        RobotoLight,
        RobotoMedium,
        RobotoRegular,
    })
}

