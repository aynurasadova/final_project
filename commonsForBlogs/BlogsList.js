import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  ActivityIndicator,
} from "react-native";
import { connect } from "react-redux";
import { useNavigation } from "@react-navigation/native";

import { COLORS } from "../style/colors";
import { DefText } from "../components";
import { UserNames } from "../commons/UserNames";
import {
  selectSingleByID,
  addToFavBlog,
  toggleLikes,
  selectBlogLikes,
  selectAddedToLaterBlogByID,
  addAddedToLater,
} from "../store/blogs";
import { BTNs } from "./BTNs";
import { selectAuthUserID } from "../store/auth";

const mapStateToProps = (state, { blog }) => ({
  singleBlog: selectSingleByID(state, blog?.id),
  likes: selectBlogLikes(state, blog?.id),
  singleItemAddedToLaterStatus: selectAddedToLaterBlogByID(state, blog?.id),
  userID: selectAuthUserID(state),
});

export const BlogsList = connect(mapStateToProps, {
  addToFavBlog,
  toggleLikes,
  addAddedToLater,
})(
  ({
    blog,
    contentWidth,
    users,
    singleBlog,
    addToFavBlog,
    toggleLikes,
    likes,
    userID,
    addAddedToLater,
    singleItemAddedToLaterStatus,
  }) => {
    const navigation = useNavigation();
    const isLiked = !!likes[userID];
    const likesCount = Object.keys(likes).length;
    const handleLikePress = () => {
      toggleLikes(userID, blog?.id, isLiked);
      addToFavBlog(userID, blog?.id, isLiked);
    };
    const likesCounter = (likeLength) => {
      if (likeLength < 1000) {
        return `${likeLength} `;
      } else if (likeLength >= 1000 && likeLength < 1000000) {
        return `${likeLength / 1000}k `;
      } else if (likeLength >= 1000000) {
        return `${likeLength / 1000000}M `;
      }
    };
    const handleAddedToLaterPress = () => {
      addAddedToLater(userID, blog?.id, singleItemAddedToLaterStatus);
    };
    const [isProfImageLoading, setIsProfImageLoading] = useState(false);
    const [isImageLoading, setIsImageLoading] = useState(false);

    return (
      <View
        style={{
          backgroundColor: "rgba(0,0,0,.14)",
          width: contentWidth ? `${contentWidth / 2 - 2 }%` : 165,
          marginRight: 30,
          padding: 5,
          borderWidth: 0.5,
          borderColor: COLORS.MAIN_LIGHT,
          borderRadius: 10,
          maxHeight: 155,
        }}
      >
        <TouchableOpacity style={styles.authorInfo}>
        {!isProfImageLoading && <ActivityIndicator style = {styles.activityIndicatorProf} size = {24} color = "black" />}

          <Image
            style={[styles.profilePhoto, {width: isProfImageLoading ? 30 : 1}]}
            source={{ uri: users[singleBlog?.author]?.profilePhoto }}
            resizeMode="cover"
            onLoadEnd = {() =>setIsProfImageLoading(true)}
          />
          <UserNames
            firstName={users[singleBlog?.author]?.userFirstName}
            lastName={users[singleBlog?.author]?.userLastName}
            spaceFromLeft={5}
            fontSize={13}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() =>
            navigation.navigate("SingleBlogScreen", { blogID: blog.id })
          }
          style={styles.singleBlog}
        >
          {!isImageLoading && <ActivityIndicator style = {styles.activityIndicator} size = {24} color = "black" />}

          <Image
            resizeMode="cover"
            source={{ uri: singleBlog?.blogPhoto }}
            style={[styles.blogPhoto, {height: isImageLoading ? "50%" : 1}]}
            onLoadEnd = {() =>setIsImageLoading(true)}
          />
          <DefText weight="light" style={styles.title} numberOfLines={1}>
            {singleBlog?.title}
          </DefText>
          <View style={styles.actionsWrapper}>
            <View style={styles.likes}>
              <BTNs
                type="like"
                status={isLiked}
                onPress={handleLikePress}
                size={16}
              />
              <DefText weight="light" style={styles.likesText}>
                {likesCounter(likesCount)} likes
              </DefText>
            </View>
            <View>
              <BTNs
                type="bookmark"
                size={16}
                status={singleItemAddedToLaterStatus}
                onPress={handleAddedToLaterPress}
              />
            </View>
          </View>
        </TouchableOpacity> 
      </View>    
    )
});


const styles = StyleSheet.create({
  authorInfo: {
    flexDirection: "row",
    alignItems: "center",
    overflow: "hidden",
  },
  activityIndicatorProf: {
    width: 30,
    height: 30,
    borderRadius: 15,
    backgroundColor: "#d4d4d4"
  },
  profilePhoto: {
    height: 30,
    borderRadius: 15,
  },
  singleBlog: {
    width: "100%",
    height: "82%",
    padding: 10,
    overflow: "hidden",
  },
  activityIndicator: {
    width: "100%",
    height: "50%",
    alignSelf: "center",
    borderRadius: 5,
    backgroundColor: "#d4d4d4",
  },
  blogPhoto: {
    width: "100%",
    alignSelf: "center",
    borderRadius: 5,
  },
  title: {
    color: COLORS.MAIN_LIGHT,
    paddingVertical: 3,
    fontSize: 13,
  },
  content: {
    color: COLORS.LINE_COLOR,
  },
  actionsWrapper: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  likes: {
    flexDirection: "row",
    alignItems: "center",
  },
  likesText: {
    marginLeft: 5,
    fontSize: 10,
  },
});
