import React from 'react';
import { StyleSheet, FlatList } from 'react-native';
import { connect } from 'react-redux';

import { BlogsList } from './BlogsList';
import { selectAppUsers } from '../store/users';
import { AllBlogsList } from './AllBlogsList';

const mapStateToProps = (state) => ({
    users: selectAppUsers(state),
});


export const FlatListForBlogs = connect(mapStateToProps)(({data, contentWidth, mainBlogScreen = true, numColumns, users, horizontalStatus = false }) => {

    return(<>
            <FlatList 
                scrollEnabled = {true}
                contentContainerStyle = {[styles.blogsColumn,{width: `${contentWidth}%`}]}
                horizontal = {horizontalStatus} 
                data={data}
                numColumns = {mainBlogScreen ? 1 : numColumns}
                showsVerticalScrollIndicator = {false}
                renderItem = {({item}) => (
                    mainBlogScreen 
                    ? 
                    <AllBlogsList blog = {item} contentWidth = {contentWidth} users = {users}/>
                    :
                    <BlogsList blog = {item} contentWidth = {contentWidth} users = {users} />
                )}
            />
        </>
    )
});

const styles = StyleSheet.create({ 
    blogsColumn: {
        justifyContent: "space-between",
        paddingBottom: 20,
    },
});
