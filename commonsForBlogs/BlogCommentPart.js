import React, { useEffect, useState } from 'react';
import { 
    StyleSheet, 
    View, 
    Dimensions,
    KeyboardAvoidingView,
    Image,
    ScrollView,
    TextInput,
    Platform,
    TouchableOpacity,
    Alert,
} from 'react-native';
import { connect } from 'react-redux';
import { Ionicons, MaterialIcons } from '@expo/vector-icons';

import { COLORS } from '../style/colors';
import { DefText } from '../components';
import { 
    addBlogComment,
    selectBlogCommentReplies,
} from '../store/blogs';
import { GLOBAL_STYLES } from '../utils/GLOBAL_STYLES';
import { ButtonwithLabel } from '../commons';
import { BTNs } from './BTNs';
import { selectAuthUserID, selectProfilePhoto } from '../store/auth';
import { selectAppUsers } from '../store/users';
import  {BlogCommentReplyModal} from "./BlogCommentReplyModal";
import { UserNames } from '../commons/UserNames';
import { BlogCommentLikesAndDislikes } from './BlogCommentLikesAndDislikes';
import { ReplyShowing } from './ReplyShowing';
import { DeleteBlogComment } from './DeleteBlogComment';

const dimensionOfPhoto =
  Dimensions.get("screen").width - GLOBAL_STYLES.PADDING * 2;

const mapStateToProps = (state, { route }) => ({
    users: selectAppUsers(state),
    userID: selectAuthUserID(state),
    profilePhoto: selectProfilePhoto(state),
    allCommentReplies: selectBlogCommentReplies(state),
});

export const BlogCommentPart = connect(mapStateToProps, {
    addBlogComment,
}
)(({
        navigation, 
        blogID,
        users,
        userID,
        profilePhoto,
        addBlogComment,
        comments,
        allCommentReplies,
    }) => {

    const commentsCount = Object.keys(comments).length;
    const [comment, setComment] = useState("");
    const commentsArray = Object.keys(comments).map((key) => ({
        id: key,
        ...comments[key],
    }))

    const submitComment = () => {
        if(comment.trim() !== "") {
            addBlogComment(userID, blogID, comment.trim());
            setComment("")
        }
    };

    const [showModal, setShowModal] = useState(false);
    const [commentID, setCommentID] = useState(null);
    const [singleComment, setSingleComment] = useState(null);

    return(
        <View style = {styles.container}>
            <ScrollView style = {styles.inner}>
                <KeyboardAvoidingView behavior = {Platform.OS && "padding"} style = {{flex: 1}}>
                    {!showModal && 
                    <DefText style = {styles.commentText} weight = "light" >
                        {commentsCount} comments
                    </DefText>}
                    { !showModal && <View style = {styles.commentInputFieldWrapper}>
                        <View style = {{flexDirection: "row"}}>
                            <Image 
                                source = {{uri: profilePhoto}}
                                style = {{width: 40, height: 40, borderRadius: 20}}
                            />
                            <TextInput 
                                multiline = {true}
                                placeholder = "Add a public comment ..."
                                placeholderTextColor = {COLORS.LINE_COLOR}
                                value = {comment}
                                onChangeText = {setComment}
                                style = {styles.commentInputField}
                            />
                        </View>
                        <TouchableOpacity onPress = {submitComment}>
                            <Ionicons name="md-send" size={30} color={COLORS.MAIN_LIGHT} />
                        </TouchableOpacity>
                    </View>}
                    {
                        !showModal && commentsArray.slice(0).reverse().map(item => (
                            <TouchableOpacity onPress = {() => {
                                setShowModal(true)
                                setCommentID(item.id)
                                setSingleComment(item)
                                }} style = {styles.commentInfoWrapper} key = {item.id}>
                                <View style = {styles.commentAuthor}>
                                    <Image 
                                        source = {{uri: users[item.author]?.profilePhoto}}
                                        style = {{width: 40, height: 40, borderRadius: 20}}
                                    />
                                    <View style = {styles.commentInfo}>
                                        <UserNames 
                                            firstName = {users[item.author]?.userFirstName}
                                            lastName = {users[item.author]?.userLastName}
                                        />
                                        <DefText style = {styles.commentItself} >{item.comment}</DefText>
                                        <BlogCommentLikesAndDislikes 
                                            userID = {userID} 
                                            singleComment = {item} 
                                            blogID = {blogID} 
                                            commentID = {item.id}
                                            onPressReplyIcon = {() => {
                                                setShowModal(true)
                                                setCommentID(item.id)
                                                setSingleComment(item)
                                            }}
                                        />
                                        <ReplyShowing commentID = {item.id}/>
                                    </View>
                                    {
                                        item.author === userID && <DeleteBlogComment blogID = {blogID} userID = {userID} commentID = {item.id} />
                                    }
                                    
                                </View>
                                
                            </TouchableOpacity>
                        ))
                    }
                    {
                        showModal && 
                        <BlogCommentReplyModal 
                            commentID = {commentID} 
                            blogID = {blogID}
                            singleComment = {singleComment}
                            onPressGoBack = {() => {
                                setShowModal(false)
                                setCommentID(null)
                                setComment(null)
                            }} 
                        />
                    }
                </KeyboardAvoidingView>
            </ScrollView>          
        </View>
    )
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.BG_PRIMARY,
    },
    commentText: {
        fontSize: 18,
        padding: GLOBAL_STYLES.PADDING / 2 + 5,
    },
    commentInputFieldWrapper: {
        flexDirection: "row",
        justifyContent: "space-between",
        paddingLeft: GLOBAL_STYLES.PADDING / 2,
        paddingRight: GLOBAL_STYLES.PADDING / 2,
        marginBottom: 20,
    },
    commentInfoWrapper: {
        paddingTop: 15,
        paddingBottom: 10,
        paddingLeft: GLOBAL_STYLES.PADDING / 2,
        paddingRight: GLOBAL_STYLES.PADDING / 2,
        paddingLeft: 20, 
        borderTopWidth: 0.3, 
        borderColor: COLORS.LINE_COLOR,
    },
    commentAuthor: {
        flexDirection: "row"
    },
    commentInputField: {
        fontSize: 15,
        marginLeft: 10,
        maxWidth: Dimensions.get("screen").width - 120,
        color: COLORS.LINE_COLOR,
    },
    commentInfo: {
        marginLeft: 10,
    },
    commentItself: {
        maxWidth: Dimensions.get("screen").width - 120,
        marginTop: 5,
    },
});