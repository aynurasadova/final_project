import React from 'react';
import { View, TouchableOpacity, StyleSheet} from 'react-native';

import { AntDesign, MaterialIcons } from '@expo/vector-icons';
import { DefText } from '../components';
import { COLORS } from '../style/colors';
import fbApp from '../utils/firebaseInit';
import { likesCounter } from '../utils/LikesCounter';
import { ConvertingObjToArr } from '../utils/ConvertingObjToArr';

export const BlogCommentReplyLikeAndDislike = ({ singleReply, commentID, replyID, userID}) => {

    const countOfLikes = ConvertingObjToArr(singleReply?.likes || {}).length || "";
      const changeLike = () => {
        const reference = fbApp.db.ref(`blogCommentsReply/${commentID}/replies/${replyID}/likes/${userID}`);
        !!singleReply?.likes
          ? !!singleReply?.likes[userID]
            ? reference.remove()
            : reference.update({
                like: true,
              })
          : reference.update({
              like: true,
            });
      };
      const changeDislike = () => {
        const reference = fbApp.db.ref(`blogCommentsReply/${commentID}/replies/${replyID}/dislikes/${userID}`);
        !!singleReply?.dislikes
          ? !!singleReply?.dislikes[userID]
            ? reference.remove()
            : reference.update({
                dislike: true,
              })
          : reference.update({
              dislike: true,
            });
      };
  
      const onPressLikeHandler = () => {
        changeLike(commentID);
        if (!!singleReply?.dislikes)
          if (!!singleReply?.dislikes[userID]?.dislike) changeDislike(commentID);
      };
  
      const onPressDislikeHandler = () => {
        changeDislike(commentID);
        if (!!singleReply?.likes) if (!!singleReply?.likes[userID]?.like) changeLike(commentID);
      };
  
  
    return (
        <View style={styles.iconsWrapper}>
            <TouchableOpacity style={styles.likeIcon} onPress={onPressLikeHandler}>
                <AntDesign
                    name="like1"
                    size={16}
                    color={
                    !!singleReply?.likes ? (!!singleReply?.likes[userID]?.like ? "#3883c4" : COLORS.TEXT_PRIMARY) : COLORS.TEXT_PRIMARY
                    }
                />
            <DefText style = {{marginLeft: 10}}>{likesCounter(countOfLikes)}</DefText>
            </TouchableOpacity>
            
            <TouchableOpacity onPress={onPressDislikeHandler}>
                <AntDesign
                    name="dislike1"
                    size={16}
                    color={
                    !!singleReply?.dislikes
                    ? !!singleReply?.dislikes[userID]?.dislike
                    ? "#3883c4"
                    : COLORS.TEXT_PRIMARY
                    : COLORS.TEXT_PRIMARY
                    }
                />
            </TouchableOpacity>
        </View>
    )
};

const styles = StyleSheet.create({
    iconsWrapper: {
        flexDirection: "row",
        marginTop: 10,
        alignItems: "center",
      },
      likeIcon: {
        flexDirection: "row",
        width: 80,
        maxWidth: 100
      },
})