import React, { useEffect, useState } from 'react';
import { 
    StyleSheet, 
    View, 
    Dimensions,
    Button,
    KeyboardAvoidingView,
    Image,
    ScrollView,
    TextInput,
    FlatList,
    Platform,
    TouchableOpacity,
    Alert,
    ActivityIndicator,
} from 'react-native';
import { connect } from 'react-redux';
import { LinearGradient } from "expo-linear-gradient";
import { Ionicons } from '@expo/vector-icons';

import { COLORS } from '../style/colors';
import { DefText } from '../components';
import { 
    selectSingleByID, 
    selectAddedToLaterBlogByID,
    toggleLikes,
    selectBlogLikes,
    addToFavBlog,
    addAddedToLater,
    addBlogComment,
    selectBlogComments,
    deleteBlog,
    selectBlogCommentReplies,
} from '../store/blogs';
import { GLOBAL_STYLES } from '../utils/GLOBAL_STYLES';
import { ButtonwithLabel } from '../commons';
import { BTNs } from './BTNs';
import { selectAuthUserID, selectProfilePhoto } from '../store/auth';
import { selectAppUsers } from '../store/users';

import { UserNames } from '../commons/UserNames';
import { BlogCommentPart } from './BlogCommentPart';

const dimensionOfPhoto =
  Dimensions.get("screen").width - GLOBAL_STYLES.PADDING * 2;

const mapStateToProps = (state, { route }) => ({
    users: selectAppUsers(state),
    userID: selectAuthUserID(state),
    singleBlog: selectSingleByID(state, route.params?.blogID),
    addedToLaterStatus: selectAddedToLaterBlogByID(state, route.params?.blogID),
    likes: selectBlogLikes(state, route.params?.blogID),
    comments: selectBlogComments(state, route.params?.blogID),
});

export const SingleBlogScreen = connect(mapStateToProps, {
    toggleLikes, 
    addToFavBlog, 
    addAddedToLater,
    deleteBlog,
}
)(({
        navigation, 
        route: {params: { blogID }}, 
        users,
        userID,
        singleBlog, 
        toggleLikes,
        likes,
        addToFavBlog,
        addAddedToLater,
        addedToLaterStatus,
        comments,
        deleteBlog,
    }) => {

    const isLiked = !!likes[userID];
    
    const handleAddedToLaterPress = () => {
        addAddedToLater( userID, blogID, addedToLaterStatus);
    };

    const handleLikePress = () => {
        toggleLikes( userID, blogID, isLiked);
        addToFavBlog(userID, blogID, isLiked);
    };

    const goBackHandler = () => {
        navigation.goBack();
    };

    const handleDeletePress = () => {
        Alert.alert("Do you really want to delete your post", "If yes, to continue click yes", [{
            text: "Cancel",
            style: "cancel",
        },{
            text: "Yes",
            onPress: () => {
                navigation.goBack();
                deleteBlog(userID, blogID, isLiked, addedToLaterStatus)
            }
        }])
    };
    
    const [isImageLoading, setIsImageLoading] = useState(false);

    const validationForUser = users[singleBlog?.author]?.userID === userID;

    return(
        <ScrollView style = {styles.container}>
            <KeyboardAvoidingView behavior="padding" style={{ flex: 1 }}>
                {!isImageLoading && <ActivityIndicator style = {styles.activityIndicator} size = {24} color = "black" />}
                <Image 
                    source = {{uri: singleBlog?.blogPhoto }} 
                    style = {[styles.headerPhoto, {            
                        width: Dimensions.get("screen").width,
                        height: isImageLoading ? Dimensions.get("screen").width / 1.4 : 1,
                    }]}
                    resizeMode = "cover"
                    onLoadEnd = {() =>setIsImageLoading(true)}
                />
                <View style = {styles.btnsWrapper}>
                    <LinearGradient
                        colors = {["rgba(0,0,0,0.8)", "rgba(0,0,0,0.5)", "transparent"]} 
                        style = {{...StyleSheet.absoluteFill}}
                    />
                    <BTNs type = "back" onPress = {goBackHandler}/>
                    <BTNs 
                        type = "bookmark"
                        status = {!!addedToLaterStatus}
                        onPress = {handleAddedToLaterPress}
                    />
                </View>
                {validationForUser && <View style = {styles.deleteIcon}>
                    <BTNs 
                        type = "delete"
                        size = {35}
                        status = {isLiked}
                        onPress = {handleDeletePress}
                    />
                </View>}
                <View style = {styles.likeAndFav}>
                    <BTNs 
                        type = "like"
                        size = {35}
                        status = {isLiked}
                        onPress = {handleLikePress}
                    />
                </View>
                <View  style = {styles.authorInfo}>
                    <LinearGradient 
                        colors = {["transparent","rgba(0,0,0,0.8)"]}
                        style = {{...StyleSheet.absoluteFill}}
                    />
                    <Image 
                        source = {{uri: users[singleBlog?.author]?.profilePhoto}}
                        style = {{width: 40, height: 40, borderRadius: 20, backgroundColor: "black"}}
                    />
                    <DefText weight = "regular" style = {styles.by}>By</DefText>
                    <UserNames 
                        fontSize = {16}
                        weight = "regular"
                        firstName = {validationForUser ? "me" : users[singleBlog?.author]?.userFirstName} 
                        lastName = {validationForUser ? "" : users[singleBlog?.author]?.userLastName}
                        spaceFromLeft = {5}
                        authorNameColor = "#ffffff"
                    />
                </View>
                <View style = {styles.titleContainer}>
                    <DefText weight = "medium" style = {styles.title}>{singleBlog?.title}</DefText>
                </View>
                <DefText style = {styles.content}>{singleBlog?.content}</DefText>
                
                <View style = {styles.lineX} />    
                <BlogCommentPart comments = {comments} blogID = {blogID} />   
            </KeyboardAvoidingView>
        </ScrollView>
    )
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.BG_PRIMARY,
    },
    inner: {
    },
    btnsWrapper: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignSelf: "center",
        width: Dimensions.get("screen").width,
        paddingHorizontal: GLOBAL_STYLES.PADDING,
        paddingVertical: GLOBAL_STYLES.PADDING,
        position: "absolute",
    },
    activityIndicator: {
        width: Dimensions.get("screen").width,
        height: Dimensions.get("screen").width / 1.4,
        overflow: "hidden",
        borderBottomLeftRadius: 35,
        borderBottomRightRadius: 35,
        backgroundColor: "#d4d4d4",  
    },
    headerPhoto: {
        overflow: "hidden",
        borderBottomLeftRadius: 35,
        borderBottomRightRadius: 35,
    }, 
    likeAndFav: {
        position: "absolute",
        backgroundColor: "#C4C4C4",
        borderRadius: 10,
        width: 56,
        height: 56,
        justifyContent: "center",
        alignItems: "center",
        marginTop: Dimensions.get("screen").width / 1.4 - 28,
        right: 0,
        marginRight: GLOBAL_STYLES.PADDING - 10,
    },
    deleteIcon: {
        position: "absolute",
        backgroundColor: "#C4C4C4",
        borderRadius: 10,
        width: 56,
        height: 56,
        justifyContent: "center",
        alignItems: "center",
        marginTop: Dimensions.get("screen").width / 1.4 - 28,
        right: 0,
        marginRight: GLOBAL_STYLES.PADDING * 3.1,
        zIndex: 250
    },
    authorInfo: {
        flexDirection: "row",
        alignItems: "center",
        position: "absolute",
        marginTop: Dimensions.get("screen").width / 1.4 - 40,
        marginLeft: GLOBAL_STYLES.PADDING - 10,
        maxWidth: Dimensions.get("screen").width - 86,
        overflow: "hidden",
        borderBottomLeftRadius: 20,
        borderTopLeftRadius: 20
    },
    by: {
        marginLeft: 5,
        fontSize: 16,
    },
    title: {
        fontSize: 28,
        color: COLORS.MAIN_DARK,
        alignSelf: "center",
        margin: 25,
    },
    lineX: {
        height: 5,
        borderWidth: 0.5,
        borderColor: "transparent",
        borderTopColor: COLORS.LINE_COLOR
    },
    content: {
        marginBottom: 30,
        fontSize: 16,
        color: COLORS.TEXT_PRIMARY,
        marginHorizontal: GLOBAL_STYLES.PADDING - 5,
    },
});