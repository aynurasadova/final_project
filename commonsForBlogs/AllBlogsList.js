import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  Dimensions,
  Alert,
  ActivityIndicator,
} from "react-native";
import { useNavigation } from "@react-navigation/native";

import { COLORS } from "../style/colors";
import { DefText } from "../components";
import { UserNames } from "../commons/UserNames";
import { ICONS } from "../style/icons";
import {
  selectAddedToLaterBlogByID,
  selectBlogLikes,
  toggleLikes,
  addToFavBlog,
  addAddedToLater,
  selectSingleByID,
  deleteBlog,
  selectBlogComments,
} from "../store/blogs";
import { connect } from "react-redux";
import { selectAuthUserID } from "../store/auth";
import { GLOBAL_STYLES } from "../utils/GLOBAL_STYLES";
import { BTNs } from "./BTNs";
import { MaterialIcons } from "@expo/vector-icons";

const mapStateToProps = (state, { blog }) => ({
  userID: selectAuthUserID(state),
  singleBlog: selectSingleByID(state, blog?.id),
  likes: selectBlogLikes(state, blog?.id),
  singleItemAddedToLaterStatus: selectAddedToLaterBlogByID(state, blog?.id),
  comments: selectBlogComments(state, blog?.id),
});

export const AllBlogsList = connect(mapStateToProps, {
  toggleLikes,
  addToFavBlog,
  addAddedToLater,
  deleteBlog,
})(
  ({
    blog,
    users,
    likes,
    userID,
    toggleLikes,
    deleteBlog,
    singleItemAddedToLaterStatus,
    addAddedToLater,
    addToFavBlog,
    singleBlog,
    comments,
  }) => {
    const isLiked = !!likes[userID];
    const likesCount = Object.keys(likes).length;
    const navigation = useNavigation();
    const commentsCount = Object.keys(comments).length;
    const { author, id } = blog;
    
    const [isImageLoading, setIsImageLoading] = useState(false);

    const likesCounter = (likeLength) => {
      if (likeLength < 1000) {
        return `${likeLength} `;
      } else if (likeLength >= 1000 && likeLength < 1000000) {
        return `${likeLength / 1000}k `;
      } else if (likeLength >= 1000000) {
        return `${likeLength / 1000000}M `;
      }
    };
    const handleAddedToLaterPress = () => {
      addAddedToLater(userID, id, singleItemAddedToLaterStatus);
    };
    const handleLikePress = () => {
      toggleLikes(userID, id, isLiked);
      addToFavBlog(userID, id, isLiked);
    };

    const handleDeletePress = () => {
      Alert.alert("Do you really want to delete your post", "If you want, click yes to continue ", [{
          text: "Cancel",
          style: "cancel",
      },{
          text: "Yes",
          onPress: () => {
              deleteBlog(userID, id, isLiked, singleItemAddedToLaterStatus)
          }
      }])
    };

  const validationForUser = users[singleBlog?.author]?.userID === userID;
    return(        
        <TouchableOpacity 
          onPress = {() => navigation.navigate("SingleBlogScreen", { blogID: id })} 
          style = {styles.singleBlog}
        >
          <View style = {styles.row}>
            <View style = {styles.blogPhotoWrapper}>
              {!isImageLoading && <ActivityIndicator style = {styles.activityIndicator} size = {24} color = "black" />}
              <Image 
                source = {{uri: singleBlog?.blogPhoto}}
                style = {styles.blogPhoto}
                resizeMode = "cover"
                onLoadEnd = {() =>setIsImageLoading(true)}
              />
            
            </View>
            <View style = {styles.blogInfoWrapper}>
                <DefText 
                    style = {styles.title} 
                    numberOfLines = {1}>{singleBlog?.title}
                </DefText>
                <DefText 
                    weight = "light" 
                    style = {styles.content}
                    numberOfLines = {1}>{singleBlog?.content}
                </DefText>
                <View style = {styles.authorInfo}>
                  <DefText style = {{color: COLORS.MAIN_LIGHT}}>By</DefText>
                  <UserNames 
                      firstName = {validationForUser ? "me" : users[singleBlog?.author]?.userFirstName} 
                      lastName = {validationForUser ? "" : users[singleBlog?.author]?.userLastName}
                      spaceFromLeft = {5}
                      authorNameColor = {COLORS.MAIN_LIGHT}
                  />
                  
                </View>
                <View style = {styles.likesAndComments}>
                  <View style = {styles.likes}>
                    <BTNs 
                      type = "like"
                      status = {isLiked}
                      onPress = {handleLikePress}
                      size = {25}
                    />
                    <DefText style = {styles.likesText}>{likesCounter(likesCount)} likes</DefText>
                  </View>      
                  <View style = {styles.comments}>
                    <DefText numberOfLines = {1} weight = "light" style = {styles.likesText}>{likesCounter(commentsCount)}</DefText>
                    <MaterialIcons
                      name = "message" 
                      size = {16} 
                      color = {COLORS.TEXT_PRIMARY}
                    />
                  </View>             
                </View>
            </View>
          </View>

        <View style={styles.actionsWrapper}>
          <BTNs
            type="bookmark"
            size={25}
            status={singleItemAddedToLaterStatus}
            onPress={handleAddedToLaterPress}
          />
          {
            validationForUser &&
              <BTNs 
                type = "delete"
                size = {25}
                onPress = {handleDeletePress}
              />
          }
        </View>
      </TouchableOpacity>
    );
  }
);

const styles = StyleSheet.create({
  singleBlog: {
    borderWidth: 1,
    borderColor: "transparent",
    marginVertical: 12,
    flexDirection: "row",
    justifyContent: "space-between",
    width: Dimensions.get("screen").width - GLOBAL_STYLES.PADDING * 2,
    height: 100,
    maxHeight: 110,
    overflow: "hidden",
  },
  row: {
    flexDirection: "row",
    width: "60%",
  },
  blogPhotoWrapper: {
    height: "100%",
    width: 122,
  },
  blogPhoto: {
    height: "100%",
    width: 110,
    borderRadius: 20,
  },
  activityIndicator: {
    height: "100%",
    width: 110,
    borderRadius: 20,
    backgroundColor: "#d4d4d4"

  },
  blogInfoWrapper: {
    paddingTop: 5,
    height: "100%",
    overflow: "hidden",
    width: "82%",
  },
  authorInfo: {
    flexDirection: "row",
    alignItems: "center",
    overflow: "hidden",
    paddingTop: 7,
  },
  likes: {
    flexDirection: "row",
    alignItems: "center",
  },
  likesText: {
    marginLeft: 5,
    marginRight: 5
  },
  title: {
    fontSize: 15,
    color: COLORS.MAIN_LIGHT,
    marginBottom: 2,
  },
  content: {
    fontSize: 13,
    color: COLORS.TEXT_SECONDARY,
  },
  actionsWrapper: {
    paddingVertical: 5,
    marginLeft: 50,
    justifyContent: "space-between"
  },
  actions: {
    zIndex: 5,
  },
  actionsImg: {
    width: 20,
    height: 20,
    marginLeft: 5,
  },
  likesAndComments: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  comments: {
    flexDirection: "row",
    alignItems: "center"
  },
});
