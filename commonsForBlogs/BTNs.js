import React from "react";
import { StyleSheet, TouchableOpacity, Image, View } from "react-native";
import { Ionicons, Fontisto, MaterialIcons, MaterialCommunityIcons } from "@expo/vector-icons";

import { DefText } from "../components";
import { ICONS } from "../style/icons";
import { COLORS } from "../style/colors";
export const BTNs = ({ name, type, size, color, status, style, ...rest }) => {
  return (
    <TouchableOpacity style={[styles.container, style]} {...rest}>
      {type === "back" && (
        <Ionicons
          name="ios-arrow-back"
          size={size || 35}
          color={color || COLORS.MAIN_LIGHT}
        />
      )}
      {type === "bookmark" && (
        <MaterialIcons
          name={status ? "bookmark" : "bookmark-border"}
          size={size || 30}
          color={status ? COLORS.BUTTON_ADDED : color || COLORS.MAIN_LIGHT}
        />
      )}
      {type === "like" && (
        <Ionicons
          name={status ? "md-heart" : "md-heart-empty"}
          size={size || 30}
          color={color || COLORS.MAIN_LIGHT}
        />
      )}
      {
        type === "delete" && (
          <MaterialCommunityIcons 
          name = "delete" 
          size = {size || 30}
          color = "#e60000" />
        )
      }
    </TouchableOpacity>
  );
};
const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 50,
    padding: 3,
    zIndex: 5000,
  },
  iconImg: {
    width: "50%",
    height: "50%",
  },
});
