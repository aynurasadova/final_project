import React from 'react';
import { StyleSheet, TouchableOpacity, View, Alert } from 'react-native';
import { selectBlogSingleCommentReplies } from '../store/blogs';
import { DefText } from '../components';
import { connect } from 'react-redux';
import { ConvertingObjToArr } from '../utils/ConvertingObjToArr';
import { Entypo } from '@expo/vector-icons';
import fbApp from '../utils/firebaseInit';
import { COLORS } from '../style/colors';

const mapStateToProps = (state, {commentID}) => ({
    singleBlogCommentReplies: selectBlogSingleCommentReplies(state, commentID)
});

export const DeleteBlogComment = ({blogID, commentID, userID}) => {
    const deletePressHandler = () => {
        Alert.alert("Are you sure to delete comment ?", "If yes, click continue", [
            {
                text: "Cancel",
                style: "cancel",
            },
            {
                text: "Continue",
                onPress: () => {
                    fbApp.db.ref(`blogCommentsReply/${commentID}`).remove()
                    fbApp.db.ref(`blogs/${blogID}/comments/${commentID}`).remove()
                }
            }
        ])
        
    }
    return(
        <TouchableOpacity style = {styles.deleteIcon} onPress = {deletePressHandler}>
            <Entypo name="cross" size={18} color = {COLORS.MAIN_LIGHT}/>
        </TouchableOpacity>
    )
};

const styles = StyleSheet.create({
    deleteIcon: {
        position: "absolute",
        right: 0,
        marginRight: 25,
        top: 5
    },
})