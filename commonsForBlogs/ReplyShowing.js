import React from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import { selectBlogSingleCommentReplies } from '../store/blogs';
import { DefText } from '../components';
import { connect } from 'react-redux';
import { ConvertingObjToArr } from '../utils/ConvertingObjToArr';

const mapStateToProps = (state, {commentID}) => ({
    singleBlogCommentReplies: selectBlogSingleCommentReplies(state, commentID)
});

export const ReplyShowing = connect(mapStateToProps)(({singleBlogCommentReplies, commentID}) => {
    const replyCounter = ConvertingObjToArr(singleBlogCommentReplies).length;
    const noReply = replyCounter === 0;
    const singleReply = replyCounter === 1;
    return(
        <>
        {
        !noReply &&
        <View style = {styles.replyWrapper} >
            <DefText style = {styles.replyText} weight = "medium">{!singleReply ? `${replyCounter} replies` : `View reply`}</DefText>
        </View>
        }
        </>
    )
});

const styles = StyleSheet.create({
    replyWrapper: {
        marginTop: 10,
        marginLeft: 15,
    },
    replyText: {
        fontSize: 17,
        color: "#3883c4"
    },
})