import React from 'react';
import { StyleSheet, View, FlatList } from 'react-native';
import { connect } from 'react-redux';

import { DefText } from '../components';
import { COLORS } from '../style/colors';
import { FlatListForAppUser } from './FlatListForAppUser';
import { selectFollowers } from '../store/auth';
import { ConvertingObjToArr } from '../utils/ConvertingObjToArr';
import { GLOBAL_STYLES } from '../utils/GLOBAL_STYLES';
import { BTNs } from '../commonsForBlogs/BTNs';

const mapStateToProps = (state) => ({
    followers: selectFollowers(state),
})

export const FollowersScreen = connect(mapStateToProps)(({
    followers,
    navigation
}) => {
    const followersArr = ConvertingObjToArr(followers || {});
    const empty = followersArr.length === 0;
    return (
        <View style = {styles.container}>
            <BTNs style = {styles.backBtn} type = "back" onPress = {() => navigation.goBack()} />
            {
                empty 
                ? <DefText
                    style={{
                    fontSize: 18,
                    alignSelf: "center",
                    marginTop: 25,
                    color: COLORS.LINE_COLOR,
                    }}
                  >No user recorded as follower</DefText> 
                : <FlatList 
                contentContainerStyle = {styles.contentContainer}
                data = {followersArr}
                renderItem = {({item}) => (
                    <FlatListForAppUser navigation = {navigation} user = {item} />
                )}
            />
            }
            
        </View>
    )
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.BG_PRIMARY,
        paddingHorizontal: GLOBAL_STYLES.PADDING,
        paddingTop: 30,
    },
    contentContainer: {
    }, 
    searchBarWrapper: {
        flexDirection: "row",
        width: "100%",
        fontSize: 20,
        justifyContent: "space-between",
    },
    cancelText: {
        paddingVertical: 10,
        color: COLORS.TEXT_PRIMARY,
    },
    backBtn: {
        paddingLeft: 5,
        paddingBottom: 20,
        alignSelf: "flex-start"
    },
});