import React, { useState } from 'react';
import { StyleSheet, View, TouchableOpacity, Image, Alert, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';

import { DefText } from '../components';
import { UserNames } from '../commons';
import { COLORS } from '../style/colors';
import { selectAppUsers, followingStates } from '../store/users';
import { ConvertingObjToArr } from '../utils/ConvertingObjToArr';
import { selectAllUploadedBlogs, selectSingleUploadedBlog } from '../store/blogs';
import { FlatListForBlogs } from '../commonsForBlogs/FlatListForBlogs';
import { BTNs } from '../commonsForBlogs/BTNs';
import { GLOBAL_STYLES } from '../utils/GLOBAL_STYLES';
import { selectFollowings } from '../store/auth';

const mapStateToProps = (state, {route}) => ({
    appUsers: selectAppUsers(state),
    userUploadedBlogs: selectSingleUploadedBlog(state, route?.params?.userID),
    followings: selectFollowings(state),
})

export const SingleAppUserScreen = connect(mapStateToProps, {
    followingStates
})(({
    route: {params: {userID}},
    appUsers,
    userUploadedBlogs,
    followings,
    followingStates,
    navigation,
}) => {
    const appUsersArray = ConvertingObjToArr(appUsers || {});
    const user = appUsersArray.find((user) => user.id === userID);

    const followingsArr = ConvertingObjToArr(followings || {});
    const following = followingsArr.find((following) => following.id === userID);
    
    const userUploadedBlogsArr = ConvertingObjToArr(userUploadedBlogs?.blogs || {});
    const empty = userUploadedBlogsArr.length === 0;

    const [isImageLoading, setIsImageLoading] = useState(false);
    const toggleFollowing = () => { 
        following?.status ? (Alert.alert("If you change your mind, you can follow again", "Are you sure to stop following", [
            {
                text: "Cancel",
                style: "cancel",
            },
            {
                text: "Unfollow",
                onPress: () => followingStates(userID, true),
            }
        ])):followingStates(userID, false);
    };
    return (
        <View style = {styles.container}>
            <BTNs style = {styles.backBtn} type = "back" onPress = {() => navigation.goBack()} />
            <View style = {styles.userInfoWrapper}>
              {!isImageLoading && <ActivityIndicator style = {styles.activityIndicator} size = {24} color = "black" />}
                <Image 
                    style = {{                
                        width: isImageLoading ? 50 : 1,
                        height: isImageLoading ? 50 : 1,
                        borderRadius: 50,
                    }} 
                    source = {{uri: user.profilePhoto}} 
                    onLoadEnd = {() =>setIsImageLoading(true)}
                />
                
                <View style = {styles.bioAndNames}>
                    <UserNames 
                        firstName = {user.userFirstName}
                        lastName = {user.userLastName}
                        fontSize = {16}
                        styleOfContainer = {{ paddingVertical: 5}}
                    /> 
                    {/* here will be BIO */}
                </View>
            </View>
            <TouchableOpacity 
                style = {[styles.followBtn, {backgroundColor: following?.status ? "#6EDA53" : "rgba(0,0,0,.3)"}]} 
                onPress = {toggleFollowing}
            >
                <DefText style = {styles.followBtnText}> {following?.status ? "Followed" : "Follow"} </DefText>
            </TouchableOpacity>
            {empty ? 
                <DefText
                    style={{
                    fontSize: 18,
                    alignSelf: "center",
                    marginTop: 25,
                    color: COLORS.LINE_COLOR,
                    }}
                >This account has not posted anything yet</DefText>
                :
                <FlatListForBlogs 
                    numColumns = {2} 
                    data = {userUploadedBlogsArr} 
                    contentWidth = {100} 
                />
            }
            
        </View>
    )
});

const styles = StyleSheet.create({
    container: {
        backgroundColor: COLORS.BG_PRIMARY,
        flex: 1,
        paddingHorizontal: GLOBAL_STYLES.PADDING,
        paddingTop: 30,
    },
    activityIndicator: {
        width: 50,
        height: 50,
        borderRadius: 50,
        backgroundColor: "#d4d4d4"
    },
    userPhoto: {
        width: 50,
        height: 50,
        borderRadius: 50,
    },
    userInfoWrapper: {
        flexDirection: "row"
    },
    bioAndNames: {
        paddingLeft: 15,
    },
    backBtn: {
        paddingLeft: 5,
        paddingBottom: 20,
        alignSelf: "flex-start"
    },
    followBtn: {
        width: 100,
        alignItems: "center",
        borderRadius: 10,
        paddingVertical: 7,
        marginVertical: 15,
    },
});