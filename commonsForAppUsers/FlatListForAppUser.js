import React, { useState } from 'react';
import { StyleSheet, View, TouchableOpacity, Image, Alert, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';

import { UserNames } from '../commons/UserNames';
import { DefText } from '../components';
import { selectAppUsers, followingStates } from '../store/users';
import { ConvertingObjToArr } from '../utils/ConvertingObjToArr';
import { selectFollowings, selectFollowers } from '../store/auth';

const mapStateToProps = (state) => ({
    allUsers: selectAppUsers(state),
    followings: selectFollowings(state),
    followers: selectFollowers(state),
});

export const FlatListForAppUser = connect(mapStateToProps, {followingStates})(({
    allUsers,
    user, 
    followings,
    followers,
    navigation,
    followingStates,
}) => {
    const appUsersArr = ConvertingObjToArr(allUsers || {});
    const singleAppUser = appUsersArr.find((u) => u.id === user?.id);

    const followingsArr = ConvertingObjToArr(followings || {});
    const following = followingsArr.find((following) => following.id === user?.id);

    const followersArr = ConvertingObjToArr(followers || {});
    const follower = followersArr.find((follower) => follower.id === user?.id);

    const toggleFollowing = () => { 
        following?.status ? (Alert.alert("If you change your mind, you can follow again", "Are you sure to stop following", [
            {
                text: "Cancel",
                style: "cancel",
            },
            {
                text: "Unfollow",
                onPress: () => followingStates(user?.id, true),
            }
        ])):followingStates(user?.id, false);
    };
    const [isImageLoading, setIsImageLoading] = useState(false);

    return (
        <TouchableOpacity 
            onPress = {() => navigation.navigate("SingleAppUserScreen", {userID: singleAppUser.id})} 
            style = {styles.container}
        >
            <View style = {styles.userProfileDetails}>
              {!isImageLoading && <ActivityIndicator style = {styles.activityIndicator} size = {24} color = "black" />}

                <Image 
                    style = {[styles.userPhoto, {
                        width: isImageLoading ? 50 : 1,
                        height: isImageLoading ? 50 : 1
                    }]} 
                    source = {{uri: singleAppUser.profilePhoto}} 
                    onLoadEnd = {() =>setIsImageLoading(true)}
                />
                <View style = {styles.userInfoWrapper}>
                    <UserNames 
                        firstName = {singleAppUser.userFirstName}
                        lastName = {singleAppUser.userLastName}
                        fontSize = {16}
                        styleOfContainer = {{ paddingVertical: 5}}
                    />
                    {/* here will be BIO */}
                </View>
            </View>
            <TouchableOpacity 
                style = {[styles.followBtn, {backgroundColor: following?.status ? "#6EDA53" : "rgba(0,0,0,.3)"}]}
                onPress = {toggleFollowing}
            >
                <DefText style = {styles.followBtnText}> {following?.status ? "Followed" :(follower?.status ? "Follow Back" : "Follow")} </DefText>
            </TouchableOpacity>
        </TouchableOpacity>
    )
})

const styles = StyleSheet.create({
    container: {
        flexDirection: "row",
        marginVertical: 10,
        alignItems: "center",
        justifyContent: "space-between"
    },
    userProfileDetails: {
        flexDirection: "row",
    },
    activityIndicator: {
        width: 50,
        height: 50,
        borderRadius: 50,
        backgroundColor: "#d4d4d4",
    },
    userPhoto: {
        borderRadius: 50,
    },
    userInfoWrapper: {
        marginLeft: 15,
    },
    followBtn: {
        width: 90,
        alignItems: "center",
        borderRadius: 10,
        paddingVertical: 5,
        marginRight: 15,
    },
    followBtnText: {
        fontSize: 13,
    },
});