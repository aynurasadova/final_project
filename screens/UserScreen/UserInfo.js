import React, { useState } from 'react';
import { 
  View, 
  StyleSheet, 
  Dimensions, 
  Image, 
  TouchableOpacity,
  ActivityIndicator
} from 'react-native';
import { connect } from 'react-redux';
import { Ionicons } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';

import { GLOBAL_STYLES } from '../../utils/GLOBAL_STYLES';
import { 
  selectAuthUserFirstName, 
  selectAuthUserLastName,
  logOut,
  selectProfilePhoto,
  selectUserInfo,
  selectFollowers,
  selectFollowings,
} from '../../store/auth';
import { UserNames } from '../../commons';
import { COLORS } from '../../style/colors';
import { DefText } from '../../components';
import { AppLoading } from 'expo';
import { selectUploadedBlogs } from '../../store/blogs';
import { ConvertingObjToArr } from '../../utils/ConvertingObjToArr';

const mapStateToProps = (state) => ({
  firstName: selectAuthUserFirstName(state),
  lastName: selectAuthUserLastName(state),
  profilePhoto: selectProfilePhoto(state),
  userInfo: selectUserInfo(state),
  uploadedBlogs: selectUploadedBlogs(state),
  followers: selectFollowers(state),
  followings: selectFollowings(state),
});

export const UserInfo = connect(mapStateToProps, {logOut})(( {
  firstName,
  lastName,
  profilePhoto,
  uploadedBlogs,
  followers,
  followings,
  userInfo,
} ) => {
  const navigation = useNavigation();
  const followingsArr = ConvertingObjToArr(followings);
  const followingsLength = followingsArr?.length || 0;
  
  const followersArr = ConvertingObjToArr(followers);
  const followersLength = followersArr?.length || 0;

  const [isImageLoading, setIsImageLoading] = useState(false); 
  return (
      <View style = {styles.container}>
      <View style = {styles.header}>
          <UserNames 
            styleOfContainer = {{ marginLeft: "36%"}}
            weight = "bold"
            firstName = {firstName} 
            lastName = {lastName} 
            spaceFromLeft = {GLOBAL_STYLES.PADDING / 2}
            fontSize = {20}
          />  
        <TouchableOpacity onPress = {() => navigation.openDrawer()} style = {styles.menu}>
          <Ionicons name = "md-menu" size = {35} color = {COLORS.MAIN_LIGHT} />
        </TouchableOpacity>
      </View>
        <View style = {styles.userInfos}>
          
          <View style = {styles.imgWrapper}>
          {!isImageLoading && <ActivityIndicator style = {styles.activityIndicator} size = {24} color = "black" />}
            <Image 
            source = {{uri: profilePhoto}}
            style = {styles.profileImg} 
            resizeMode = "cover"
            onLoadEnd = {() =>setIsImageLoading(true)}
            />
          </View>
          <View style = {styles.profileDetailsWrapper}>
            <View style = {styles.profileDetails}>
              <DefText style = {styles.profileDetailsText}>{uploadedBlogs.length || "0"}</DefText>
              <DefText style = {styles.profileDetailsText}>Posts</DefText>
            </View>
            <TouchableOpacity onPress = {() => navigation.navigate("FollowersScreen")} style = {styles.profileDetails}>
              <DefText style = {styles.profileDetailsText}>{followersLength}</DefText>
              <DefText style = {styles.profileDetailsText}>Followers</DefText>
            </TouchableOpacity>
            <TouchableOpacity onPress = {() => navigation.navigate("FollowingsScreen")} style = {styles.profileDetails}>
              <DefText style = {styles.profileDetailsText}>{followingsLength}</DefText>
              <DefText style = {styles.profileDetailsText}>Followings</DefText>
            </TouchableOpacity>
          </View>
            
        </View>
        <TouchableOpacity onPress = {() => navigation.navigate("EditProfile")} style = {styles.editProfileBtn}>
          <DefText style = {styles.editProfile}>Edit Profile</DefText>
        </TouchableOpacity>
      </View>
  );
})

const styles = StyleSheet.create({
  container: {
    width: Dimensions.get("screen").width - GLOBAL_STYLES.PADDING * 2,
    alignSelf: "center",
  },
  header: {
    flexDirection: "row", 
    alignItems: "center", 
    justifyContent: "space-between", 
    marginBottom: 20, 
    marginTop: 15, 
  },
  menu: {
    paddingRight: 5,
  },
  userInfos: {
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    overflow: "hidden",
  },
  imgWrapper: {
    width: 75,
    height: 75,
    borderRadius: 50,
  },
  activityIndicator: {
    width: 75,
    height: 75,
    borderRadius: 50,
    backgroundColor: "#d4d4d4",
  },
  profileImg: {
    width: 75,
    height: 75,
    borderRadius: 50,
  },
  editProfileBtn: {
      padding: 6,
      marginTop: 30,
      marginBottom: 15,
      borderWidth: 1,
      borderColor: COLORS.MAIN_LIGHT,
      alignSelf: "center",
      borderRadius: 7,
      width: Dimensions.get("screen").width - GLOBAL_STYLES.PADDING * 2,
  },
  editProfile: {
      fontSize: 15,
      color: COLORS.MAIN_LIGHT,
      alignSelf: "center"
  },
  profileDetailsWrapper: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  profileDetails: {
    alignItems: "center",
    marginHorizontal: 10
  },
  profileDetailsText: {
    fontSize: 16,
    color: "#FE5B5B"
  },
});
