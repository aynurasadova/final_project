import React, { useState } from 'react';
import { View, StyleSheet, Dimensions, Image, TouchableOpacity } from 'react-native';

import { TabBarView } from './TabBarView';
import { COLORS } from '../../style/colors';
import { UserInfo } from './UserInfo';

export const UserScreen = ( {navigation} ) => {
  return (
    <View style = {styles.container}>
      <UserInfo />
      <TabBarView navigation = {navigation} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.BG_PRIMARY,
  },
});
