import React, { useState } from 'react';
import { 
    StyleSheet, 
    View, 
    TouchableOpacity, 
    Image, 
    ScrollView,
    KeyboardAvoidingView,
    Alert,
    Dimensions,
    Platform
} from 'react-native';
import { connect } from 'react-redux';
import { Ionicons, Fontisto } from '@expo/vector-icons';
import * as Permissions from "expo-permissions";
import * as ImagePicker from "expo-image-picker";


import { COLORS } from '../../../style/colors';
import { ICONS } from '../../../style/icons';
import { GLOBAL_STYLES } from '../../../utils/GLOBAL_STYLES';
import { 
    InputField, 
    ButtonwithLabel, 
} from '../../../commons';
import { createBlog } from '../../../store/blogs';
import { DefText } from '../../../components';

const imagePickerOptions = {
    mediaTypes: ImagePicker.MediaTypeOptions.Images,
    allowsEditing: true,
    aspect: [1, 1],
    quality: 0.3,
};  

export const CreateBlogScreen = connect(null, {createBlog})(({navigation, createBlog}) => {

    const [field, setField] = useState({
        title: "",
        content: "",
    });

    const [blogImg, setBlogImg] = useState("");

    const fieldChangeHandler = (name, value) => {
        setField((state) => ({
            ...state,
            [name]: value,
        }))
    };

    const selectImage = async(isCamera) => {
        try {
            const permission = await ImagePicker.requestCameraPermissionsAsync();
            if(permission) {
                let image;
                if(!isCamera) {
                    image = await ImagePicker.launchImageLibraryAsync(imagePickerOptions);
                } else {
                    image = await ImagePicker.launchCameraAsync(imagePickerOptions);
                }
                const {cancelled, uri} = image;
                if(!cancelled) {
                    setBlogImg(uri);
                }
            }
        } catch (error) {
            console.log("selectImage error", error)
        }
    }
    const submitForPostHandler = () => {
        if(field.title.trim() === "") {
            Alert.alert("Form validation error", "Title is required for posting");
            return;
        } else if (field.content.trim() === "") {
            Alert.alert("Form validation error", "Content is required for posting");
            return;
        } else if (blogImg === "") {
            Alert.alert("Form validation error", "Image is required to clarify your blog content");
        } else {
            setField({
                title: "",
                content: "",
            });
            setBlogImg("");
            createBlog(field.title.trim(), field.content.trim(), blogImg);
            navigation.goBack();
            navigation.navigate("BlogsStack");
        }
    };

    const goBackHandler = () => {
        setField({
            title: "",
            content: "",
        });
        setBlogImg("");
        navigation.goBack();
    }

    return(
    <ScrollView scrollEnabled = {true} style = {styles.container}>
        <KeyboardAvoidingView behavior = "padding" style = {{flex: 1}}>
            <ScrollView style = {{flex: 1}}>
                <View style = {styles.headerWrapper}>
                    <TouchableOpacity onPress = {goBackHandler}>
                        <Ionicons name = "ios-arrow-back" size = { 35 } color = {COLORS.MAIN_LIGHT} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress = {submitForPostHandler}>
                        <DefText style = {styles.share}>Share</DefText>
                    </TouchableOpacity>
                </View>
                <View style = {styles.inner}>
                    <InputField 
                        onChangeText = {(value) => fieldChangeHandler("title", value)}
                        maxHeight = {60} 
                        value = {field.title}
                        placeholderText = "Blog title"
                    />
                    { !!blogImg &&
                        <Image 
                            source = {{uri: blogImg}}
                            style = {styles.blogPhoto}
                        />
                    }
                    <View style = {{flexDirection: "row", justifyContent: "space-between"}}>
                        <ButtonwithLabel 
                            title = "Take Photo"
                            onPress = {() => selectImage(true)}
                            width = "45%"
                        />
                        <ButtonwithLabel 
                            title = "Browse Library"
                            onPress = {() => selectImage(false)}
                            width = "45%"
                        />
                    </View>
                    <InputField 
                        onChangeText = {(value) => fieldChangeHandler("content", value)}
                        maxHeight = {460} 
                        placeholderText = "Write your blog..."
                        value = {field.content}
                    />
                </View>
            </ScrollView>
        </KeyboardAvoidingView>
    </ScrollView>
    
    )
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingVertical: 28,
        paddingHorizontal: GLOBAL_STYLES.PADDING,
        backgroundColor: COLORS.BG_PRIMARY,
        // height: "100%"
    },
    headerWrapper: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        paddingHorizontal: 5,
        marginBottom: 20,
    },
    // backBtn: {
    //     paddingBottom: 20
    // },
    share: {
        fontSize: 16,
        color: COLORS.MAIN_LIGHT,
    },
    blogPhoto: {
        width: Dimensions.get("screen").width / 2,
        height: Dimensions.get("screen").width / 2,
        alignSelf: "center",
        marginTop: 20,
        borderRadius: 10
    },
    inner: {
        backgroundColor: "rgba(0,0,0,.13)",
        padding: 20,
        borderRadius: 8
    },
});
