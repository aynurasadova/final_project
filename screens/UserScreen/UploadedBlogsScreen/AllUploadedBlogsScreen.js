import React from 'react';
import { StyleSheet, View } from 'react-native';
import { connect } from 'react-redux';

import { FlatListForBlogs } from '../../../commonsForBlogs/FlatListForBlogs';
import { COLORS } from '../../../style/colors';
import { GLOBAL_STYLES } from '../../../utils/GLOBAL_STYLES';
import { selectUploadedBlogs } from '../../../store/blogs';
import { DefText } from '../../../components';
import { BTNs } from '../../../commonsForBlogs/BTNs';

const mapStateToProps = (state) => ({
    uploadedBlogs: selectUploadedBlogs(state),
})

export const AllUploadedBlogsScreen = connect(mapStateToProps)(({navigation, uploadedBlogs}) => {
    return(
        <View style = {styles.container}>
            <BTNs style = {styles.backBtn} type = "back" onPress = {() => navigation.goBack()} />
            <DefText style = {styles.title}>My Posts</DefText>
            <FlatListForBlogs numColumns = {2} data = {uploadedBlogs} contentWidth = {100} />
        </View>
    )
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.BG_PRIMARY,
        paddingHorizontal: GLOBAL_STYLES.PADDING,
        paddingTop: 30,
    },
    title: {
        fontSize: 25,
        alignSelf: "center",
        color: COLORS.MAIN_DARK,
        marginBottom: 10,
    },
    backBtn: {
        paddingLeft: 5,
        paddingBottom: 10,
        alignSelf: "flex-start"
    },
})