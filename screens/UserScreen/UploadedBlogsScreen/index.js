import React, {useState, useEffect} from 'react';
import { StyleSheet, View, Image, TouchableOpacity } from 'react-native';
import { DefText } from '../../../components';
import { UserScreenTabPart } from '../../../commons';
import { connect } from 'react-redux';
import { selectUploadedBlogs } from '../../../store/blogs';
import { COLORS } from '../../../style/colors';
import { FlatListForBlogs } from '../../../commonsForBlogs/FlatListForBlogs';
import { ICONS } from '../../../style/icons';

const mapStateToProps = (state) => ({
    uploadedBlogs: selectUploadedBlogs(state),
});

export const UploadedBlogsScreen = connect(mapStateToProps)(({
    navigation, 
    uploadedBlogs,
}) => {

    const [showNextArr, setShowNextArr] = useState(false);

    useEffect(() => {
        if(uploadedBlogs.length > 2) {
            setShowNextArr(true);
        } else {
            setShowNextArr(false)
        }
    }, [uploadedBlogs.length]);
    const empty = uploadedBlogs.length === 0
    return (
        <UserScreenTabPart 
            headerText = {!empty&& "My posts ..."}
            onPressIcon = {() => navigation.navigate("CreateBlogScreen")}
            ionicon = {true}
            iconName = "ios-add-circle"
            iconTitle = "Write Your Blog"
        >
            {   empty 
                &&
                <DefText weight = "light" style = {styles.postsText}>You have no posts</DefText>
            }
            <View style = {{flexDirection: "row", alignItems: "center"}}>
                <FlatListForBlogs mainBlogScreen = {false} numColumns = {2} data = {uploadedBlogs.slice(0,2)} contentWidth = {90} />
                {showNextArr && (<TouchableOpacity onPress = {() => navigation.navigate("AllUploadedBlogsScreen")} style = {styles.nextArrBtn}>
                    <Image style = {styles.nextArr} source = {ICONS.nextArrow} />
                </TouchableOpacity>)}
            </View>
        </UserScreenTabPart>
    )
});
const styles = StyleSheet.create({
    postsText: {
        fontSize: 17,
        alignSelf: "center",
        marginTop: 25,
        color: "lightgrey"
    },
    nextArrBtn: {
      width: 50,
      height: 50,
      backgroundColor: "#eee",
      alignItems: "center",
      justifyContent: "center",
      borderRadius: 50,
    },
    nextArr: {
        width: 25,
        height: 25,
    },
});
