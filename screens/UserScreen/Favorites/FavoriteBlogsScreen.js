import React from "react";
import { StyleSheet, View } from "react-native";
import { connect } from "react-redux";

import { FlatListForBlogs } from "../../../commonsForBlogs/FlatListForBlogs";
import { COLORS } from "../../../style/colors";
import { GLOBAL_STYLES } from "../../../utils/GLOBAL_STYLES";
import { BTNforFavs } from "./BTNforFavs";
import { selectFavoriteBlogs } from "../../../store/blogs";
import { DefText } from "../../../components";
import { BTNs } from "../../../commonsForBlogs/BTNs";

const mapStateToProps = (state, { route }) => ({
  favoriteList: selectFavoriteBlogs(state),
});

export const FavoriteBlogsScreen = connect(mapStateToProps)(
  ({ navigation, favoriteList }) => {
    const favoritesCount = Object.keys(favoriteList).length;
    const noFavorites = favoritesCount === 0;
    return (
      <View style={styles.container}>
        <BTNs style = {styles.backBtn} type = "back" onPress = {() => navigation.goBack()} />
        {noFavorites ? (
          <DefText
            style={{
              fontSize: 18,
              alignSelf: "center",
              marginTop: 25,
              color: COLORS.LINE_COLOR,
            }}
          >
            There is no favorite item
          </DefText>
        ) : (
          <>
            <BTNforFavs title="Blogs" btnWidth={20} status={true} />
            <FlatListForBlogs
              numColumns={2}
              data={favoriteList}
              contentWidth={100}
            />
          </>
        )}
      </View>
    );
  }
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.BG_PRIMARY,
    paddingHorizontal: GLOBAL_STYLES.PADDING,
    paddingTop: 30,
  },
  backBtn: {
      paddingLeft: 5,
      paddingBottom: 10,
      alignSelf: "flex-start"
  },
});
