import React, { useEffect } from "react";
import { StyleSheet, View, TouchableOpacity } from "react-native";

import { DefText } from "../../../components";
import { UserScreenTabPart } from "../../../commons";

import { COLORS } from "../../../style/colors";
import { BTNforFavs } from "./BTNforFavs";
import { selectAuthUserID } from "../../../store/auth";
import { connect } from "react-redux";

const mapStateToProps = (state) => ({
  userID: selectAuthUserID(state),
});

export const Favorites = connect(mapStateToProps)(({ navigation, userID }) => {
  return (
    <UserScreenTabPart headerText="Favorites">
      <View style={styles.btnWrapper}>
        <BTNforFavs
          status={false}
          btnWidth={40}
          title="Books"
          onPress={() => navigation.navigate("FavoriteBooksScreen")}
        />
        <BTNforFavs
          status={false}
          btnWidth={40}
          title="Blogs"
          onPress={() => navigation.navigate("FavoriteBlogsScreen")}
        />
      </View>
    </UserScreenTabPart>
  );
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  btnWrapper: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
});
