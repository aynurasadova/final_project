import React from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  FlatList,
  Dimensions,
} from "react-native";
import { connect } from "react-redux";

import { COLORS } from "../../../style/colors";
import { ICONS } from "../../../style/icons";
import { GLOBAL_STYLES } from "../../../utils/GLOBAL_STYLES";
import { BTNforFavs } from "./BTNforFavs";
import { selectFavorites, selectBooksList } from "../../../store/books";
import { formatData } from "../../../commons/SearchBar";
import { SingleBook } from "../../../commonsForBooks/SingleBook";

const mapStateToProps = (state) => ({
  favoriteBooks: selectFavorites(state),
  books: selectBooksList(state),
});
export const FavoriteBooksScreen = connect(mapStateToProps)(
  ({ navigation, favoriteBooks, books }) => {
    const listOfFavBooks = () => {
      let list = [];
      favoriteBooks.map((fav) => {
        list.push(books.find((book) => book.id === fav.id));
      });
      return list;
    };
    return (
      <View style={styles.container}>
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          style={styles.backBtn}
        >
          <Image style={styles.backBtnImg} source={ICONS.back} />
        </TouchableOpacity>
        <BTNforFavs title="Books" btnWidth={30} status={true} />
        <FlatList
          columnWrapperStyle={styles.list}
          data={formatData(listOfFavBooks(), 2)}
          numColumns={2}
          keyExtractor = {(item) => item.id}
          showsVerticalScrollIndicator={false}
          renderItem={({ item }) => (
            <SingleBook
              book={item}
              onPress={() =>
                navigation.navigate("SingleBookScreen", { bookID: item.id })
              }
            />
          )}
        />
      </View>
    );
  }
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.BG_PRIMARY,
    paddingHorizontal: GLOBAL_STYLES.PADDING,
    paddingTop: 30,
  },
  backBtnImg: {
    width: 28,
    height: 28,
    marginBottom: 15,
  },
  list: {
    marginTop: 30,
    width: Dimensions.get("screen").width - GLOBAL_STYLES.PADDING * 2,
    justifyContent: "space-evenly",
  },
});
