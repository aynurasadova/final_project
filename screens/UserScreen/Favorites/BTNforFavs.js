import React from 'react';
import { TouchableOpacity, StyleSheet } from 'react-native';
import { DefText } from '../../../components';
import { COLORS } from '../../../style/colors';

export const BTNforFavs = ({title, onPress, btnWidth, status, ...rest}) => {
    return (
        <TouchableOpacity disabled = {status} style = {[styles.btn, {width: `${btnWidth}%`}]} onPress = {onPress}>
            <DefText weight = "medium" style = {styles.btnText}>{title}</DefText>
        </TouchableOpacity>
    )
};

const styles = StyleSheet.create({
    btn: {
        backgroundColor: COLORS.BG_SECONDARY,
        paddingVertical: 5,
        marginTop: 10,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 10,
        marginBottom: 15,
    },
    btnText: {
        fontSize: 22,
        color: COLORS.MAIN_DARK,
    },
})