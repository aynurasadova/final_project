import React from "react";
import { StyleSheet, FlatList, Dimensions, View, Image, TouchableOpacity } from "react-native";
import { connect } from "react-redux";

import { UserScreenTabPart } from "../../../commons";
import { formatData } from "../../../commons/SearchBar";
import { 
  selectBooksList, 
  selectAddedToLaterBooks, 
  selectTotalRatings, 
  selectSingleBookFavoriteByID,
} from "../../../store/books";
import { GLOBAL_STYLES } from "../../../utils/GLOBAL_STYLES";
import { BTNs } from "../../../commonsForBlogs/BTNs";
import { DefText } from "../../../components";
import { COLORS } from "../../../style/colors";
import { Ionicons } from '@expo/vector-icons';
import { averageRatingCalculator } from "../../../utils/AverageRatingCalculator";
import fbApp from "../../../utils/firebaseInit";
import { selectAuthUserID } from "../../../store/auth";

const mapStateToProps = (state, {book}) => ({
  userID: selectAuthUserID(state),
  totalRatings: selectTotalRatings(state),
  singleItemFav: selectSingleBookFavoriteByID(state, book?.id),
});

export const FlatListForAddedToLaterBooks = connect(mapStateToProps)(({ 
    book, 
    navigation, 
    totalRatings, 
    singleItemFav,
    userID,

}) => {
    const ratingCalc = averageRatingCalculator(totalRatings, book.id);
    
    const toggleFavorite = () => {
        const reference = fbApp.db.ref(`favoritesBooks/${userID}/${book?.id}`);
        !!singleItemFav
          ? reference.remove()
          : reference.update({
              favorite: true,
            });
      };
    
    return (
            <View key = {book.bookUrl} style = {styles.singleBookWrapper}>
              <View style = {styles.bookPhotoWrapper}>
                <Image style = {styles.bookPhoto} source = {{uri: book.coverImgUrl}} />
              </View>

              <View style = {styles.bookInfo}>

                <DefText numberOfLines = {1} style = {styles.title}>{book.title}</DefText>
                
                <TouchableOpacity onPress = {() => navigation.navigate("SingleBookScreen", { bookID: book?.id })}  style = {styles.readDetailBtn}>
                  <DefText style = {styles.readDetailText}>Read Details</DefText>
                </TouchableOpacity>
                
                <View style = {styles.starDetail}>
                  <DefText>{!!ratingCalc ? (ratingCalc?.sum / ratingCalc?.count).toFixed(1) : "0.0"}</DefText>
                  <Ionicons name = "ios-star" size={24} color = {ratingCalc ? COLORS.FILL_STAR : "white"} />
                </View>
              
              </View>

              <View style = {styles.favorite}>
                <BTNs onPress = {toggleFavorite} type = "like" status = {singleItemFav} size = {25} />
              </View>
            </View>
    );
  }
);

const styles = StyleSheet.create({
  list: {
    width: Dimensions.get("screen").width - GLOBAL_STYLES.PADDING * 2.5,
  },
  singleBookWrapper: {
    flexDirection: "row",
    marginVertical: 10
  },
  bookPhotoWrapper: {
    width: 80,
    height: 130,
  },
  bookPhoto: {
    width: "100%",
    height: "100%",
    borderRadius: 5,
  },
  bookInfo: {
    marginLeft: 15,
    width: 200,
    overflow: "hidden",
    paddingVertical: 10
  },
  title: {
    fontSize: 16,
    color: COLORS.MAIN_LIGHT
  },
  readDetailBtn: {
    backgroundColor: COLORS.MAIN_LIGHT,
    width: "50%",
    alignItems: "center",
    paddingVertical: 5,
    marginTop: 20,
    borderRadius: 8
  },
  readDetailText: {
    fontSize: 13
  },
  starDetail: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    width: "25%",
    marginTop: 10
  },
  favorite: {
    alignSelf: "flex-end",
    marginBottom: 10
  },
});
