import React from "react";
import { 
  StyleSheet, 
  FlatList, 
  Dimensions, 
  View, 
} from "react-native";
import { connect } from "react-redux";

import { formatData } from "../../../commons/SearchBar";
import { 
  selectBooksList, 
  selectAddedToLaterBooks, 
  selectTotalRatings, 
} from "../../../store/books";
import { GLOBAL_STYLES } from "../../../utils/GLOBAL_STYLES";
import { BTNs } from "../../../commonsForBlogs/BTNs";
import { DefText } from "../../../components";
import { COLORS } from "../../../style/colors";
import { FlatListForAddedToLaterBooks } from "./FlatListForAddedToLaterBooks";

const mapStateToProps = (state) => ({
  books: selectBooksList(state),
  addedToLater: selectAddedToLaterBooks(state),
  totalRatings: selectTotalRatings(state),
});

export const ToReadBooks = connect(mapStateToProps)(
  ({ books, addedToLater, navigation, totalRatings }) => {
    const listOfAddedToLaterBooks = () => {
      let list = [];
      addedToLater.map((added) => {
        list.push(books.find((book) => book.id === added.id));
      });
      return list;
    };
    
    const empty = listOfAddedToLaterBooks().length === 0;
    return (
      <View style = {styles.container}>
        <BTNs style = {styles.backBtn} type = "back" onPress = {() => navigation.goBack()} />
        <DefText style = {{...styles.backBtn, color: COLORS.LINE_COLOR, fontSize: 19, alignSelf: empty ? "center" : "flex-start"}}>{empty ? "There is no saved book" : "Continue to read ..."}</DefText>
        <FlatList
        contentContainerStyle={styles.list}
        data={formatData(listOfAddedToLaterBooks(), 1)}
        numColumns={1}
        keyExtractor = {(item) => item.id}
        showsVerticalScrollIndicator={false}
        renderItem = {({item}) => (
          <FlatListForAddedToLaterBooks key = {item.id} navigation = {navigation} book = {item} />
        )}
        />
        </View>
        
    );
  }
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.BG_PRIMARY,
    paddingTop: 20,
    paddingHorizontal: GLOBAL_STYLES.PADDING,
  },
  backBtn: {
      paddingLeft: 5,
      paddingBottom: 10,
      alignSelf: "flex-start"
  },
  list: {
    width: Dimensions.get("screen").width - GLOBAL_STYLES.PADDING * 2.5,
  },
});
