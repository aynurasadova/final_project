import React, { useState } from "react";
import {
  View,
  StyleSheet,
  Dimensions,
  Image,
  Button,
  TouchableOpacity,
} from "react-native";
import { TabView, SceneMap, TabBar } from "react-native-tab-view";
import { MaterialIcons, MaterialCommunityIcons } from '@expo/vector-icons';

import { DefText } from "../../components";
import { ICONS } from "../../style/icons";
import { Reminder } from "./Reminder";
import { Favorites } from "./Favorites";
import { COLORS } from "../../style/colors";
import { UploadedBlogsScreen } from "./UploadedBlogsScreen";

const initialLayout = { width: Dimensions.get("screen").width };

export const TabBarView = ({ navigation }) => {
  const [index, setIndex] = useState(0);
  const [routes] = useState([
    {key: "upload"},
    { key: "reminder" },
    { key: "favorites" },
  ]);

  const getTabBarIcon = (props) => {
    const {
      route: { key },
      focused,
    } = props;
    let icon;
    if (key === "reminder") {
      icon = <MaterialCommunityIcons name = "calendar-month-outline" size = {28} color = {focused ? COLORS.MAIN_DARK : COLORS.LINE_COLOR} />
    } else if (key === "favorites") {
      icon = <MaterialIcons name = {focused ? "favorite" : "favorite-border"} size = {28} color = {focused ? COLORS.MAIN_DARK : COLORS.LINE_COLOR} />
    } else if (key === "upload") {
      icon = <MaterialIcons name = "cloud-upload" size = {28} color = {focused ? COLORS.MAIN_DARK : COLORS.LINE_COLOR} />
    }
    return icon;
  };

  const renderScene = ({ route: { key } }) => {
    switch (key) {
      case "upload":
        return <UploadedBlogsScreen navigation = {navigation} />;
      case "reminder":
        return <Reminder navigation={navigation} />;
      case "favorites":
        return <Favorites navigation={navigation} />;
      
      default:
        return null;
    }
  };

  return (
    <View style={styles.container}>
      <TabView
        renderTabBar={(props) => (
          <TabBar
            {...props}
            pressColor={COLORS.MAIN_LIGHT}
            indicatorStyle={{ backgroundColor: COLORS.MAIN_DARK }}
            style={{ backgroundColor: COLORS.BG_PRIMARY }}
            renderIcon={(props) => getTabBarIcon(props)}
          />
        )}
        style={styles.tabViewStyle}
        navigationState={{ index, routes }}
        renderScene={renderScene}
        onIndexChange={setIndex}
        initialLayout={initialLayout}
        timingConfig={{
          duration: 400,
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  tabViewStyle: { 
    backgroundColor: COLORS.BG_PRIMARY, 
  },
});
