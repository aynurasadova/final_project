import React, { useState, useEffect } from 'react';
import { StyleSheet, TouchableOpacity, Image, View } from 'react-native';
import { connect } from 'react-redux';

import { COLORS } from '../../../style/colors';
import { UserScreenTabPart } from '../../../commons';
import { selectAddedToLaterBlogs } from '../../../store/blogs';
import { FlatListForBlogs } from '../../../commonsForBlogs/FlatListForBlogs';
import { ICONS } from '../../../style/icons';
import { BTNs } from '../../../commonsForBlogs/BTNs';
import { GLOBAL_STYLES } from '../../../utils/GLOBAL_STYLES';
import { DefText } from '../../../components';

const mapStateToProps = (state) => ({
    addedToLaterBlogs: selectAddedToLaterBlogs(state),
})

export const ToReadAndWriteBlogs = connect( mapStateToProps )(({
    navigation, 
    addedToLaterBlogs,
}) => {      
    const empty = addedToLaterBlogs.length === 0;
    return(
        <View style = {styles.container}>
        <BTNs style = {styles.backBtn} type = "back" onPress = {() => navigation.goBack()} />
        <DefText style = {{...styles.backBtn, color: COLORS.LINE_COLOR, fontSize: 19, alignSelf: empty ? "center" : "flex-start"}}>{empty ? "There is no saved blog" : "Continue to read ..."}</DefText>
        <FlatListForBlogs 
            numColumns = {2} 
            data = {addedToLaterBlogs} 
            contentWidth = {100} 
        />
        </View>
    )
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        backgroundColor: COLORS.BG_PRIMARY,
        paddingTop: 20,
        paddingHorizontal: GLOBAL_STYLES.PADDING,
    },
    backBtn: {
        paddingLeft: 5,
        paddingBottom: 10,
        alignSelf: "flex-start"
    },
});
