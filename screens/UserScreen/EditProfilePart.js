import React, {useState} from 'react';
import { 
    Image, 
    View, 
    StyleSheet, 
    TouchableOpacity, 
    ScrollView, 
    KeyboardAvoidingView, 
    TextInput 
} from 'react-native'
import { Ionicons } from '@expo/vector-icons';
import * as Permissions from "expo-permissions";
import * as ImagePicker from "expo-image-picker";
import { connect } from 'react-redux';

import { DefText } from '../../components';
import { COLORS } from '../../style/colors';
import { 
    selectProfilePhoto, 
    selectAuthUserFirstName, 
    selectAuthUserLastName,
} from '../../store/auth';
import { editProfile } from '../../store/users';
import { GLOBAL_STYLES } from '../../utils/GLOBAL_STYLES';

const mapStateToProps = (state) => ({
    firstName: selectAuthUserFirstName(state),
    lastName: selectAuthUserLastName(state),
    profilePhoto: selectProfilePhoto(state),
});

const imagePickerOptions = {
    mediaTypes: ImagePicker.MediaTypeOptions.Images,
    allowsEditing: true,
    aspect: [1, 1],
    quality: 0.2,
};  

export const EditProfilePart = connect(mapStateToProps, {editProfile})(({
    editProfile, 
    navigation,
    firstName,
    lastName,
    profilePhoto,
}) => {

    const [fields, setFields] = useState({
        firstName: firstName,
        lastName: lastName,
    })
    const [profileImg, setProfileImg] = useState(profilePhoto);
    const [profileImgStatus, setProfileImgStatus] = useState(false);
    const goBackHandler = () => {
        setProfileImg(profilePhoto);
        setProfileImgStatus(false);
        setFields({
            firstName: firstName,
            lastName: lastName,
        });
        navigation.goBack();
    };

    const fieldChangeHandler = (name, value) => {
        setFields((p) => ({
            ...p,
            [name]: value
        }))
    }
    const selectImage = async() => {
        try {
            const permission = await ImagePicker.requestCameraPermissionsAsync();
            if(permission) {
                let image;
                image = await ImagePicker.launchImageLibraryAsync(imagePickerOptions);
                const {cancelled, uri} = image;
                if(!cancelled) {
                    setProfileImg(uri);
                    setProfileImgStatus(true);
                }
            }
        } catch (error) {
            console.log("selectImage error", error)
        }
    };
    const submitSave = () => {
        editProfile(fields.firstName, fields.lastName, profileImg, profileImgStatus);
        navigation.goBack();
    }
    return(
        <ScrollView scrollEnabled = {true} style = {styles.container}>
            <KeyboardAvoidingView behavior = "padding" style = {{flex: 1}}>
            <View style = {styles.headerWrapper}>
                <TouchableOpacity onPress = {goBackHandler}>
                    <Ionicons name = "ios-arrow-back" size = { 35 } color = {COLORS.MAIN_LIGHT} />
                </TouchableOpacity>
                <TouchableOpacity onPress = {submitSave} >
                    <DefText style = {styles.share}>Save</DefText>
                </TouchableOpacity>
            </View>
            <Image 
                resizeMode = "cover" 
                style = {styles.profileImg} 
                source = {{uri: profileImg}} 
            />
            <TouchableOpacity style = {styles.changeProfileImg} onPress = {selectImage}>
                <DefText style = {styles.changeProfileImgTxt}>Change profile photo</DefText>
            </TouchableOpacity>
            <View style = {styles.editForm}>
                <View style = {styles.fieldWrapper}>
                    <DefText weight = 'light' style = {styles.label} >First name</DefText>
                    <TextInput 
                        placeholder = "enter ..." 
                        onChangeText = {(value) => fieldChangeHandler("firstName", value)}
                        value = {fields.firstName} 
                        style = {styles.inputField} 
                        placeholderTextColor = {COLORS.LINE_COLOR}
                    />
                </View>
                <View style = {styles.fieldWrapper}>
                    <DefText weight = 'light' style = {styles.label} >Last name</DefText>
                    <TextInput 
                        placeholder = "enter ..." 
                        onChangeText = {(value) => fieldChangeHandler("lastName", value)}
                        value = {fields.lastName} 
                        style = {styles.inputField} 
                        placeholderTextColor = {COLORS.LINE_COLOR}
                    />
                </View>
            </View>
        </KeyboardAvoidingView>
    </ScrollView>
    )
});

const styles = StyleSheet.create({
    container: {
        backgroundColor: COLORS.BG_PRIMARY,
        flex: 1,
    },
    profileImg: {
        width: 100,
        height: 100,
        borderRadius: 50,
        alignSelf: "center",
    },
    changeProfileImg: {
        alignSelf: "center",
        paddingVertical: 12
    },
    changeProfileImgTxt: {
        color: COLORS.MAIN_LIGHT,
        fontSize: 16,
    },
    headerWrapper: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        paddingHorizontal: 5,
        marginBottom: 20,
        padding: GLOBAL_STYLES.PADDING,
        paddingLeft: GLOBAL_STYLES.PADDING,
        paddingRight: GLOBAL_STYLES.PADDING
    },
    share: {
        fontSize: 16,
        color: COLORS.MAIN_LIGHT,
    },
    editForm: {
        borderWidth: 0.5,
        borderBottomColor: COLORS.LINE_COLOR,
        borderTopColor: COLORS.LINE_COLOR,
        paddingVertical: 20
    },
    fieldWrapper: {
        flexDirection: "row",
        alignItems: "center",
        paddingHorizontal: GLOBAL_STYLES.PADDING,
    },
    label: {
        color: "#eee",
        fontSize: 17,
        paddingVertical: 10,
    },
    inputField: {
        color: COLORS.LINE_COLOR,
        fontSize: 16,
        width: "60%",
        paddingVertical: 10,
        paddingLeft: GLOBAL_STYLES.PADDING / 2,
        paddingRight: GLOBAL_STYLES.PADDING,
        marginLeft: 25,
        borderWidth: 1,
        borderColor: "transparent",
        borderBottomColor: COLORS.LINE_COLOR
    },
})