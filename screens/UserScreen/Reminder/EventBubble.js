import React from "react";
import { StyleSheet, TouchableOpacity, View } from "react-native";

import { DefText } from "../../../components";
import { COLORS } from "../../../style/colors";

export const EventBubble = ({
  summary,
  description,
  startDateTime,
  style,
  onLongPress,
}) => {
  return (
    <View style={[{ ...styles.container }, style]}>
      <TouchableOpacity onLongPress={onLongPress}>
        <DefText
          weight="normal"
          style={{
            ...styles.title,
            color: COLORS.MAIN_DARK,
          }}
        >
          {summary}
        </DefText>
        <DefText numberOfLines={2} ellipsizeMode="tail" style={styles.context}>
          {description}
        </DefText>
        <DefText style={styles.time}>
          at {formatDate(startDateTime)}
          {`\n`}
          {formatTime(startDateTime)}
        </DefText>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 81,
    backgroundColor: "white",
    borderBottomRightRadius: 10,
    borderBottomLeftRadius: 10,
    borderTopRightRadius: 10,
    paddingHorizontal: 6,
    paddingVertical: 10,
    backgroundColor: "#eeeeee",
    alignContent: "center",
    justifyContent: "center",
  },
  title: { fontSize: 10 },
  context: { fontSize: 10, color: "black" },
  time: { fontSize: 10, color: "black" },
});

export const formatDate = (dateString) => {
  const date = new Date(Date.parse(dateString));
  const d = `${date.getDate() < 10 ? `0${date.getDate()}` : date.getDate()}/${
    date.getMonth() < 9 ? `0${date.getMonth() + 1}` : date.getMonth() + 1
  }/${date.getFullYear()}`;
  return d;
};

export const formatTime = (timeString) => {
  const time = new Date(Date.parse(timeString));

  const t = `${
    time.getHours() < 10 ? `0${time.getHours()}` : time.getHours()
  }:${time.getMinutes() < 10 ? `0${time.getMinutes()}` : time.getMinutes()}`;
  return t;
};
