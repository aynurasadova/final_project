import React from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  ScrollView,
  FlatList,
} from "react-native";
import { connect } from "react-redux";

import { ICONS } from "../../../style/icons";
import { GLOBAL_STYLES } from "../../../utils/GLOBAL_STYLES";
import { COLORS } from "../../../style/colors";
import { DefText } from "../../../components";
import { EventBubble } from "./EventBubble";
import { selectReminders, deleteReminder } from "../../../store/reminders";
import { ReminderCard } from "./ReminderCard";

const mapStateToProps = (state) => ({
  reminders: selectReminders(state),
});

export const AllEventsScreen = connect(mapStateToProps, { deleteReminder })(
  ({ navigation, reminders, deleteReminder }) => {
    return (
      <View style={styles.container}>
        <View style={styles.inner}>
          <TouchableOpacity onPress={() => navigation.navigate("User")}>
            <Image style={styles.backBtn} source={ICONS.back} />
          </TouchableOpacity>
          <DefText weight="light" style={styles.headerTitle}>
            Upcoming Events
          </DefText>

          <FlatList
            scrollEnabled={true}
            data={reminders}
            renderItem={({ item }) => (
              <ReminderCard
                onPress={() => deleteReminder(item.id)}
                summary={item.summary}
                description={item.description}
                startDateTime={item.start.dateTime}
              />
            )}
          />
        </View>
      </View>
    );
  }
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingVertical: 28,
    paddingHorizontal: GLOBAL_STYLES.PADDING,
    backgroundColor: COLORS.BG_PRIMARY,
    paddingBottom: 65,
  },
  backBtn: {
    width: 28,
    height: 28,
    paddingBottom: 20,
  },
  headerTitle: {
    textAlign: "center",
    color: COLORS.TEXT_SECONDARY,
    fontSize: 25,
    marginBottom: 25,
  },
});
