export const EVENTS = [
  {
    id: 1,
    summary: "Meeting",
    description: "Group meeting for project",
    start: { dateTime: new Date(2020, 7, 2, 7, 30, 0, 0).toISOString() },
    end: { dateTime: new Date(2020, 5, 28, 18, 30, 0, 0) },
  },
];
