import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  Image,
  TextInput,
  Alert,
  Dimensions,
  KeyboardAvoidingView,
} from "react-native";
import { ScrollView } from "react-native-gesture-handler";

import CalendarPicker from "react-native-calendar-picker";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { connect } from "react-redux";
import { Ionicons } from "@expo/vector-icons";

import { ICONS } from "../../../style/icons";
import { GLOBAL_STYLES } from "../../../utils/GLOBAL_STYLES";
import { COLORS } from "../../../style/colors";
import { DefText } from "../../../components";
import { formatTime } from "./EventBubble";

import { addReminder } from "../../../store/reminders";
import {
  signInWithGoogleAsync,
  AddEventToGoogleCalendar,
} from "../../../utils/googleAPI";

export const AddEventScreen = connect(null, { addReminder })(
  ({ navigation, addReminder }) => {
    const [date, setDate] = useState(new Date());
    const [time, setTime] = useState(new Date());
    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
    const [isTimePickerVisible, setTimePickerVisibility] = useState(false);
    const [addToGoogleCalendar, setAddToGoogleCalendar] = useState(false);

    const [field, setField] = useState({
      summary: "",
      description: "",
    });
    const fieldChangeHandler = (name, value) => {
      setField((state) => ({
        ...state,
        [name]: value,
      }));
    };

    async function submitForAddEvent() {
      if (field.summary.trim() === "") {
        Alert.alert(
          "Form validation error",
          "Summary is required for reminder"
        );
        return;
      } else if (field.description.trim() === "") {
        Alert.alert(
          "Form validation error",
          "Description is required for reminder"
        );
        return;
      } else {
        if (addToGoogleCalendar) {
          const accessToken = await signInWithGoogleAsync();
          const eventData = {
            summary: field.summary,
            description: field.description,
            start: {
              dateTime: date,
              timeZone: "Asia/Baku",
            },
            end: {
              dateTime: date,
              timeZone: "Asia/Baku",
            },
          };

          AddEventToGoogleCalendar(accessToken, eventData);
        }

        addReminder(
          field.summary,
          field.description,
          date.toISOString(),
          date.toISOString()
        );
        navigation.navigate("AllEventsScreen");
      }
    }

    const handleConfirm = (date) => {
      setTimePickerVisibility(Platform.OS === "ios");
      setDate(date);
      hideTimePicker();
    };
    const onDateChange = (date) => {
      setDate(new Date(Date.parse(date)));
    };

    const showTimePicker = () => {
      setTimePickerVisibility(true);
    };
    const hideTimePicker = () => {
      setTimePickerVisibility(false);
    };

    return (
      <View style={styles.container}>
        <KeyboardAvoidingView behavior="padding" style={{ flex: 1 }}>
          <View style={styles.headerWrapper}>
            <TouchableOpacity onPress={() => navigation.goBack()}>
              <Ionicons
                name="ios-arrow-back"
                size={35}
                color={COLORS.MAIN_LIGHT}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={submitForAddEvent}>
              <DefText style={styles.add}>ADD</DefText>
            </TouchableOpacity>
          </View>
          <ScrollView showsVerticalScrollIndicator={false}>
            <CalendarPicker
              textStyle={{ color: "white" }}
              onDateChange={onDateChange}
              width={Dimensions.get("screen").width - GLOBAL_STYLES.PADDING * 2}
              selectedDayColor={COLORS.MAIN_LIGHT}
              selectedDayTextColor="white"
              todayBackgroundColor={COLORS.BG_SECONDARY}
            />
            <TouchableOpacity
              onPress={() => showTimePicker()}
              style={styles.chosenDateTime}
            >
              <DefText weight="medium" style={styles.chosenDateTimeText}>
                {formatTime(date)}
              </DefText>
            </TouchableOpacity>

            <DefText style={{ ...styles.inputTitles, marginTop: 15 }}>
              Event Summary
            </DefText>
            <TextInput
              style={styles.inputText}
              onChangeText={(value) => fieldChangeHandler("summary", value)}
            />

            <DefText style={styles.inputTitles}>Event Description</DefText>
            <TextInput
              style={styles.inputText}
              onChangeText={(value) => fieldChangeHandler("description", value)}
            />

            <TouchableOpacity
              onPress={() => {
                addToGoogleCalendar
                  ? setAddToGoogleCalendar(false)
                  : setAddToGoogleCalendar(true);
              }}
              style={{
                ...styles.checkBoxContainer,
                backgroundColor: addToGoogleCalendar
                  ? COLORS.MAIN_LIGHT
                  : "white",
              }}
            >
              <DefText
                style={{
                  ...styles.checkBoxText,
                  color: addToGoogleCalendar ? "white" : COLORS.MAIN_LIGHT,
                }}
              >
                Add to Google Calendar
              </DefText>
              {addToGoogleCalendar && (
                <Ionicons
                  name="ios-checkmark-circle-outline"
                  size={20.5}
                  color={"white"}
                  style={{ marginLeft: 20 }}
                />
              )}
            </TouchableOpacity>

            <DateTimePickerModal
              date={date}
              isVisible={isTimePickerVisible}
              mode="time"
              onConfirm={handleConfirm}
              onCancel={hideTimePicker}
            />
          </ScrollView>
        </KeyboardAvoidingView>
      </View>
    );
  }
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 28,
    paddingBottom: 10,
    paddingHorizontal: GLOBAL_STYLES.PADDING,
    backgroundColor: COLORS.BG_PRIMARY,
  },
  headerWrapper: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: 5,
    marginBottom: 20,
  },
  add: {
    fontSize: 16,
    color: COLORS.MAIN_LIGHT,
  },
  backBtn: {
    width: 28,
    height: 28,
    paddingBottom: 20,
  },
  headerTitle: {
    textAlign: "center",
    color: COLORS.TEXT_SECONDARY,
    fontSize: 25,
    marginBottom: 25,
  },
  chosenDateTime: {
    width: Dimensions.get("screen").width - GLOBAL_STYLES.PADDING * 2,
    height: 40,
    backgroundColor: COLORS.BG_SECONDARY,
    borderRadius: 5,
    color: "black",
    marginTop: 15,
    justifyContent: "center",
  },
  chosenDateTimeText: {
    color: "white",
    fontSize: 18,
    textAlign: "center",
  },
  inputTitles: {
    fontSize: 18,
    color: "#e4e9f2",
  },
  inputText: {
    width: "100%",
    height: 40,
    backgroundColor: COLORS.BG_SECONDARY,
    borderRadius: 5,
    color: COLORS.TEXT_PRIMARY,
    marginVertical: 8,
    justifyContent: "center",

    padding: 10,
  },
  checkBoxText: {
    textTransform: "uppercase",
    fontSize: 19,
  },
  checkBoxContainer: {
    borderRadius: 5,
    paddingHorizontal: 15,
    marginTop: 10,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    height: 40,
  },
});
