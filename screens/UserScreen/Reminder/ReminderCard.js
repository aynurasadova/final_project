import React from "react";
import { StyleSheet, View } from "react-native";

import { COLORS } from "../../../style/colors";
import { DefText } from "../../../components";
import { TouchableOpacity } from "react-native-gesture-handler";
import { formatTime } from "./EventBubble";

export const ReminderCard = ({
  summary,
  description,
  startDateTime,
  onPress,
}) => {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <DefText weight="medium" style={styles.title}>
          {summary}
        </DefText>
        <TouchableOpacity onPress={onPress} style={styles.button}>
          <DefText style={styles.buttonText}>Delete</DefText>
        </TouchableOpacity>
      </View>
      <View style={styles.body}>
        <View style={styles.mainText}>
          <DefText>{description}</DefText>
        </View>
        <View style={styles.dateTime}>
          <DefText weight="light" style={styles.date}>
            {getDayAndMonth(startDateTime)}
          </DefText>
          <View style={styles.timeBox}>
            <DefText weight="medium" style={styles.time}>
              {formatTime(startDateTime)}
            </DefText>
          </View>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: "100%",
    height: 145,
    borderRadius: 10,
    backgroundColor: COLORS.BG_SECONDARY,
    marginBottom: 25,
    paddingVertical: 15,
    paddingLeft: 20,
    paddingRight: 10,
  },
  header: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: 8,
  },
  title: {
    fontSize: 22,
  },
  button: {
    width: 44,
    height: 17,
    backgroundColor: "#CC2F2F",
    borderRadius: 15,
    justifyContent: "center",
    alignItems: "center",
  },
  buttonText: {
    fontSize: 8,
    textTransform: "uppercase",
  },
  body: {
    flexDirection: "row",
    height: 85,
  },
  mainText: {
    width: "70%",
  },
  dateTime: {
    width: "30%",
    justifyContent: "center",
    alignItems: "center",
  },
  date: { fontSize: 22 },
  time: { textAlign: "center", fontSize: 12 },
  timeBox: {
    backgroundColor: COLORS.BUTTON_ADDED,
    width: 60,
    height: 22,
    borderRadius: 12,
    justifyContent: "center",
    marginTop: 7,
  },
});

const getDayAndMonth = (dateString) => {
  const monthNames = [
    "Jan.",
    "Feb.",
    "Mar.",
    "Apr.",
    "May",
    "June",
    "July",
    "Aug.",
    "Sept.",
    "Oct.",
    "Nov.",
    "Dec.",
  ];
  const date = new Date(Date.parse(dateString));
  const d = `${date.getDate()} ${monthNames[date.getMonth()]}`;
  return d;
};
