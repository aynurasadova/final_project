import React, { useEffect } from "react";
import { StyleSheet, View, FlatList, Image } from "react-native";
import { connect } from "react-redux";

import { UserScreenTabPart } from "../../../commons";
import { EventBubble } from "./EventBubble";
import { EVENTS } from "./dummyData";
import { TouchableOpacity, ScrollView } from "react-native-gesture-handler";
import { ICONS } from "../../../style/icons";
import { selectReminders, deleteReminder } from "../../../store/reminders";

const mapStateToProps = (state) => ({
  reminders: selectReminders(state),
});

export const Reminder = connect(mapStateToProps, { deleteReminder })(
  ({ navigation, reminders, deleteReminder }) => {
    return (
      <UserScreenTabPart
        headerText="Upcoming events"
        iconName="addReminder"
        iconTitle="Add Reminder"
        onPressIcon={() => navigation.navigate("AddEventScreen")}
        //onPressIcon -> this will work when you click on Add Reminder icon, provide function, and pass for onPresssIcon
      >
        <View style={styles.container}>
          <FlatList
            style={{ height: 81 }}
            initialNumToRender={2}
            scrollEnabled={false}
            data={reminders}
            numColumns={2}
            renderItem={({ item }) => (
              <EventBubble
                onLongPress={() => deleteReminder(item.id)}
                summary={item.summary}
                description={item.description}
                startDateTime={item.start.dateTime}
                style={{ width: 105, marginRight: 11 }}
              />
            )}
          />
          {reminders.length > 2 && (
            <TouchableOpacity
              onPress={() => navigation.navigate("AllEventsScreen")}
              style={styles.forwardButton}
            >
              <Image source={ICONS.nextArrow} style={styles.nextIcon} />
            </TouchableOpacity>
          )}
        </View>
      </UserScreenTabPart>
    );
  }
);

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  forwardButton: {
    backgroundColor: "white",
    width: 40,
    height: 40,
    borderRadius: 40,
    justifyContent: "center",
    alignItems: "center",
  },
  nextIcon: {
    width: 20,
    height: 20,
  },
});
