import React, { useState, useEffect } from 'react';
import { StyleSheet, View, FlatList, Keyboard, TouchableOpacity } from 'react-native'
import { connect } from 'react-redux';

import { DefText } from '../../components';
import { COLORS } from '../../style/colors';
import { selectAppUsers } from '../../store/users';
import { ConvertingObjToArr } from '../../utils/ConvertingObjToArr';
import { GLOBAL_STYLES } from '../../utils/GLOBAL_STYLES';
import { SearchBarStyled } from '../../commons/SearchBar/SerachBarStyled';
import { formatData, searchHandlerForNames } from '../../commons/SearchBar';
import { FlatListForAppUser } from '../../commonsForAppUsers/FlatListForAppUser';
import { selectAuthUserID } from '../../store/auth';

const mapStateToProps = (state) => ({
    appUsers: selectAppUsers(state),
    userID: selectAuthUserID(state),
});

export const AppUsers = connect(mapStateToProps)(({
    navigation,
    appUsers,
    userID,
}) => {
    const appUsersArray = ConvertingObjToArr(appUsers);
    const excludingNowUser = [];
    appUsersArray.map(u => {
        if(u.id !== userID) {
            return excludingNowUser.push(u)
        }
    });

    const [value, setValue] = useState("");
    const [showCancel, setShowCancel] = useState(false);
    const [searchBarWidth, setSearchBarWidth] = useState(100);
    const onChangeText = (v) => {
        setSearchBarWidth(80);
        setShowCancel(true);
        setValue(v);
    };

    const handleCancel = () => {
        Keyboard.dismiss();
        setValue("");
    };

    useEffect(() => {
        if (value.trim() === "") {
        setShowCancel(false);
        setSearchBarWidth(100);
        }
    }, [value]);
    return (
        <View style = {styles.container}>
            <View style={styles.searchBarWrapper}>
                <SearchBarStyled
                    value={value}
                    onChangeText={(v) => onChangeText(v)}
                    placeholderText="Search for user ..."
                    searchBarWidth={searchBarWidth}
                />
                {showCancel && (
                <TouchableOpacity onPress={handleCancel}>
                    <DefText style={styles.cancelText}>Cancel</DefText>
                </TouchableOpacity>
                )}
            </View>
            <FlatList 
                contentContainerStyle = {styles.contentContainer}
                data = {formatData(searchHandlerForNames(value, excludingNowUser), 1)}
                renderItem = {({item}) => (
                    <FlatListForAppUser navigation = {navigation} user = {item} />
                )}
            />
        </View>
    )
})

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.BG_PRIMARY,
        paddingHorizontal: GLOBAL_STYLES.PADDING,
        paddingTop: 30,
    },
    contentContainer: {
    }, 
    searchBarWrapper: {
        flexDirection: "row",
        width: "100%",
        fontSize: 20,
        justifyContent: "space-between",
    },
    cancelText: {
        paddingVertical: 10,
        color: COLORS.TEXT_PRIMARY,
    },
})