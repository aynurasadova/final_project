import React, {useState, useEffect} from 'react';
import { StyleSheet, View, TouchableOpacity, Image, ScrollView, FlatList } from 'react-native';
import { connect } from 'react-redux';
import { useNavigation } from '@react-navigation/native';

import { selectBlogsList } from '../../store/blogs';
import { FlatListForBlogs } from '../../commonsForBlogs/FlatListForBlogs';
import { DefText } from '../../components';
import { ICONS } from '../../style/icons';
import { BlogsList } from '../../commonsForBlogs/BlogsList';
import { selectAppUsers } from '../../store/users';

const mapStateToProps = (state) => ({
    blogs: selectBlogsList(state),
    users: selectAppUsers(state),
})

export const RecommendBlogs = connect(mapStateToProps)(({blogs, users}) => {
    const navigation = useNavigation();
    const [showNextArr, setShowNextArr] = useState(false);
    const firstFourBlogs = blogs.slice(0,3);

    return(
        <>
        <ScrollView 
            horizontal = {true} 
            contentContainerStyle = {[styles.carousel]}
            showsHorizontalScrollIndicator={false}
            // onContentSizeChange={(w, h) => init(w)}
            scrollEventThrottle={200}
            pagingEnabled
            decelerationRate="fast"
        >
            {
                firstFourBlogs.map(blog => (
                    <BlogsList key = {blog.id} blog = {blog} contentWidth = {85} users = {users} />
                ))
            }
            <TouchableOpacity onPress = {() => navigation.navigate("BlogsStack")} style = {styles.nextArrBtn}>
                <Image style = {styles.nextArr} source = {ICONS.nextArrow} />
            </TouchableOpacity>
        </ScrollView>
        </>
    )
});

const styles = StyleSheet.create({
    nextArrBtn: {
        width: 50,
        height: 50,
        backgroundColor: "#eee",
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 50,
      },
      nextArr: {
          width: 25,
          height: 25,
      },
      carousel: {
      },
})
