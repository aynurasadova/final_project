import React, { useEffect, useState } from "react";
import { 
  StyleSheet, 
  View, 
  Image, 
  Text, 
  Button, 
  ScrollView, 
  FlatList, 
  TouchableOpacity, 
  Dimensions, 
  ActivityIndicator } from "react-native";
import { connect } from "react-redux";

import { DefText } from "../../components";
import {
  getAndListenForBlogs,
  getAndListenForAddedToLaterBlogs,
  clearBlogs,
  getAndListenForFavoriteBlogs,
  clearFavorites,
  clearAddedToLaters,
  getAndListenForUploadedBlogs,
  clearUploadedBlogs,
  getAndListenForAllUploadedBlogs,
  clearAllUploadedBlogs,
  selectBlogsList,
  getAndListenForBlogCommentReplies,
  clearBlogCommentReply,
} from "../../store/blogs";
import { getAndListenAppUsers, selectAppUsers } from "../../store/users";
import {
  getAndListenForBooks,
  getAndListenForFavoriteBooks,
  getAndListenForBooksRating,
  getAndListenForAddedToLaterBooks,
  getAndListenForAllBooksRating,
  getAndListenForBookCommentReplies,
  selectBooksList,
} from "../../store/books";
import { getAndListenForReminders } from "../../store/reminders";

import { BookUploader } from "../../bookUploader";
import { getAndListenUserInfo, selectFollowings } from "../../store/auth";
import { ConvertingObjToArr } from "../../utils/ConvertingObjToArr";
import { GLOBAL_STYLES } from "../../utils/GLOBAL_STYLES";
import { COLORS } from "../../style/colors";
import { MaterialIcons, Entypo } from "@expo/vector-icons";
import { BlogsList } from "../../commonsForBlogs/BlogsList";
import { FollowingsUser } from "./FollowingsUser";

const mapStateToProps = (state) => ({
  books: selectBooksList(state),
  blogs: selectBlogsList(state),
  followings: selectFollowings(state),
  users: selectAppUsers(state),
});

export const HomeScreen = connect(mapStateToProps, {
  getAndListenForBlogs,
  getAndListenForAddedToLaterBlogs,
  clearBlogs,
  getAndListenAppUsers,
  getAndListenForBooks,
  getAndListenForFavoriteBooks,
  getAndListenForBooksRating,
  getAndListenForFavoriteBlogs,
  clearFavorites,
  clearAddedToLaters,
  clearUploadedBlogs,
  getAndListenForReminders,
  getAndListenForAddedToLaterBooks,
  getAndListenUserInfo,
  getAndListenForUploadedBlogs,
  clearAllUploadedBlogs,
  getAndListenForAllUploadedBlogs,
  getAndListenForBlogCommentReplies,
  clearBlogCommentReply,
  getAndListenForBookCommentReplies,
})(
  ({
    getAndListenForBlogs,
    getAndListenForAddedToLaterBlogs,
    clearBlogs,
    getAndListenAppUsers,
    getAndListenForBooks,
    getAndListenForFavoriteBooks,
    getAndListenForBooksRating,
    getAndListenForFavoriteBlogs,
    getAndListenForReminders,
    clearFavorites,
    clearUploadedBlogs,
    clearAddedToLaters,
    getAndListenForAddedToLaterBooks,
    getAndListenUserInfo,
    getAndListenForUploadedBlogs,
    getAndListenForAllUploadedBlogs,
    clearAllUploadedBlogs,
    getAndListenForBlogCommentReplies,
    clearBlogCommentReply,
    getAndListenForBookCommentReplies,
    followings,
    blogs,
    books,
    users,
    navigation,
  }) => {

    const [isImageLoading, setIsImageLoading] = useState(false);
    useEffect(() => {
      const unsubscribe = getAndListenForBlogs();
      return unsubscribe;
    }, []);

    useEffect(() => {
      const unsubscribe = getAndListenForAddedToLaterBlogs();
      return unsubscribe;
    }, []);

    useEffect(() => {
      const unsubscribe = getAndListenAppUsers();
      return unsubscribe;
    }, []);

    useEffect(() => clearBlogs, []);
    useEffect(() => clearFavorites, []);
    useEffect(() => clearAddedToLaters, []);
    useEffect(() => clearUploadedBlogs, []);
    useEffect(() => clearAllUploadedBlogs, []);
    useEffect(() => clearBlogCommentReply, []);

    useEffect(() => {
      const unsubscribe = getAndListenForBooks();
      return unsubscribe;
    }, []);

    useEffect(() => {
      const unsubscribe = getAndListenForFavoriteBooks();
      return unsubscribe;
    }, []);
    useEffect(() => {
      const unsubscribe = getAndListenForBooksRating();
      return unsubscribe;
    }, []);
    useEffect(() => {
      const unsubscribe = getAndListenForBooksRating(true);
      return unsubscribe;
    }, []);
    useEffect(() => {
      const unsubscribe = getAndListenForFavoriteBlogs();
      return unsubscribe;
    }, []);
    useEffect(() => {
      const unsubscribe = getAndListenForAddedToLaterBooks();
      return unsubscribe;
    }, []);
    useEffect(() => {
      const unsubscribe = getAndListenForReminders();
      return unsubscribe;
    }, []);
    useEffect(() => {
      const unsubscribe = getAndListenUserInfo();
      return unsubscribe;
    }, []);
    useEffect(() => {
      const unsubscribe = getAndListenForUploadedBlogs();
      return unsubscribe;
    }, []);
    useEffect(() => {
      const unsubscribe = getAndListenForAllUploadedBlogs();
      return unsubscribe;
    }, []);
    useEffect(() => {
      const unsubscribe = getAndListenForBlogCommentReplies();
      return unsubscribe;
    }, []);
    useEffect(() => {
      const unsubscribe = getAndListenForBookCommentReplies();
      return unsubscribe;
    }, []);
    return (
      <ScrollView style = {styles.scrollContainer} contentContainerStyle={styles.container}>
        <DefText style={[styles.headerText, {marginTop: 10}]}>Recommended books</DefText>
        <ScrollView horizontal={true} contentContainerStyle={[styles.listWrapper, {alignItems: "center"}]} showsHorizontalScrollIndicator={false}>
          {
            books.slice(0,5).map(book => (
              <TouchableOpacity
                key = {book.id}
                onPress={() => {
                  navigation.navigate("SingleBookScreen", { bookID: book.id });
                }}
              >
                {!isImageLoading && <ActivityIndicator style = {styles.activityIndicator} size = {24} color = "black" />}
                <Image 
                  source={{ uri: book.coverImgUrl }} 
                  style={{
                    width: isImageLoading ? 87 : 87,
                    height: isImageLoading ? 135 : 2,
                    marginRight: 25,
                    borderRadius: 10,
                  }} 
                  onLoadEnd = {() =>setIsImageLoading(true)}
                />
              </TouchableOpacity>
            ))
          }
          <View>
            <TouchableOpacity style={styles.more} onPress={() => navigation.navigate("BooksStack")}>
              <MaterialIcons name="navigate-next" size={50} color= {COLORS.MAIN_LIGHT} />
            </TouchableOpacity>
            <DefText style = {styles.discoverText}>Discover more</DefText>
          </View>

        </ScrollView>
        <DefText style={styles.headerText}>Recommended blogs</DefText>

        <ScrollView horizontal={true} contentContainerStyle = {[styles.listWrapper, {alignItems: "center"}]} showsHorizontalScrollIndicator={false}>
            {
              blogs.slice(0,6).map(blog => (
                <BlogsList key = {blog.id} blog = {blog} users = {users} />
              ))
            }
            <View>
              <TouchableOpacity style={styles.more} onPress={() => navigation.navigate("BlogsStack")}>
                {blogs.length !==0 ? <MaterialIcons name="navigate-next" size={50} color= {COLORS.MAIN_LIGHT} />
                :<Entypo name="plus" size={50} color={COLORS.MAIN_LIGHT} />}
              </TouchableOpacity>
              <DefText style = {styles.discoverText}>Discover more</DefText>
            </View>
        </ScrollView>
        <FollowingsUser/>
      </ScrollView>
    );
  }
);

const styles = StyleSheet.create({
  scrollContainer: {
    backgroundColor: COLORS.BG_PRIMARY,
    paddingHorizontal: GLOBAL_STYLES.PADDING-2,
  },
  headerText: {
    fontSize: 18,
    alignSelf: "flex-start",
    color: "#a6a09f",
    borderColor: COLORS.TEXT_PRIMARY,
    marginBottom: 7,
  },
  listWrapper: {
    flexDirection: "row",
    marginVertical: 20,
    maxHeight: 160,
  },
  list: {
    alignItems: "center",
  },
  activityIndicator: {
    width: 87,
    height: 135,
    marginRight: 25,
    backgroundColor: "#d4d4d4",
    borderRadius: 10,
  },
  img: {
    width: 87,
    height: 135,
    marginRight: 25,
  },
  more: {
    width: 70,
    height: 70,
    alignItems: "center",
    backgroundColor: COLORS.TEXT_PRIMARY,
    borderRadius: 75,
    justifyContent: "center",
    alignSelf: "center"
  },
  discoverText: {
    marginTop: 12,
    fontSize: 15,
    color: COLORS.TEXT_PRIMARY
  },
});
