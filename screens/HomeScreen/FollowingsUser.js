import React from 'react';
import { StyleSheet, View, FlatList } from 'react-native';
import { connect } from 'react-redux';

import { DefText } from '../../components';
import { selectFollowings } from '../../store/auth';
import { ConvertingObjToArr } from '../../utils/ConvertingObjToArr';
import { FollowingUserBlogs } from './FollowingUserBlogs';
import { COLORS } from '../../style/colors';

const mapStateToProps = (state) => ({
    followings: selectFollowings(state),
});

export const FollowingsUser = connect(mapStateToProps)(({followings}) => {
    
    const followingsArr = ConvertingObjToArr(followings);
    const empty = followingsArr.length === 0;
    return (
        <View style = {{backgroundColor: COLORS.BG_PRIMARY}}>
            {!empty ? <DefText style={styles.headerText}>Blogs of People you follow</DefText> : <DefText style = {styles.headerText}>Follow people to discover more blogs ...</DefText>}
            {   !empty &&
                followingsArr.map(following => (
                    <FollowingUserBlogs key = {following.id} following = {following} />
                ))
            }
        </View>
        
        
    )
});

const styles = StyleSheet.create({
    headerText: {
        fontSize: 18,
        alignSelf: "flex-start",
        color: "#a6a09f",
        borderColor: COLORS.TEXT_PRIMARY,
        marginBottom: 10,
    },
})