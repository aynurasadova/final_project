import React from 'react';
import { StyleSheet, View, FlatList } from 'react-native';
import { connect } from 'react-redux';

import { DefText } from '../../components';
import { ConvertingObjToArr } from '../../utils/ConvertingObjToArr';
import { selectSingleUploadedBlog } from '../../store/blogs';
import { FlatListForBlogs } from '../../commonsForBlogs/FlatListForBlogs';
import { COLORS } from '../../style/colors';
import { BlogsList } from '../../commonsForBlogs/BlogsList';
import { selectAppUsers } from '../../store/users';
import { AllBlogsList } from '../../commonsForBlogs/AllBlogsList';

const mapStateToProps = (state, {following}) => ({
    singleUserUploadedBlogs: selectSingleUploadedBlog(state, following?.id),
    users: selectAppUsers(state),
});

export const FollowingUserBlogs = connect(mapStateToProps)(({users, singleUserUploadedBlogs}) => {

    const singleUserUploadedBlogsArr = ConvertingObjToArr(singleUserUploadedBlogs?.blogs || {});
    const empty = singleUserUploadedBlogsArr.length === 0;
    return (
        <>
        {!empty &&
            singleUserUploadedBlogsArr.map(singleUserUpBlog => (
                <AllBlogsList key = {singleUserUpBlog.id} blog = {singleUserUpBlog} contentWidth = {100} users = {users}/>
            ))
        }
        </>
    )
})