import React, { useState } from "react";
import { StyleSheet, View, TouchableOpacity, Alert, Image } from "react-native";
import { connect } from "react-redux";

import { logIn, signUp } from "../../store/auth";
import { ICONS } from "../../style/icons";
import { COLORS } from "../../style/colors";
import { GLOBAL_STYLES } from "../../utils/GLOBAL_STYLES";
import { DefText } from "../../components";
import { AuthInputField } from "./AuthInputField";

export const AuthScreen = connect(null, { logIn, signUp })(
  ({ logIn, signUp }) => {
    const [fields, setFields] = useState({
      email: { value: "", label: "Email" },
      userFirstName: { value: "", label: "First Name" },
      userLastName: { value: "", label: "Last Name" },
      password: { value: "", label: "Password" },
      repassword: { value: "", label: "Repeat Password" },
    });
    const [isSignUp, setIsSignUp] = useState(false);
    const [showPassword, setShowPassword] = useState(false);
    const [showRepeatPassword, setShowRepeatPassword] = useState(false);
    const toggleIsSignUp = () => setIsSignUp((v) => !v);
    const fieldsChangeHandler = (name, value) => {
      setFields((fields) => ({
        ...fields,
        [name]: {
          ...fields[name],
          value,
        },
      }));
    };

    const validateForm = () => {
      if (fields.email.value.trim() === "") {
        Alert.alert("Email required");
        return false;
      }
      if (fields.password.value === "") {
        Alert.alert("Password required");
        return false;
      }
      if (isSignUp) {
        if (fields.password.value !== fields.repassword.value) {
          Alert.alert("Passwords must match");
          return false;
        }
        if (
          fields.userFirstName.value.trim() === "" ||
          fields.userLastName.value.trim() === ""
        ) {
          Alert.alert("Username required");
          return false;
        }
      }

      return true;
    };

    const submit = () => {
      if (validateForm()) {
        if (isSignUp) {
          signUp(
            fields.email.value.trim(),
            fields.password.value.trim(),
            fields.userFirstName.value.trim(),
            fields.userLastName.value.trim()
          );
        } else {
          logIn(fields.email.value.trim(), fields.password.value.trim());
        }
      }
    };

    return (
      <View style={styles.container}>
        <Image source={ICONS.appLogo} style={styles.appLogo} />
        <View style={styles.form}>
          <DefText weight="medium" style={styles.title}>
            {isSignUp ? "Sign Up" : "Welcome back,"}
          </DefText>

          {isSignUp && (
            <View
              style={{
                flexDirection: "row",
                justifyContent: "space-between",
                width: "100%",
              }}
            >
              <AuthInputField
                style = {styles.field}
                value={fields.userFirstName.value}
                label={fields.userFirstName.label}
                onChangeText={(value) =>
                  fieldsChangeHandler("userFirstName", value)
                }
                width="40%"
                maxLength={15}
              />
              <AuthInputField
                style = {styles.field}
                value={fields.userLastName.value}
                label={fields.userLastName.label}
                onChangeText={(value) =>
                  fieldsChangeHandler("userLastName", value)
                }
                width="40%"
                maxLength={20}
              />
            </View>
          )}
          <AuthInputField
            style = {styles.field}
            value={fields.email.value}
            label={fields.email.label}
            onChangeText={(value) => fieldsChangeHandler("email", value)}
          />
          <View style = {styles.passwordField}>
          <AuthInputField
            value={fields.password.value}
            label={fields.password.label}
            secureTextEntry = {showPassword}
            onChangeText={(value) => fieldsChangeHandler("password", value)}
          />
          <TouchableOpacity onPress = {() => setShowPassword(v => !v)}>
            <Image style = {styles.openEyeIcon} source = {!showPassword ? ICONS.openEye : ICONS.closeEye}/>
          </TouchableOpacity>
          </View>
          {isSignUp && (
            <View style = {styles.passwordField}>
            <AuthInputField
              value={fields.repassword.value}
              label={fields.repassword.label}
              secureTextEntry = {showRepeatPassword}
              onChangeText={(value) => fieldsChangeHandler("repassword", value)}
            />
            <TouchableOpacity onPress = {() => setShowRepeatPassword(v => !v)}>
              <Image style = {styles.openEyeIcon} source = {!showRepeatPassword ? ICONS.openEye : ICONS.closeEye}/>
            </TouchableOpacity>
            </View>
          )}
          <TouchableOpacity style={styles.authType} onPress={toggleIsSignUp}>
            <DefText style={styles.authTypeChangerTitle}>
              {isSignUp
                ? "Already have an account ?"
                : "Don't have an account?"}
            </DefText>
          </TouchableOpacity>

          <TouchableOpacity style={styles.submitBtn} onPress={submit}>
            <DefText>{isSignUp ? "Sign up" : "Log in"}</DefText>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: GLOBAL_STYLES.PADDING,
    backgroundColor: COLORS.BG_PRIMARY,
  },
  appLogo: {
    width: 120,
    height: 120,
  },
  form: {
    maxWidth: "100%",
    marginTop: 15,
  },
  title: {
    fontSize: 25,
    color: "white",
    marginVertical: 20,
  },
  authType: {
    marginBottom: 20,
    alignSelf: "flex-end",
  },
  authTypeChangerTitle: {
    color: COLORS.TEXT_PRIMARY,
    paddingRight: 15,
  },
  submitBtn: {
    width: "100%",
    height: 40,
    backgroundColor: COLORS.REGISTER_BTNS,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 10,
  },
  openEyeIcon: {
    height: 30,
    width: 30,
    marginRight: 60
  },
  field: {
    marginBottom: 20,
    height: 43,
    paddingHorizontal: 15,
    borderRadius: 10,
    backgroundColor: "transparent",
    color: "#AAB2BA",
    borderWidth: 1,
    borderColor: "#AAB2BA",
  },
  passwordField: {
    flexDirection: "row",
    width: "100%",
    marginBottom: 20,
    height: 43,
    paddingLeft: 15,
    paddingRight: GLOBAL_STYLES.PADDING * 2,
    alignItems: "center",
    borderRadius: 10,
    backgroundColor: "transparent",
    color: "#AAB2BA",
    borderWidth: 1,
    borderColor: "#AAB2BA",
  },
});
