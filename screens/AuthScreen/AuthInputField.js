import React, { useState } from "react";
import {
  StyleSheet,
  TextInput,
} from "react-native";

export const AuthInputField = ({width, value, label, ...rest}) => {

    return (
            <TextInput
            style={[styles.field, {width: width || "100%"}]}
            value={value}
            placeholderTextColor = "#AAB2BA"
            placeholder = {label}
            {...rest}
          />
    )
  };

const styles = StyleSheet.create({
  field: {
    color: "#AAB2BA"
  },
});
