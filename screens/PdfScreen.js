import React from "react";
import { StyleSheet, Dimensions, View } from "react-native";
import WebView from "react-native-webview";

import { COLORS } from "../style/colors";
import { BTNs } from "../commonsForBlogs/BTNs";
import { GLOBAL_STYLES } from "../utils/GLOBAL_STYLES";

export const PdfScreen = ({
  route: {
    params: { bookUrl },
  },
  navigation,
}) => {
  return (
    <View style={styles.container}>
      <BTNs
        type="back"
        color="white"
        style={styles.backBtn}
        onPress={() => navigation.goBack()}
        size={26}
      />
      <WebView source={{ uri: bookUrl }} style={styles.pdf} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
    backgroundColor: COLORS.BG_PRIMARY,
  },
  backBtn: {
    position: "absolute",
    left: 14,
    top: 12,
    paddingHorizontal: 12,
    paddingVertical: 2,
    backgroundColor: "#4d89f7",
    borderRadius: 2,
  },
  pdf: {},
});
