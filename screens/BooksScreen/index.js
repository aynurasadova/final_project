import React, { useState, useEffect } from "react";
import {
  View,
  StyleSheet,
  FlatList,
  TextInput,
  Image,
  Dimensions,
} from "react-native";
import { connect } from "react-redux";

import { GLOBAL_STYLES } from "../../utils/GLOBAL_STYLES";
import { COLORS } from "../../style/colors";
import { SearchBarStyled } from "../../commons/SearchBar/SerachBarStyled";
import { formatData, searchHandler } from "../../commons/SearchBar";
import { selectBooksList } from "../../store/books";
import { SingleBookModal } from "../../commonsForBooks/SingleBookModal";
import { SingleBook } from "../../commonsForBooks/SingleBook";

const mapStateToProps = (state) => ({
  books: selectBooksList(state),
});

export const BooksScreen = connect(mapStateToProps)(({ navigation, books }) => {
  const [value, setValue] = useState("");
  const [popUp, setPopUp] = useState({});
  const [isPopUpActive, setIsPopUpActive] = useState(false);

  return (
    <>
      <View style={styles.container}>
        <SearchBarStyled
          value={value}
          placeholder="Searching for books..."
          onChangeText={(v) => setValue(v)}
        />

        <FlatList
          columnWrapperStyle={styles.list}
          data={formatData(searchHandler(value, books), 3)}
          numColumns={3}
          keyExtractor = {(item) => item.id}
          showsVerticalScrollIndicator={false}
          renderItem={({ item }) => (
            <SingleBook
              book={item}
              onPress={() => {
                setPopUp(item);
                setIsPopUpActive((v) => !v);
              }}
            />
          )}
        />

        {isPopUpActive && (
          <View style={styles.popUpContainer}>
            <SingleBookModal
              onReleaseUp={() =>
                navigation.navigate("SingleBookScreen", { bookID: popUp.id })
              }
              onReleaseDown={() => setIsPopUpActive(false)}
              book={popUp}
            />
          </View>
        )}
      </View>
    </>
  );
});

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: GLOBAL_STYLES.PADDING,

    backgroundColor: COLORS.BG_PRIMARY,
    paddingTop: 30,
    flex: 1,
    justifyContent: "center",
  },
  list: {
    width: Dimensions.get("screen").width - GLOBAL_STYLES.PADDING * 2,
    justifyContent: "space-between",
  },
  popUpContainer: {
    position: "absolute",
    bottom: 0,
    left: 0,
    width: Dimensions.get("screen").width,
  },
});
