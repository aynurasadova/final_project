import React, { useState, useEffect } from "react";
import { View, StyleSheet, TouchableOpacity, Keyboard } from "react-native";
import { connect } from "react-redux";

import { selectBlogsList } from "../../store/blogs";
import { GLOBAL_STYLES } from "../../utils/GLOBAL_STYLES";
import { searchHandler, formatData } from "../../commons/SearchBar";
import { SearchBarStyled } from "../../commons/SearchBar/SerachBarStyled";
import { FlatListForBlogs } from "../../commonsForBlogs/FlatListForBlogs";
import { DefText } from "../../components";
import { COLORS } from "../../style/colors";

const mapStateToProps = (state) => ({
  blogs: selectBlogsList(state),
});

export const BlogsScreen = connect(mapStateToProps)(({ blogs }) => {
  const [value, setValue] = useState("");
  const [showCancel, setShowCancel] = useState(false);
  const [searchBarWidth, setSearchBarWidth] = useState(100);
  const onChangeText = (v) => {
    setSearchBarWidth(80);
    setShowCancel(true);
    setValue(v);
  };

  const handleCancel = () => {
    Keyboard.dismiss();
    setValue("");
  };

  useEffect(() => {
    if (value.trim() === "") {
      setShowCancel(false);
      setSearchBarWidth(100);
    }
  }, [value]);

  return (
    <View style={styles.container}>
      <View style={styles.searchBarWrapper}>
        <SearchBarStyled
          value={value}
          onChangeText={(v) => onChangeText(v)}
          placeholderText="Searching for blogs ..."
          searchBarWidth={searchBarWidth}
        />
        {showCancel && (
          <TouchableOpacity onPress={handleCancel}>
            <DefText style={styles.cancelText}>Cancel</DefText>
          </TouchableOpacity>
        )}
      </View>
      <FlatListForBlogs
        data={formatData(searchHandler(value, blogs), 1)}
        contentWidth={100}
        numColumns={2}
      />
    </View>
  );
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    // alignItems: 'center',
    backgroundColor: COLORS.BG_PRIMARY,
    paddingHorizontal: GLOBAL_STYLES.PADDING,
    paddingTop: 30,
    zIndex: 2000,
  },
  blogsStyle: {
    // width: Dimensions.get("screen").width - GLOBAL_STYLES.PADDING * 2,
    width: "100%",
  },
  searchBarWrapper: {
    flexDirection: "row",
    width: "100%",
    fontSize: 20,
    justifyContent: "space-between",
  },
  cancelText: {
    paddingVertical: 10,
    color: COLORS.TEXT_PRIMARY,
  },
});
