import * as Google from "expo-google-app-auth";

const clientIDs = {
  androidClientId:
    "543313296849-j752bkrmilh1u1j02tuq2k5htmu5e0a7.apps.googleusercontent.com",
  iosClientId:
    "543313296849-09tbunuq40v22cqq6rdgo4nnv3302kk3.apps.googleusercontent.com",
};

const API_KEY = "AIzaSyDhj0DY29ap018sDPyfYKS9ETS7nr0JiXU";

export async function signInWithGoogleAsync() {
  try {
    const result = await Google.logInAsync({
      scopes: [
        "email",
        "profile",
        "https://www.googleapis.com/auth/calendar.events",
      ],
      androidStandaloneAppClientId:
        "543313296849-613i0n4l0qcn1b5rtu4kouej8eo422p4.apps.googleusercontent.com",
    });
    if (result.type === "success") {
      return result.accessToken;
    } else {
      console.log("cancelled");
    }
  } catch (e) {
    console.log("error", e);
  }
}
export const AddEventToGoogleCalendar = async (accessToken, eventData) => {
  try {
    const calendarsList = await fetch(
      `https://www.googleapis.com/calendar/v3/calendars/primary/events?key=${API_KEY}`,
      {
        method: "POST",
        headers: {
          Authorization: `Bearer ${accessToken}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(eventData),
      }
    );
  } catch (e) {
    console.log("error", e);
  }
};
