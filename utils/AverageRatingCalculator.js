export const averageRatingCalculator = (totalRatings, bookID) => {
  let ratingsArray = [];
  totalRatings.map((user) => {
    for (const book in user) {
      if (book !== "id") {
        let status = false;
        ratingsArray.map((item) => {
          if (item.id === book) {
            item.count = item.count + 1;
            item.sum = item.sum + user[book].rate;
            status = true;
          }
        });
        if (!status)
          ratingsArray.push({ id: book, sum: user[book].rate, count: 1 });
      }
    }
  });
  return ratingsArray.find((book) => book.id === bookID);
};
