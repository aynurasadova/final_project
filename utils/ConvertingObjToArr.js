export const ConvertingObjToArr = (objects) => {
    let arrays;
    arrays = Object.keys(objects).map((key) => ({
        id: key,
        ...objects[key],
    }));
    return arrays;
};