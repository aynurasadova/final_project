export const likesCounter = (likeLength) => {
  if (likeLength < 1000) {
    return `${likeLength} `;
  } else if (likeLength >= 1000 && likeLength < 1000000) {
    return `${likeLength / 1000}k `;
  } else if (likeLength >= 1000000) {
    return `${likeLength / 1000000}M `;
  }
};
