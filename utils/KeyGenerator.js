import React from "react";
import { Alert } from "react-native";

import { ButtonwithLabel } from "../commons";
import fbApp from "./firebaseInit";

export const KeyGenerator = () => {
  const pressHandler = () => {
    const newBookID = fbApp.db.ref("books").push().key;

    const updates = {
      [`books/${newBookID}`]: {
        title: `book 1`,
        author: `author 1`,
        bookUrl: `booksUrl`,
        coverImgUrl: `something.png`,
        desc: `description`,
      },
    };
    fbApp.db.ref().update(updates, (err) => {
      if (err) {
        Alert.alert("Fail", err.message);
      } else {
        Alert.alert("Success", `New book key created`);
      }
    });
  };
  return (
    <>
      <ButtonwithLabel title="Generate key for Books" onPress={pressHandler} />
    </>
  );
};
