import * as firebase from "firebase";

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBE_hZ-gmZerjuiVhIS9pzfzGZZmLePHos",
  authDomain: "young-net-app.firebaseapp.com",
  databaseURL: "https://young-net-app.firebaseio.com",
  projectId: "young-net-app",
  storageBucket: "young-net-app.appspot.com",
  messagingSenderId: "543313296849",
  appId: "1:543313296849:web:2bce2f590b67475ab774f0",
  measurementId: "G-TX131GN0YD",
};
// Initialize Firebase
try {
  firebase.initializeApp(firebaseConfig);
} catch (err) {
  // we skip the "already exists" message which is
  // not an actual error when we're hot-reloading
  if (!/already exists/.test(err.message)) {
    console.error("Firebase initialization error", err.stack);
  }
}

    const fbApp = {
        db: firebase.database(),
        auth: firebase.auth(),
        storage: firebase.storage(),
    }
    
    export default fbApp;
