import fbApp from "../utils/firebaseInit";
import { selectAuthUserID } from "./auth";
import { ConvertingObjToArr } from "../utils/ConvertingObjToArr";

const SET_BLOGS = "SET_BLOGS";
const SET_ADDED_TO_LATER_BLOGS = "SET_ADDED_TO_LATER_BLOGS";
const SET_FAVORITE_BLOGS = "SET_FAVORITE_BLOGS";
const SET_BLOG_PHOTOS = "SET_BLOG_PHOTOS";
const SET_UPLOADED_BLOGS = "SET_UPLOADED_BLOGS";
const SET_ALL_UPLOADED_BLOGS = "SET_ALL_UPLOADED_BLOGS";
const SET_BLOG_COMMENT_REPLY = "SET_BLOG_COMMENT_REPLY";

const CLEAR_BLOGS = "CLEAR_BLOGS";
const CLEAR_FAVORITES = "CLEAR_FAVORITES";
const CLEAR_ADDED_TO_LATERS = "CLEAR_ADDED_TO_LATERS";
const CLEAR_UPLOADED_BLOGS = "CLEAR_UPLOADED_BLOGS";
const CLEAR_ALL_UPLOADED_BLOGS = "CLEAR_ALL_UPLOADED_BLOGS";
const CLEAR_BLOG_COMMENT_REPLY = "CLEAR_BLOG_COMMENT_REPLY";

//SELECTORS

export const MODULE_NAME = "blogs";
export const selectBlogsList = (state) => state[MODULE_NAME]?.blogs;
export const selectSingleByID = (state, ID) =>
  selectBlogsList(state).find((blog) => blog.id === ID);

export const selectAddedToLaterBlogs = (state) =>
  state[MODULE_NAME]?.addedToLaterBlogs;
export const selectAddedToLaterBlogByID = (state, ID) =>
  selectAddedToLaterBlogs(state).find((blog) => blog.id === ID);

export const selectBlogLikes = (state, id) =>
  state[MODULE_NAME].blogs.find((blog) => blog.id === id)?.likes || {};
export const selectFavoriteBlogs = (state) => state[MODULE_NAME]?.favoritesBlogs;

export const selectBlogComments = (state, id) => state[MODULE_NAME]?.blogs.find((blog) => blog.id === id)?.comments || {};

export const selectBlogPhotos = (state) => state[MODULE_NAME]?.blogPhotos;
export const selectSingleBlogPhoto = (state, ID) => selectBlogPhotos(state).find((blog) => blog.id === ID);

export const selectUploadedBlogs = (state) => state[MODULE_NAME]?.uploadedBlogs;
export const selecetUploadedBlogByID = (state, id) => selectUploadedBlogs(state).find((blog) => blog.id === id);

export const selectAllUploadedBlogs = (state) => state[MODULE_NAME]?.allUploadedBlogs;
export const selectSingleUploadedBlog = (state, id) => selectAllUploadedBlogs(state).find((blog) => blog.id === id);

export const selectBlogCommentReplies = (state) => state[MODULE_NAME]?.blogCommentReplies;
export const selectBlogSingleCommentReplies = (state, id) => selectBlogCommentReplies(state).find((comment) => comment.id === id)?.replies || {};

//REDUCER

const initialState = {
  blogs: [],
  favoritesBlogs: [],
  addedToLaterBlogs: [],
  blogPhotos: [],
  uploadedBlogs: [],
  allUploadedBlogs: [],
  blogCommentReplies: [],
};

export function blogsReducer(state = initialState, { type, payload }) {
  switch (type) {
    case SET_BLOGS:
      return {
        ...state,
        blogs: payload,
      };
    case SET_BLOG_PHOTOS:
      return {
        ...state,
        blogPhotos: payload,
      }
    case SET_ADDED_TO_LATER_BLOGS:
      return {
        ...state,
        addedToLaterBlogs: payload,
      };
    case SET_FAVORITE_BLOGS:
      return {
        ...state,
        favoritesBlogs: payload,
      };
    case SET_UPLOADED_BLOGS:
      return {
        ...state,
        uploadedBlogs: payload,
      }
    case SET_ALL_UPLOADED_BLOGS:
      return {
        ...state,
        allUploadedBlogs: payload,
      }
    case SET_BLOG_COMMENT_REPLY:
      return {
        ...state,
        blogCommentReplies: payload,
      }
    case CLEAR_BLOGS:
      return {
        ...state,
        blogs: [],
      };
    case CLEAR_FAVORITES:
      return {
        ...state,
        favoritesBlogs: [],
      };
    case CLEAR_ADDED_TO_LATERS:
      return {
        ...state,
        addedToLaterBlogs: [],
      };
    case CLEAR_UPLOADED_BLOGS:
      return {
        ...state,
        uploadedBlogs: [],
      }
    case CLEAR_ALL_UPLOADED_BLOGS:
      return {
        ...state,
        allUploadedBlogs: [],
      }
    case CLEAR_BLOG_COMMENT_REPLY:
      return {
        ...state,
        blogCommentReplies: [],
      }
    default:
      return state;
  }
}

export const setBlogs = (payload) => ({
  type: SET_BLOGS,
  payload,
});

export const setBlogPhotos = (payload) => ({
  type: SET_BLOG_PHOTOS,
  payload,
})

export const setAddedToLaterBlogs = (payload) => ({
  type: SET_ADDED_TO_LATER_BLOGS,
  payload,
});

export const setFavoriteBlogs = (payload) => ({
  type: SET_FAVORITE_BLOGS,
  payload,
});

export const setUploadedBlogs = (payload) => ({
  type: SET_UPLOADED_BLOGS,
  payload,
});

export const setAllUploadedBlogs = (payload) => ({
  type: SET_ALL_UPLOADED_BLOGS,
  payload,
});

export const setBlogCommentReply = (payload) => ({
  type: SET_BLOG_COMMENT_REPLY,
  payload,
});

export const clearBlogs = () => ({
  type: CLEAR_BLOGS,
});

export const clearFavorites = () => ({
  type: CLEAR_FAVORITES,
});

export const clearAddedToLaters = () => ({
  type: CLEAR_ADDED_TO_LATERS,
});

export const clearUploadedBlogs = () => ({
  type: CLEAR_UPLOADED_BLOGS,
});

export const clearAllUploadedBlogs = () => ({
  type: CLEAR_ALL_UPLOADED_BLOGS,
});

export const clearBlogCommentReply = () => ({
  type: CLEAR_BLOG_COMMENT_REPLY,
});


//MIDDLEWARES

export const getAndListenForBlogs = () => (dispatch) => {
  try {
    const reference = fbApp.db.ref("blogs");
    reference.on(
      "value",
      (snap) => {
        if (snap.exists()) {
          const blogsObject = snap.val();
          //converting object of Blogs to an array
          const blogsArray = Object.keys(blogsObject).map((key) => ({
            id: key,
            ...blogsObject[key],
          }));

          dispatch(setBlogs(blogsArray));
        } else {
          dispatch(setBlogs([])); 
        }
      },
      (error) => {
        console.log("getAndListenForBlogs error", error);
      }
    );
    return () => reference.off();
  } catch (error) {
    console.log("getAndListenForBlogs error", error.message);
  }
};

export const getAndListenForAddedToLaterBlogs = () => (dispatch, getState) => {
  try {
    const userID = selectAuthUserID(getState());
    const reference = fbApp.db.ref(`addAddedToLater/${userID}/blogs`);
    reference.on(
      "value",
      (snap) => {
        if (snap.exists()) {
          const addedToLaterBlogsObject = snap.val();
          const addedToLaterBlogsArray = Object.keys(
            addedToLaterBlogsObject
          ).map((key) => ({
            id: key,
            ...addedToLaterBlogsObject[key],
          }));

          dispatch(setAddedToLaterBlogs(addedToLaterBlogsArray));
        } else {
          dispatch(setAddedToLaterBlogs([]));
        }
      },
      (err) => {
        console.log("getAndListenForFavoriteBlogs error", err.message);
      }
    );
    return () => reference.off();
  } catch (error) {
    console.log("getAndListenForFavoriteBlogs error", error.message);
  }
};

export const getAndListenForFavoriteBlogs = () => (dispatch, getState) => {
  try {
    const userID = selectAuthUserID(getState());
    const reference = fbApp.db.ref(`favoritesBlogs/${userID}/blogs`);
    reference.on(
      "value",
      (snap) => {
        if (snap.exists()) {
          const favoritesObj = snap.val();
          const favoritesArr = Object.keys(favoritesObj).map((key) => ({
            id: key,
            ...favoritesObj[key],
          }));
          dispatch(setFavoriteBlogs(favoritesArr));
        } else {
          dispatch(setFavoriteBlogs([]));
        }
      },
      (err) => {
        console.log("getAndListenForFavoriteBlogs error", err.message);
      }
    );
    return () => reference.off();
  } catch (error) {
    console.log("getAndListenForFavoriteBlogs error", error);
  }
};

export const getAndListenForUploadedBlogs = () => (dispatch, getState) => {
  try {
    const userID = selectAuthUserID(getState());
    const reference = fbApp.db.ref(`uploadedBlogs/${userID}/blogs`);
    reference.on("value", snap => {
      if(snap.exists()) {
        const uploadedObj = snap.val();
        const uploadedArr = Object.keys(uploadedObj).map((key) => ({
          id: key,
          ...uploadedObj[key],
        }));
        dispatch(setUploadedBlogs(uploadedArr))
      } else {
        dispatch(setUploadedBlogs([]));
      }
    },(err) => {
      console.log("getAndListenForFavoriteBlogs error", err.message);
    });
    return () => reference.off();
  } catch (error) {
    console.log("getAndListenForUploadedBlogs error", error)
  }
}

export const getAndListenForAllUploadedBlogs = () => (dispatch) => {
  try {
    const reference = fbApp.db.ref(`uploadedBlogs`);
    reference.on("value", snap => {
      if(snap.exists()) {
        const uploadedObj = snap.val();
        const uploadedArr = ConvertingObjToArr(uploadedObj);
        dispatch(setAllUploadedBlogs(uploadedArr))
      } else {
        dispatch(setAllUploadedBlogs([]));
      }
    },(err) => {
      console.log("getAndListenForAllUploadedBlogs error", err.message);
    });
    return () => reference.off();
  } catch (error) {
    console.log("getAndListenForAllUploadedBlogs error", error)
  }
};

export const getAndListenForBlogCommentReplies = () => (dispatch) => {
  try {
    const reference = fbApp.db.ref(`blogCommentsReply`);

    reference.on("value", snap => {
      if(snap.exists()) {
        const blogCommentReplyObj = snap.val();
        const blogCommentReplyArr = ConvertingObjToArr(blogCommentReplyObj);
        dispatch(setBlogCommentReply(blogCommentReplyArr));
      } else {
        dispatch(setBlogCommentReply([]));
      }
    }, err => {
      console.log("getAndListenForBlogCommentReplies error", err.message)
    });
    return () => reference.off();
  } catch (error) {
    console.log("getAndListenForBlogCommentReplies error", error)
  }
}

export const createBlog = (title, content, url) => async(dispatch, getState) => {
  try {
    const ref = fbApp.db.ref(`blogs`).push();
    // take image by uri form file system
    const response = await fetch(url);
    // transform downloaded image to blob obj
    const blob = await response.blob();
    // Upload image to firebase storage
    const snap = await fbApp.storage.ref((await ref).key).put(blob);
    // Get uploaded image url on success
    const imgUri = await snap.ref.getDownloadURL();

    const userID = selectAuthUserID(getState());
    
    fbApp.db.ref(`uploadedBlogs/${userID}/blogs/${(await ref).key}`).update({
      stautus: true
    });
    const blogContent = {
      author: userID,
      title,
      content,
      blogPhoto: imgUri,
    };

    ref.set(blogContent, (err) => {
      if (err) {
        console.log(err);
      }
    });
  } catch (error) {
    console.log("createBlog error", error);
  }
};

export const deleteBlog = (userID, blogID, isLiked, isAdded) => async () => {
  try {
    fbApp.db.ref(`blogs/${blogID}`).remove();
    console.log("userID redux", userID);

    fbApp.db.ref(`uploadedBlogs/${userID}/blogs/${blogID}`).remove();
    !!isAdded && fbApp.db.ref(`favoritesBlogs/${userID}/blogs/${blogID}`).remove();
    !!isLiked && fbApp.db.ref(`addAddedToLater/${userID}/blogs/${blogID}`).remove();
  } catch (error) {
    console.log("deleteBlog error", error)
  }
}

export const addAddedToLater = (userID, blogID, isAdded) => async () => {
  try {
    !isAdded
      ? fbApp.db.ref(`addAddedToLater/${userID}/blogs/${blogID}`).update({
          status: true,
        })
      : fbApp.db.ref(`addAddedToLater/${userID}/blogs/${blogID}`).remove();
  } catch (error) {
    console.log("addAddedToLater error", error);
  }
};

export const addToFavBlog = (userID, blogID, isLiked) => async () => {
  try {
    !isLiked
      ? fbApp.db.ref(`favoritesBlogs/${userID}/blogs/${blogID}`).update({
          status: true,
        })
      : fbApp.db.ref(`favoritesBlogs/${userID}/blogs/${blogID}`).remove();
  } catch (error) {
    console.log("addToFavBlog error", error);
  }
};

export const addBlogComment = (userID, blogID, commentText) => async () => {
  try {
    const referenceToBlog = `blogs/${blogID}/comments`;
    fbApp.db.ref(`${referenceToBlog}`).push().set({
      author: userID,
      comment: commentText,
    })
  } catch (error) {
    console.log("addBlogComment error", error)
  }
}

export const toggleLikes = (userID, blogID, isLiked) => async () => {
  try {
    const referenceToBlog = `blogs/${blogID}/likes/${userID}`;
    !isLiked
      ? fbApp.db.ref(`${referenceToBlog}`).update({
          like: true,
        })
      : fbApp.db.ref(`${referenceToBlog}`).remove();
  } catch (error) {
    console.log("toggleLikes error", error);
  }
};

export const addBlogCommentRelpy = (authorID, commentID, commentReplyText) => async() => {
  try {
    const referenceToComment = `blogCommentsReply/${commentID}/replies`;

    fbApp.db.ref(`${referenceToComment}`).push().set({
      author: authorID,
      commentReplyText: commentReplyText,
    })
  } catch (error) {
    console.log("addBlogCommentRelpy error", error.message)
  }
}
