import fbApp from "../utils/firebaseInit";
import { selectAuthUserID } from "./auth";
import { ConvertingObjToArr } from "../utils/ConvertingObjToArr";

const SET_BOOK = "SET_BOOK";
const SET_BOOK_RATING = "SET_BOOK_RATING";
const SET_FAVORITE_BOOK = "SET_FAVORITE_BOOK";
const SET_ADDED_TO_LATER_BOOKS = "SET_ADDED_TO_LATER_BOOKS";
const SET_TOTAL_RATINGS = "SET_TOTAL_RATINGS";
const SET_BOOK_COMMENT_REPLY = "SET_BOOK_COMMENT_REPLY";

//SELECTORS

export const MODULE_NAME = "books";

export const selectBooksList = (state) => state[MODULE_NAME].books;
export const selectSingleByID = (state, ID) => selectBooksList(state).find((book) => book.id === ID);

export const selectFavorites = (state) => state[MODULE_NAME].favoritesBooks;
export const selectSingleBookFavoriteByID = (state, ID) => selectFavorites(state).find((fav) => fav.id === ID);

export const selectAddedToLaterBooks = (state) => state[MODULE_NAME].addedToLaterBooks;
export const selectAddedToLaterBookByID = (state, ID) => selectAddedToLaterBooks(state).find((book) => book.id === ID);

export const selecBooksRating = (state) => state[MODULE_NAME].booksRating;
export const selecBooksRatingByID = (state, ID) => selecBooksRating(state).find((book) => book.id === ID);
export const selectTotalRatings = (state) => state[MODULE_NAME].totalRatings;

export const selectBookComments = (state, id) =>
  state[MODULE_NAME].books.find((book) => book.id === id)?.comments || {};

export const selectBookCommentReplies = (state) => state[MODULE_NAME]?.bookCommentReplies;
export const selectBookSingleCommentReplies = (state, id) => selectBookCommentReplies(state).find((comment) => comment.id === id)?.replies || {};
  
//REDUCER

const initialState = {
  books: [],
  favoritesBooks: [],
  addedToLaterBooks: [],
  booksRating: [],
  totalRatings: [],
  bookCommentReplies: [],
};

export function booksReducer(state = initialState, { type, payload }) {
  switch (type) {
    case SET_BOOK:
      return {
        ...state,
        books: payload,
      };
    case SET_FAVORITE_BOOK:
      return {
        ...state,
        favoritesBooks: payload,
      };
    case SET_ADDED_TO_LATER_BOOKS:
      return {
        ...state,
        addedToLaterBooks: payload,
      };
    case SET_BOOK_RATING:
      return {
        ...state,
        booksRating: payload,
      };
    case SET_TOTAL_RATINGS:
      return {
        ...state,
        totalRatings: payload,
      };
    case SET_BOOK_COMMENT_REPLY:
      return {
        ...state,
        bookCommentReplies: payload,
      }
    default:
      return state;
  }
}

export const setBook = (payload) => ({
  type: SET_BOOK,
  payload,
});

export const setBookRating = (payload) => ({
  type: SET_BOOK_RATING,
  payload,
});

export const setTotalRatings = (payload) => ({
  type: SET_TOTAL_RATINGS,
  payload,
});

export const setFavoriteBook = (payload) => ({
  type: SET_FAVORITE_BOOK,
  payload,
});

export const setAddedToLaterBooks = (payload) => ({
  type: SET_ADDED_TO_LATER_BOOKS,
  payload,
});

export const setBookCommentReply = (payload) => ({
  type: SET_BOOK_COMMENT_REPLY,
  payload,
});

export const getAndListenForBooks = () => (dispatch) => {
  try {
    const reference = fbApp.db.ref("books");
    reference.on(
      "value",
      (snap) => {
        if (snap.exists()) {
          const booksObject = snap.val();
          const booksArray = Object.keys(booksObject).map((key) => ({
            id: key,
            ...booksObject[key],
          }));

          dispatch(setBook(booksArray));
        }
      },
      (error) => {
        console.log("getAndListenForBooks error", error);
      }
    );
    return () => reference.off();
  } catch (error) {
    console.log("getAndListenForBooks error", error.message);
  }
};

export const getAndListenForBooksRating = (all = false) => (dispatch, getState) => {
  try {
    const userID = selectAuthUserID(getState());
    const reference = fbApp.db.ref(`booksRating/${all ? "" : userID}`);
    reference.on(
      "value",
      (snap) => {
        if (snap.exists()) {
          const booksRatingObject = snap.val();
          const booksRatingArray = Object.keys(booksRatingObject).map((key) => ({
            id: key,
            ...booksRatingObject[key],
          }));
          all ? dispatch(setTotalRatings(booksRatingArray)) : dispatch(setBookRating(booksRatingArray));
        } else {
          all ? dispatch(setTotalRatings([])) : dispatch(setBookRating([]));
        }
      },
      (error) => {
        console.log("getAndListenForBooksRating error", error);
      }
    );
    return () => reference.off();
  } catch (error) {
    console.log("getAndListenForBooksRating error", error.message);
  }
};

export const getAndListenForFavoriteBooks = () => (dispatch, getState) => {
  try {
    const userID = selectAuthUserID(getState());
    const reference = fbApp.db.ref(`favoritesBooks/${userID}`);
    reference.on(
      "value",
      (snap) => {
        if (snap.exists()) {
          const favoritesBooksObject = snap.val();
          const favoritesBooksArray = Object.keys(favoritesBooksObject).map((key) => ({
            id: key,
            ...favoritesBooksObject[key],
          }));

          dispatch(setFavoriteBook(favoritesBooksArray));
        } else {
          dispatch(setFavoriteBook([]));
        }
      },
      (err) => {
        console.log("getAndListenForFavoriteBooks error", err.message);
      }
    );
    return () => reference.off();
  } catch (error) {
    console.log("getAndListenForFavoriteBooks error", error.message);
  }
};

export const getAndListenForAddedToLaterBooks = () => (dispatch, getState) => {
  try {
    const userID = selectAuthUserID(getState());
    const reference = fbApp.db.ref(`addedToLaterBooks/${userID}`);
    reference.on(
      "value",
      (snap) => {
        if (snap.exists()) {
          const addedToLaterBooksObject = snap.val();
          const addedToLaterBooksArray = Object.keys(addedToLaterBooksObject).map((key) => ({
            id: key,
            ...addedToLaterBooksObject[key],
          }));

          dispatch(setAddedToLaterBooks(addedToLaterBooksArray));
        } else {
          dispatch(setAddedToLaterBooks([]));
        }
      },
      (err) => {
        console.log("getAndListenForAddedToLaterBooks error", err.message);
      }
    );
    return () => reference.off();
  } catch (error) {
    console.log("getAndListenForAddedToLaterBooks error", error.message);
  }
};

export const getAndListenForBookCommentReplies = () => (dispatch) => {
  try {
    const reference = fbApp.db.ref(`bookCommentsReply`);

    reference.on("value", snap => {
      if(snap.exists()) {
        const bookCommentReplyObj = snap.val();
        const bookCommentReplyArr = ConvertingObjToArr(bookCommentReplyObj);
        dispatch(setBookCommentReply(bookCommentReplyArr));
      } else {
        dispatch(setBookCommentReply([]));
      }
    }, err => {
      console.log("getAndListenForBookCommentReplies error", err.message)
    });
    return () => reference.off();
  } catch (error) {
    console.log("getAndListenForBookCommentReplies error", error)
  }
};

export const addBookComment = (userID, bookID, commentText) => async () => {
  try {
    const referenceToBook = `books/${bookID}/comments`;
    fbApp.db.ref(referenceToBook).push().set({
      author: userID,
      comment: commentText,
    });
  } catch (error) {
    console.log("addBookComment error", error);
  }
};

export const addBookCommentRelpy = (authorID, commentID, commentReplyText) => async() => {
  try {
    const referenceToComment = `bookCommentsReply/${commentID}/replies`;

    fbApp.db.ref(`${referenceToComment}`).push().set({
      author: authorID,
      commentReplyText: commentReplyText,
    })
  } catch (error) {
    console.log("addBookCommentRelpy error", error.message)
  }
};

export const addReply = (userID, bookID, replyText, commentID) => async () => {
  try {
    const referenceToBook = `books/${bookID}/comments/${commentID}/replies`;
    fbApp.db.ref(referenceToBook).push().set({
      author: userID,
      comment: replyText,
    });
  } catch (error) {
    console.log("addReply error", error);
  }
};

// const changeLike = (commentID) => {
//   const reference = fbApp.db.ref(`books/${bookID}/comments/${commentID}/likes/${userID}`);
//   !!comments[commentID]?.likes
//     ? !!comments[commentID]?.likes[userID]
//       ? reference.remove()
//       : reference.update({
//           like: true,
//         })
//     : reference.update({
//         like: true,
//       });
// };
// const changeDislike = (commentID) => {
//   const reference = fbApp.db.ref(`books/${bookID}/comments/${commentID}/dislikes/${userID}`);
//   !!comments[commentID]?.dislikes
//     ? !!comments[commentID]?.dislikes[userID]
//       ? reference.remove()
//       : reference.update({
//           dislike: true,
//         })
//     : reference.update({
//         dislike: true,
//       });
// };

// const onPressLikeHandler = (commentID) => {
//   changeLike(commentID);
//   if (!!comments[commentID]?.dislikes)
//     if (!!comments[commentID]?.dislikes[userID]?.dislike) changeDislike(commentID);
// };

// const onPressDislikeHandler = (commentID) => {
//   changeDislike(commentID);
//   if (!!comments[commentID]?.likes) if (!!comments[commentID]?.likes[userID]?.like) changeLike(commentID);
// };
