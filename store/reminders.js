import fbApp from "../utils/firebaseInit";
import { selectAuthUserID } from "./auth";

const SET_REMINDERS = "SET_REMINDERS";
const CLEAR_REMINDERS = "CLEAR_REMINDERS";

export const MODULE_NAME = "reminders";
export const selectReminders = (state) => state[MODULE_NAME].reminders;
const initialState = {
  reminders: [],
};

export function remindersReducer(state = initialState, { type, payload }) {
  switch (type) {
    case SET_REMINDERS:
      return {
        ...state,
        reminders: payload,
      };
    case CLEAR_REMINDERS:
      return {
        ...state,
        reminders: [],
      };
    default:
      return state;
  }
}

export const setReminders = (payload) => ({
  type: SET_REMINDERS,
  payload,
});
export const clearReminders = () => ({
  type: CLEAR_REMINDERS,
});

export const getAndListenForReminders = () => (dispatch, getState) => {
  try {
    const userID = selectAuthUserID(getState());
    const reference = fbApp.db.ref(`users/${userID}/reminders`);
    reference.on(
      "value",
      (snap) => {
        if (snap.exists()) {
          const remindersObject = snap.val();

          //converting object of reminders to an array
          const remindersArray = Object.keys(remindersObject).map((key) => ({
            id: key,
            ...remindersObject[key],
          }));
          dispatch(setReminders(remindersArray));
        } else {
          dispatch(setReminders([]));
        }
      },
      (error) => {
        console.log("getAndListenForReminders error", error);
      }
    );
    return () => reference.off();
  } catch (error) {
    console.log("getAndListenForReminders error", error.message);
  }
};

export const addReminder = (
  summary,
  description,
  startDateTime,
  endDateTime
) => (dispatch, getState) => {
  try {
    const userID = selectAuthUserID(getState());
    const ref = fbApp.db.ref(`users/${userID}/reminders`);

    const reminderData = {
      summary,
      description,
      start: {
        dateTime: startDateTime,
      },
      end: {
        dateTime: endDateTime,
      },
    };

    ref.push().set(reminderData, (err) => {
      if (err) {
        console.log(err);
      }
    });
  } catch (error) {
    console.log("addReminder error", error);
  }
};
export const deleteReminder = (reminderID) => (dispatch, getState) => {
  try {
    const userID = selectAuthUserID(getState());
    const ref = fbApp.db.ref(`users/${userID}/reminders/${reminderID}`);
    ref.remove();
  } catch (error) {
    console.log("delete reminder error", error);
  }
};
