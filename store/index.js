import React from "react";
import { combineReducers, createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import { composeWithDevTools } from "redux-devtools-extension";

import { MODULE_NAME as blogsModuleName, blogsReducer } from "./blogs";
import { MODULE_NAME as authModuleName, authReducer } from "./auth";
import { MODULE_NAME as usersModuleName, appUsersReducer } from "./users";
import { MODULE_NAME as booksModuleName, booksReducer } from "./books";
import {
  MODULE_NAME as remindersModuleName,
  remindersReducer,
} from "./reminders";

import { AsyncStorage } from "react-native";
import { persistReducer, persistStore } from "redux-persist";

const config = {
  key: "root",
  storage: AsyncStorage,
};

const rootReducer = combineReducers({
  [blogsModuleName]: blogsReducer,
  [authModuleName]: authReducer,
  [usersModuleName]: appUsersReducer,
  [booksModuleName]: booksReducer,
  [remindersModuleName]: remindersReducer,
});

const rootPersistReducer = persistReducer(config, rootReducer);

const store = createStore(
  rootPersistReducer,
  composeWithDevTools(applyMiddleware(thunk))
);

export const persistor = persistStore(store);

export default store;
