import fbApp from "../utils/firebaseInit";
import { selectAuthUserID } from "./auth";

const SET_APP_USERS = "SET_APP_USERS";

export const MODULE_NAME = "users";
export const selectAppUsers = (state) => state[MODULE_NAME].users;

const initialState = {
    users: [],
};

export function appUsersReducer(state = initialState, {type, payload} ) {
    switch (type) {
        case SET_APP_USERS:
            return {
                ...state,
                users: payload,
            }
        default:
            return state;
    }
};

export const setAppUsers = (payload) => ({
    type: SET_APP_USERS,
    payload,
});

export const getAndListenAppUsers = () => (dispatch) => {
    try {
        const ref = fbApp.db.ref("users");
        ref.on("value", snap => {
            if(snap.exists()) {
                dispatch(setAppUsers(snap.val()))
            }
        });
        return () => ref.off();
    } catch (error) {
        console.log("getAndListenAppUsers error", error)
    }
};
export const editProfile = (userFirstName, userLastName, url, photoChangeStatus) => async(dispatch, getState) => {
    try {
        const userID = selectAuthUserID(getState());
        const response = await fetch(url);
        const blob = await response.blob();
        const snap = await fbApp.storage.ref(userID).put(blob);
        const imgUri = await snap.ref.getDownloadURL();
        photoChangeStatus && fbApp.db.ref(`users/${userID}/profilePhoto`).set(imgUri);
        fbApp.db.ref(`users/${userID}/userFirstName`).set(userFirstName);
        fbApp.db.ref(`users/${userID}/userLastName`).set(userLastName);
    } catch (error) {
        console.log("editProfile error", error)
    }
};

export const followingStates = (otherUserID, isFollowed) => (dispatch, getState) => {
    try {
        const userID = selectAuthUserID(getState());
        !isFollowed ? fbApp.db.ref(`users/${userID}/followings/${otherUserID}`).update({status: true}) : fbApp.db.ref(`users/${userID}/followings/${otherUserID}`).remove();
        !isFollowed ? fbApp.db.ref(`users/${otherUserID}/followers/${userID}`).update({status: true}) : fbApp.db.ref(`users/${otherUserID}/followers/${userID}`).remove() ;
    } catch (error) {
        console.log("followingStates error", error)
    }
}

