import fbApp from "../utils/firebaseInit";
import { Alert } from "react-native";

const SET_AUTH_STATUS = "SET_AUTH_STATUS";
const SET_AUTH_SUCCESS = "SET_AUTH_SUCCESS";
const SET_AUTH_LOGOUT = "SET_AUTH_LOGOUT";

export const MODULE_NAME = "auth";

export const selectAuthStatus = (state) => state[MODULE_NAME].status;
export const selectUserInfo = (state) => state[MODULE_NAME].userInfo;
export const selectAuthUserID = (state) => state[MODULE_NAME].userInfo.userID;
export const selectAuthUserFirstName = (state) => state[MODULE_NAME].userInfo.userFirstName;
export const selectAuthUserLastName = (state) => state[MODULE_NAME].userInfo.userLastName;
export const selectProfilePhoto = (state) => state[MODULE_NAME].userInfo.profilePhoto;
export const selectFollowers = (state) => state[MODULE_NAME].userInfo?.followers || {};
export const selectFollowings = (state) => state[MODULE_NAME].userInfo?.followings || {};

const initialState = {
    status: false,
    userInfo: {
        userID: null,
        email: null,
        userFirstName: null,
        userLastName: null,
        profilePhoto: null,
        followings: null,
        followers: null,
    },
};

export function authReducer (state = initialState, {type, payload}) {
    switch (type) {
        case SET_AUTH_STATUS:
            return {
                ...state,
                status: payload,
            }
        case SET_AUTH_SUCCESS:
            return {
                ...state,
                status: true,
                userInfo: {
                    ...state.userInfo,
                    userID: payload.userID,
                    email: payload.email,
                    userFirstName: payload.userFirstName,
                    userLastName: payload.userLastName,
                    profilePhoto: payload.profilePhoto,
                    followings: payload.followings,
                    followers: payload.followers,
                }
            }
        case SET_AUTH_LOGOUT: 
            return {
                ...state,
                status: false,
                userInfo: {
                    userID: null,
                    email: null,
                    userFirstName: null,
                    userLastName: null,
                    profilePhoto: null,
                    followers: null,
                    followings: null,
                }
            }
        default:
            return state;
    }
}

export const setAuthStatus = (payload) => ({
    type: SET_AUTH_STATUS,
    payload,
});

export const setAuthSuccess = (payload) => ({
    type: SET_AUTH_SUCCESS,
    payload,
});

export const setAuthLogOut = () => ({
    type: SET_AUTH_LOGOUT,
});

export const logIn = (email, password) => async(dispatch) => {
    try {
        const {
          user: { uid },
        } = await fbApp.auth.signInWithEmailAndPassword(email, password);
    
        const userDataSnapshot = await fbApp.db.ref(`users/${uid}`).once("value");

        dispatch(setAuthSuccess({ userID: uid }))

    } catch (error) {
        Alert.alert("Login error", error.message)
    }

};

export const signUp = (email, password, userFirstName, userLastName) => async(dispatch) => {
    try {
        const {user: {uid}} = await fbApp.auth.createUserWithEmailAndPassword(email, password);
        fbApp.db.ref(`users/${uid}`).set({
            userID: uid,
            userFirstName,
            userLastName,
            email,
            profilePhoto: "https://firebasestorage.googleapis.com/v0/b/young-net-app.appspot.com/o/defaultAvatar.png?alt=media&token=f658b742-fd4f-44a1-8c29-7e2d6a9f4e9f",
            followers: {},
            followings: {},
        });

        dispatch(setAuthSuccess({userID: uid, userFirstName, userLastName, email}))
    } catch (error) {
        Alert.alert("Sign up error", error.message)
    }
};

export const getAndListenUserInfo = () => (dispatch, getState) => {
    try {
        const userID = selectAuthUserID(getState());
        const reference = fbApp.db.ref(`users/${userID}`);
        reference.on(
        "value",
        (snap) => {
            if (snap.exists()) {
            const userInfoObj = snap.val();
            dispatch(setAuthSuccess({
                userID: userID,
                profilePhoto: userInfoObj.profilePhoto,
                userFirstName: userInfoObj.userFirstName,
                userLastName: userInfoObj.userLastName,
                email: userInfoObj.email,
                followings: userInfoObj?.followings || {},
                followers: userInfoObj?.followers || {},
            }));
            } else {
            console.log("userInfoObj doesnt exist")
            }
        },
        (err) => {
            console.log("getAndListenUserInfo error", err.message);
        }
        );
        return () => reference.off();
    } catch (error) {
        console.log("getAndListenUserInfo error", error)
    }
}

export const logOut = () => async(dispatch) => {
    try {
        await fbApp.auth.signOut();
        dispatch(setAuthLogOut());
    } catch (error) {
        console.log("logOut error", error.message)
    }
}