import React from 'react';
import { View, TouchableOpacity, StyleSheet} from 'react-native';

import { AntDesign, MaterialIcons } from '@expo/vector-icons';
import { DefText } from '../components';
import { COLORS } from '../style/colors';
import fbApp from '../utils/firebaseInit';
import { likesCounter } from '../utils/LikesCounter';
import { ConvertingObjToArr } from '../utils/ConvertingObjToArr';

export const CommentLikesAndDislikes = ({ singleComment, bookID, commentID, userID, onPressReplyIcon}) => {

    // const countOfLikes = () => {
    //     // const commentLikes = !!replyID ? comments[commentID]?.replies[replyID]?.likes : comments[commentID]?.likes;
    //     const commentLikes = singleComment?.likes;
    //     if (commentLikes) return Object.keys(commentLikes).length;
    //   };
    const countOfLikes = ConvertingObjToArr(singleComment?.likes || {}).length || "";
      const changeLike = () => {
        const reference = fbApp.db.ref(`books/${bookID}/comments/${commentID}/likes/${userID}`);
        !!singleComment?.likes
          ? !!singleComment?.likes[userID]
            ? reference.remove()
            : reference.update({
                like: true,
              })
          : reference.update({
              like: true,
            });
      };
      const changeDislike = () => {
        const reference = fbApp.db.ref(`books/${bookID}/comments/${commentID}/dislikes/${userID}`);
        !!singleComment?.dislikes
          ? !!singleComment?.dislikes[userID]
            ? reference.remove()
            : reference.update({
                dislike: true,
              })
          : reference.update({
              dislike: true,
            });
      };
  
      const onPressLikeHandler = () => {
        changeLike(commentID);
        if (!!singleComment?.dislikes)
          if (!!singleComment?.dislikes[userID]?.dislike) changeDislike(commentID);
      };
  
      const onPressDislikeHandler = () => {
        changeDislike(commentID);
        if (!!singleComment?.likes) if (!!singleComment?.likes[userID]?.like) changeLike(commentID);
      };
  
  
    return (
        <View style={styles.iconsWrapper}>
            <TouchableOpacity style = {styles.likeIcon} onPress={onPressLikeHandler}>
                <AntDesign
                    name="like1"
                    size={16}
                    color={
                    !!singleComment?.likes ? (!!singleComment?.likes[userID]?.like ? "#3883c4" : COLORS.TEXT_PRIMARY) : COLORS.TEXT_PRIMARY
                    }
                />
            <DefText style={{ marginLeft: 10 }}>{likesCounter(countOfLikes)}</DefText>
            </TouchableOpacity>
            
            <TouchableOpacity onPress={onPressDislikeHandler}>
                <AntDesign
                    name="dislike1"
                    size={16}
                    color={
                    !!singleComment?.dislikes
                    ? !!singleComment?.dislikes[userID]?.dislike
                    ? "#3883c4"
                    : COLORS.TEXT_PRIMARY
                    : COLORS.TEXT_PRIMARY
                    }
                />
            </TouchableOpacity>

            <TouchableOpacity style = {{marginLeft: 35}}  onPress = {onPressReplyIcon}>
              <MaterialIcons 
                name = "message" 
                size = {16} 
                color = {COLORS.TEXT_PRIMARY}
              />
            </TouchableOpacity>
        </View>
    )
};

const styles = StyleSheet.create({
  iconsWrapper: {
    flexDirection: "row",
    marginTop: 10,
    alignItems: "center",
  },
  likeIcon: {
    flexDirection: "row",
    width: 80,
    maxWidth: 100,
  },
})