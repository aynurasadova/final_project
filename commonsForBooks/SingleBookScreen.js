import React, { useState, useEffect } from "react";
import { StyleSheet, View, Image, ScrollView, TouchableOpacity, Dimensions, TextInput, KeyboardAvoidingView } from "react-native";
import { connect } from "react-redux";
import { LinearGradient } from "expo-linear-gradient";

import { DefText, Rating } from "../components";
import { GLOBAL_STYLES } from "../utils/GLOBAL_STYLES";
import { COLORS } from "../style/colors";
import {
  selectSingleBookFavoriteByID,
  selectSingleByID,
  selectAddedToLaterBookByID,
  selecBooksRatingByID,
  selectAddedToLaterBooks,
  selectBookComments,
  addBookComment,
  addReply,
} from "../store/books";
import fbApp from "../utils/firebaseInit";
import { selectAuthUserID } from "../store/auth";
import { BTNs } from "../commonsForBlogs/BTNs";
import { selectAppUsers } from "../store/users";
import { BookCommentPart } from "./BookCommentPart";

const mapStateToProps = (state, { route }) => ({
  users: selectAppUsers(state),
  userID: selectAuthUserID(state),
  addedToLater: selectAddedToLaterBooks(state),
  singleBook: selectSingleByID(state, route.params?.bookID),
  singleBookFavorite: selectSingleBookFavoriteByID(state, route.params?.bookID),
  singleAddedToLater: selectAddedToLaterBookByID(state, route.params?.bookID),
  singleBookRating: selecBooksRatingByID(state, route.params?.bookID),
  comments: selectBookComments(state, route.params?.bookID),
});

export const SingleBookScreen = connect(mapStateToProps, { addBookComment, addReply })(
  ({
    navigation,
    route: {
      params: { bookID },
    },
    users,
    userID,
    singleBook,
    singleBookFavorite,
    singleAddedToLater,
    singleBookRating,
    comments,
    addBookComment,
    addReply,
  }) => {
    const commentsCount = Object.keys(comments).length;
    const [isReplyModalActive, setIsReplyModalActive] = useState(false);
    const [comment, setComment] = useState("");

    const toggleAddedToLater = () => {
      const reference = fbApp.db.ref(`addedToLaterBooks/${userID}/${bookID}`);
      !!singleAddedToLater
        ? reference.remove()
        : reference.update({
            status: true,
          });
    };

    const toggleFavorite = () => {
      const reference = fbApp.db.ref(`favoritesBooks/${userID}/${bookID}`);
      !!singleBookFavorite
        ? reference.remove()
        : reference.update({
            favorite: true,
          });
    };

    return (
        <ScrollView style={styles.container}>
        <KeyboardAvoidingView behavior="padding" style={{ flex: 1 }}>
          <View style={styles.bookWrapper}>
            <LinearGradient
              colors={["rgba(0,0,0,1)", "rgba(0,0,0,0.4)", "transparent"]}
              style={{ ...StyleSheet.absoluteFill }}
            />
            <View style={styles.btnsWrapper}>
              <TouchableOpacity style={styles.back} onPress={() => navigation.goBack()}>
                <BTNs type="back" color="white" onPress={() => navigation.goBack()} />
                <DefText style={styles.backText}>Back</DefText>
              </TouchableOpacity>
              <BTNs type="bookmark" color="white" status={singleAddedToLater} onPress={toggleAddedToLater} />
            </View>
            <View style={styles.bookInfo}>
              <Image source={{ uri: singleBook?.coverImgUrl }} style={styles.cover} />
              <View style={styles.info}>
                <Rating size={18} margin={8} rate={singleBookRating?.rate} id={bookID} userID={userID} />
                <DefText weight="bold" style={styles.title}>
                  {singleBook?.title}
                </DefText>
                <DefText style={styles.writer}>By {singleBook?.author}</DefText>

                <ScrollView>
                  <DefText style={styles.descText}>{singleBook?.desc}</DefText>
                </ScrollView>
              </View>
            </View>
          </View>
          <View style={styles.btnsWrapper}>
            <TouchableOpacity
              style={styles.readNow}
              onPress={() => navigation.navigate("Pdf", { bookUrl: singleBook?.bookUrl })}
            >
              <DefText style={styles.readNowText}>Read now</DefText>
            </TouchableOpacity>
            <BTNs type="like" size={35} status={singleBookFavorite} onPress={toggleFavorite} color="#3883c4" />
          </View>

          <View style={styles.lineX} />
          <BookCommentPart bookID = {bookID} comments = {comments} bookID = {bookID} />
          </KeyboardAvoidingView>
        </ScrollView>
    );
  }
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLORS.BG_PRIMARY,
  },
  inner: {},
  bookWrapper: {
    paddingHorizontal: GLOBAL_STYLES.PADDING,
    backgroundColor: "#3883c4",
    borderBottomLeftRadius: 15,
    borderBottomRightRadius: 15,
  },
  btnsWrapper: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignSelf: "center",
    width: Dimensions.get("screen").width,
    paddingHorizontal: GLOBAL_STYLES.PADDING,
    paddingVertical: GLOBAL_STYLES.PADDING,
  },
  back: {
    flexDirection: "row",
    alignItems: "center",
  },
  backText: {
    fontSize: 18,
    marginLeft: 10,
  },
  bookInfo: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  cover: {
    width: 137,
    height: 221,
    marginBottom: 35,
    borderRadius: 10,
  },
  info: {
    width: "50%",
    paddingTop: 5,
  },
  title: {
    fontSize: 22,
    color: "white",
    marginTop: 12,
  },
  writer: {
    marginTop: 6,
    color: COLORS.TEXT_PRIMARY,
    fontSize: 16,
  },
  descText: {
    marginTop: 12,
    fontSize: 12,
    color: COLORS.TEXT_PRIMARY,
    maxHeight: 85,
  },
  readNow: {
    borderColor: "#3883c4",
    borderWidth: 1,
    paddingHorizontal: 60,
    paddingVertical: 12,
    borderRadius: 5,
  },
  readNowText: {
    color: "#3883c4",
    fontWeight: "bold",
  },
  lineX: {
    height: 5,
    borderWidth: 1,
    borderColor: "transparent",
    borderTopColor: "#808080",
  },
  popUpContainer: {
    position: "absolute",
    bottom: 0,
    left: 0,
    width: Dimensions.get("screen").width,
    backgroundColor: COLORS.BG_PRIMARY,
  },
});
