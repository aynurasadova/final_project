import React, { useEffect, useState } from "react";
import { StyleSheet, View, Image, TouchableOpacity, ActivityIndicator } from "react-native";
import { connect } from "react-redux";

import { ICONS } from "../style/icons";
import {
  selecBooksRatingByID,
  selectSingleBookFavoriteByID,
  selectTotalRatings,
} from "../store/books";
import { DefText } from "../components";
import { averageRatingCalculator } from "../utils/AverageRatingCalculator";

const mapStateToProps = (state, { book }) => ({
  singleItemFav: selectSingleBookFavoriteByID(state, book?.id),
  totalRatings: selectTotalRatings(state),
});

export const SingleBook = connect(mapStateToProps)(
  ({ book, onPress, singleItemFav, totalRatings }) => {
    const ratingCalc = averageRatingCalculator(totalRatings, book.id);
    const [isImageLoading, setIsImageLoading] = useState(false);

    return (
      <View style={styles.container}>
        {!book.empty && (
          <>
            <TouchableOpacity onPress={onPress}>
            {!isImageLoading && <ActivityIndicator style = {styles.activityIndicator} size = {24} color = "black" />}
              <Image source={{ uri: book.coverImgUrl }} style={[styles.cover, {height: isImageLoading ? 135 : 1}]} onLoadEnd = {() =>setIsImageLoading(true)}/>
            </TouchableOpacity>
            <View style={styles.wrapper}>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <DefText style={{ marginLeft: 5 }}>
                  {ratingCalc
                    ? (ratingCalc?.sum / ratingCalc?.count).toFixed(1)
                    : ""}
                </DefText>
                <Image
                  style={{
                    width: 10,
                    height: 10,
                    marginLeft: ratingCalc ? 5 : 0,
                  }}
                  source={
                    ratingCalc ? ICONS.starIconChoosen : ICONS.starIconWhite
                  }
                />
              </View>
              <Image
                source={
                  singleItemFav
                    ? ICONS.favoritesIconOnPress
                    : ICONS.favoritesIcon
                }
                style={styles.fav}
              />
            </View>
          </>
        )}
        {book.empty && (
          <View style={[styles.container, styles.itemInvisible]}></View>
        )}
      </View>
    );
  }
);

const styles = StyleSheet.create({
  container: {
    width: 87,
    marginBottom: 30,
  },
  activityIndicator: {
    width: "100%",
    height: 135,
    borderRadius: 5,
    backgroundColor: "#d4d4d4"
  },
  cover: {
    width: "100%",
    borderRadius: 5,
  },
  fav: {
    width: 11,
    height: 11,
    right: 5,
  },
  wrapper: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
  },
  itemInvisible: {
    backgroundColor: "transparent",
  },
});
