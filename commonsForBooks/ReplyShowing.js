import React from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import { DefText } from '../components';
import { connect } from 'react-redux';
import { ConvertingObjToArr } from '../utils/ConvertingObjToArr';
import { selectBookSingleCommentReplies } from '../store/books';

const mapStateToProps = (state, {commentID}) => ({
    singleBookCommentReplies: selectBookSingleCommentReplies(state, commentID)
});

export const ReplyShowing = connect(mapStateToProps)(({singleBookCommentReplies, commentID}) => {
    const replyCounter = ConvertingObjToArr(singleBookCommentReplies).length;
    const noReply = replyCounter === 0;
    const singleReply = replyCounter === 1;
    return(
        <>
        {
        !noReply &&
        <View style = {styles.replyWrapper} >
            <DefText style = {styles.replyText} weight = "medium">{!singleReply ? `${replyCounter} replies` : `View reply`}</DefText>
        </View>
        }
        </>
    )
});

const styles = StyleSheet.create({
    replyWrapper: {
        marginTop: 10,
        marginLeft: 15,
    },
    replyText: {
        fontSize: 17,
        color: "#3883c4"
    },
})