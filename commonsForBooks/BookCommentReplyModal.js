import React, { useEffect, useState } from 'react';
import { 
    StyleSheet, 
    View, 
    Dimensions,
    KeyboardAvoidingView,
    Image,
    ScrollView,
    TextInput,
    Platform,
    TouchableOpacity,
    Alert,
    Button,
} from 'react-native';
import { connect } from 'react-redux';
import { Ionicons } from '@expo/vector-icons';
import { AntDesign } from '@expo/vector-icons';

import { COLORS } from '../style/colors';
import { DefText } from '../components';
import { GLOBAL_STYLES } from '../utils/GLOBAL_STYLES';
import { selectAuthUserID, selectProfilePhoto } from '../store/auth';
import { selectAppUsers } from '../store/users';
import { UserNames } from '../commons/UserNames';
import { 
    selectBookCommentReplies, 
    selectBookSingleCommentReplies, 
    addBookCommentRelpy
} from '../store/books';
import { ConvertingObjToArr } from '../utils/ConvertingObjToArr';
import { CommentLikesAndDislikes } from './CommentLikesAndDislikes';
import {BookCommentReplyLikeAndDislike} from "./BookCommentReplyLikeAndDislike";
const dimensionOfPhoto =
  Dimensions.get("screen").width - GLOBAL_STYLES.PADDING * 2;

const mapStateToProps = (state, { commentID }) => ({
    users: selectAppUsers(state),
    userID: selectAuthUserID(state),
    profilePhoto: selectProfilePhoto(state),
    allCommentReplies: selectBookCommentReplies(state),
    singleCommentReplies: selectBookSingleCommentReplies(state, commentID)

});

export const BookCommentReplyModal = connect(mapStateToProps, {
    addBookCommentRelpy,
}
)(({
        navigation, 
        commentID,
        users,
        userID,
        profilePhoto,
        addBookCommentRelpy,
        singleCommentReplies,
        onPressGoBack,
        singleComment,
        bookID,
    }) => {

    const [commentReply, setCommentReply] = useState("");
    const submitComment = () => {
        if(commentReply.trim() !== "") {
            addBookCommentRelpy(userID, commentID, commentReply.trim());
            setCommentReply("")
        }
    };
    const deletePressHandler = (replyID) => {
        Alert.alert("Are you sure to delete comment ?", "If yes, click continue", [
            {
                text: "Cancel",
                style: "cancel",
            },
            {
                text: "Continue",
                onPress: () => {
                    fbApp.db.ref(`bookCommentsReply/${commentID}/replies/${replyID}`).remove()
                }
            }
        ])
        
    };
    const singleCommentRepliesArr = ConvertingObjToArr(singleCommentReplies);
    return(
        <View style = {styles.container}>
            <ScrollView style = {styles.inner}>
                <KeyboardAvoidingView behavior = {Platform.OS && "padding"} style = {{flex: 1}}>
                    <View style = {styles.goBack}>
                        <TouchableOpacity style = {styles.backIcon} onPress = {onPressGoBack}>
                            <AntDesign name="back" size={24} color="#bab8b8" />
                        </TouchableOpacity>
                        <DefText style = {styles.commentText}>Replies</DefText>
                    </View>

                <View style = {[styles.commentAuthor, styles.commentInfoWrapper]}>
                    <Image 
                        source = {{uri: users[singleComment.author]?.profilePhoto}}
                        style = {{width: 40, height: 40, borderRadius: 20}}
                    />
                    <View style = {styles.commentInfo}>
                        <UserNames 
                            firstName = {users[singleComment.author]?.userFirstName}
                            lastName = {users[singleComment.author]?.userLastName}
                        />
                        <DefText style = {styles.commentItself} >{singleComment.comment}</DefText>
                        <CommentLikesAndDislikes 
                            userID = {userID} 
                            singleComment = {singleComment} 
                            bookID = {bookID} 
                            commentID = {commentID} 
                        />
                    </View>
                </View>

                <View style = {styles.commentInputFieldWrapper}>
                    <View style = {{flexDirection: "row"}}>
                        <Image 
                            source = {{uri: profilePhoto}}
                            style = {{width: 40, height: 40, borderRadius: 20}}
                        />
                        <TextInput 
                            multiline = {true}
                            placeholder = "Add a reply ..."
                            placeholderTextColor = {COLORS.LINE_COLOR}
                            value = {commentReply}
                            onChangeText = {setCommentReply}
                            style = {styles.commentInputField}
                        />
                    </View>
                    <TouchableOpacity onPress = {submitComment}>
                        <Ionicons name="md-send" size={30} color={COLORS.MAIN_LIGHT} />
                    </TouchableOpacity>
                </View>
                    
                    {
                        singleCommentRepliesArr.slice(0).reverse().map(item => (
                            <View
                            style = {[styles.commentInfoWrapper, styles.replyWrapper]} key = {item.id}>
                                <View style = {styles.commentAuthor}>
                                    <Image 
                                        source = {{uri: users[item.author]?.profilePhoto}}
                                        style = {{width: 35, height: 35, borderRadius: 20}}
                                    />
                                    <View style = {styles.commentInfo}>
                                        <UserNames 
                                            firstName = {users[item.author]?.userFirstName}
                                            lastName = {users[item.author]?.userLastName}
                                        />
                                        <DefText style = {styles.commentItself} >{item.commentReplyText}</DefText>
                                        <BookCommentReplyLikeAndDislike
                                            userID = {userID} 
                                            singleReply = {item} 
                                            replyID = {item.id} 
                                            commentID = {commentID}
                                        />
                                    </View>
                                </View>
                                
                            </View>
                        ))
                    }
                </KeyboardAvoidingView>
            </ScrollView>          
        </View>
    )
});

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLORS.BG_PRIMARY,
    },
    goBack: {
        padding: GLOBAL_STYLES.PADDING / 2 + 5,
        flexDirection: "row",
    },
    backIcon: {
        paddingRight: 15
    },
    commentText: {
        fontSize: 18,
        color: "#bab8b8"
    },
    commentInputFieldWrapper: {
        flexDirection: "row",
        justifyContent: "space-between",
        paddingLeft: GLOBAL_STYLES.PADDING / 2,
        paddingRight: GLOBAL_STYLES.PADDING / 2,
        marginBottom: 20,
        marginTop: 10,
    },
    replyWrapper: {
        paddingLeft: 20, 
        borderTopWidth: 0.5, 
        borderColor: COLORS.LINE_COLOR
    },
    commentInfoWrapper: {
        paddingVertical: 15,
        paddingLeft: GLOBAL_STYLES.PADDING / 2,
        paddingRight: GLOBAL_STYLES.PADDING / 2,
    },
    commentAuthor: {
        flexDirection: "row"
    },
    commentInputField: {
        fontSize: 15,
        marginLeft: 10,
        maxWidth: Dimensions.get("screen").width - 120,
        color: COLORS.LINE_COLOR,
    },
    commentInfo: {
        marginLeft: 10,
    },
    commentItself: {
        maxWidth: Dimensions.get("screen").width - 120,
    },
});