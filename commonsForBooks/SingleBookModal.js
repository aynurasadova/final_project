import React, { useRef } from "react";
import { StyleSheet, View, Image, PanResponder, Animated } from "react-native";
import { connect } from "react-redux";

import { DefText, Rating } from "../components";
import { COLORS } from "../style/colors";
import { selecBooksRatingByID, selectTotalRatings } from "../store/books";
import { ICONS } from "../style/icons";
import { averageRatingCalculator } from "../utils/AverageRatingCalculator";

const mapStateToProps = (state, { book }) => ({
  singleItemRating: selecBooksRatingByID(state, book?.id),
  totalRatings: selectTotalRatings(state),
});

export const SingleBookModal = connect(mapStateToProps)(
  ({ book, onReleaseDown, onReleaseUp, totalRatings }) => {
    const swipeDistance = useRef(new Animated.Value(0)).current;
    const isSwipeOpen = useRef(false);

    const ratingCalc = averageRatingCalculator(totalRatings, book.id);

    const pan = useRef(
      PanResponder.create({
        onStartShouldSetPanResponderCapture: () => true,
        onMoveShouldSetPanResponderCapture: () => true,
        onPanResponderGrant: () => {
          swipeDistance.extractOffset();
          swipeDistance.setValue(0);
        },
        onPanResponderMove: (_, gestureState) => {
          const { dy } = gestureState;
          if (isSwipeOpen.current) {
            if (dy > -160 && dy < 0)
              Animated.event([{ dy: swipeDistance }])(gestureState);
          } else {
            if (dy < 160 && dy > 0)
              Animated.event([{ dy: swipeDistance }])(gestureState);
          }
        },
        onPanResponderRelease: (_, { dy }) => {
          swipeDistance.flattenOffset();
          if (isSwipeOpen.current) {
            const isPassMin = dy < -120;
            Animated.spring(swipeDistance, {
              toValue: isPassMin ? 0 : 160,
            }).start(() => {
              isSwipeOpen.current = !isPassMin;
            });
          } else {
            if (dy < -100) onReleaseUp();
            const isPassMin = dy > 120;
            if (isPassMin) onReleaseDown();
            Animated.spring(swipeDistance, {
              toValue: isPassMin ? 160 : 0,
            }).start(() => {
              isSwipeOpen.current = isPassMin;
            });
          }
        },
      })
    ).current;
    return (
      <Animated.View
        style={[
          styles.bookInfoContainer,
          {
            transform: [{ translateY: swipeDistance }],
            height: 155 - swipeDistance._value,
          },
        ]}
        {...pan.panHandlers}
      >
        <Image
          source={{ uri: book?.coverImgUrl }}
          style={styles.selectedBookCover}
        />
        <View style={styles.bookInfo}>
          <DefText style={styles.bookTitle}>{book?.title}</DefText>
          <DefText style={styles.bookWriter}>{book?.author}</DefText>
          <View style={styles.ratingWrapper}>
            <DefText style={styles.rating}>
              {ratingCalc
                ? (ratingCalc?.sum / ratingCalc?.count).toFixed(1)
                : 0}
            </DefText>
            <Image
              style={styles.star}
              source={ratingCalc ? ICONS.starIconChoosen : ICONS.starIconWhite}
            />
          </View>
        </View>
      </Animated.View>
    );
  }
);

const styles = StyleSheet.create({
  bookInfoContainer: {
    padding: 10,
    backgroundColor: COLORS.BG_SECONDARY,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    flexDirection: "row",
  },
  selectedBookCover: {
    width: 87,
    height: 135,
    borderRadius: 5,
  },
  bookInfo: {
    marginHorizontal: 20,
    width: "60%",
  },
  bookTitle: {
    color: COLORS.TEXT_PRIMARY,
    fontWeight: "bold",
    fontSize: 16,
    flexWrap: "wrap",
    marginVertical: 10,
  },
  bookWriter: {
    color: COLORS.TEXT_PRIMARY,
    fontSize: 14,
    marginBottom: 12,
  },
  ratingWrapper: {
    flexDirection: "row",
    alignItems: "center",
  },
  rating: {
    fontSize: 22,
    marginRight: 10,
  },
  star: {
    width: 22,
    height: 22,
  },
});
