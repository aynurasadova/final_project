import React, { useState } from 'react';
import { View, TouchableOpacity, TextInput, StyleSheet, Image } from 'react-native';
import { DefText } from '../components';
import { ButtonwithLabel } from '../commons';
import { ICONS } from '../style/icons';
import { GLOBAL_STYLES } from '../utils/GLOBAL_STYLES';
import { COLORS } from '../style/colors';
import { useNavigation } from '@react-navigation/native';
import { sub } from 'react-native-reanimated';
import fbApp from '../utils/firebaseInit';

export const BookUploader = () => {
    const [fields, setFields] = useState({
        name: "",
        bookLink: "",
        coverImgLink: "",
        desc: "",
        author: "",
    })

    const fieldChangeHandler = (name, value) => {
        setFields((p) => ({
            ...p,
            [name]: value,
        }))
    }

    const submit = async() => {
        try {
            const ref = fbApp.db.ref(`books`).push(); 
        
            const bookContent = {
              author: fields.author,
              bookUrl: fields.bookLink,
              coverImgUrl: imgUri,
              desc: fields.desc,
              title: fields.name,
            };
        
            ref.set(bookContent, (err) => {
              if (err) {
                console.log(err);
              }
            });
            setFields({
                name: "",
                bookLink: "",
                coverImgLink: "",
                desc: "",
                author: "",
            })
          } catch (error) {
            console.log("createBlog error", error);
          }
    }

    const navigation = useNavigation();
    return (
        <View style={styles.container}>
            <View style={styles.inner}>
            <TouchableOpacity onPress = {() => navigation.goBack()}>
                <Image style={styles.backBtn} source={ICONS.back} />
            </TouchableOpacity>
            <DefText weight="light" style={styles.headerTitle}>
                Upload Book
            </DefText>

            <DefText style={styles.inputTitles}>Book name</DefText>
            <TextInput
                style={styles.inputText}
                onChangeText ={(v) => fieldChangeHandler("name", v)}
            />

            <DefText style={styles.inputTitles}>Book link</DefText>
            <TextInput
                style={styles.inputText}
                onChangeText ={(v) => fieldChangeHandler("bookLink", v)}
            />

            <DefText style={styles.inputTitles}>Image link</DefText>
            <TextInput
                style={styles.inputText}
                onChangeText ={(v) => fieldChangeHandler("coverImgLink", v)}
            />

            <DefText style={styles.inputTitles}>Description</DefText>
            <TextInput
                style={styles.chosenDateTime}
                onChangeText ={(v) => fieldChangeHandler("desc", v)}
            >
                
            </TextInput>

            <DefText style={styles.inputTitles}>author</DefText>
            <TextInput
                style={styles.chosenDateTime}
                onChangeText ={(v) => fieldChangeHandler("author", v)}
            >
            </TextInput>
            <ButtonwithLabel title="ADD" onPress = {submit} />
            </View>
      </View>
    )
}


const styles = StyleSheet.create({
    container: {
      flex: 1,
      paddingVertical: 28,
      width: "100%",
      backgroundColor: COLORS.BG_PRIMARY,
      paddingHorizontal: 20
    },
    backBtn: {
      width: 28,
      height: 28,
      paddingBottom: 20,
    },
    headerTitle: {
      textAlign: "center",
      color: COLORS.TEXT_SECONDARY,
      fontSize: 25,
      marginBottom: 25,
    },
    chosenDateTime: {
      width: "100%",
      height: 40,
      backgroundColor: "#e4e9f2",
      borderRadius: 10,
      color: "black",
      marginVertical: 8,
      justifyContent: "center",
    },
    chosenDateTimeText: {
      color: COLORS.MAIN_DARK,
      fontSize: 14,
      marginLeft: 15,
    },
    inputTitles: {
      fontSize: 18,
      color: "#e4e9f2",
    },
    inputText: {
      width: "100%",
      height: 40,
      backgroundColor: "#e4e9f2",
      borderRadius: 10,
      color: "black",
      marginVertical: 8,
      justifyContent: "center",
      paddingLeft: 15,
      color: COLORS.BG_SECONDARY,
    },
  });
  