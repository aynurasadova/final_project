import React from "react";
import {
  StyleSheet,
  View,
  TouchableOpacity,
  TouchableNativeFeedback,
  Platform,
} from "react-native";

import { COLORS } from "../style/colors";
import { DefText } from "./DefText";

export const CustomBtn = ({ title, onPress, style, ...rest }) => {
  const Touchable =
    Platform.OS === "android" ? TouchableNativeFeedback : TouchableOpacity;
  return (
    <View style={[styles.container, style]}>
      <Touchable onPress={onPress} {...rest}>
        <View style={styles.btn}>
          <DefText weight="semi" style={styles.title} numberOfLines={1}>
            {title}
          </DefText>
        </View>
      </Touchable>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: "100%",
    borderRadius: 10,
    overflow: "hidden",
    marginVertical: 20,
  },
  btn: {
    backgroundColor: COLORS.MAIN_DARK,
    borderRadius: 10,
    height: 40,
    alignItems: "center",
    justifyContent: "center",
  },
  title: {
    color: "white",
    fontSize: 17,
    textAlign: "center",
  },
});
