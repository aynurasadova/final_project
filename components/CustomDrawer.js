import React from 'react';
import { 
    View, 
    StyleSheet,
    Image,
    TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import { Ionicons, MaterialIcons } from '@expo/vector-icons';

import { DefText } from './DefText';
import { COLORS } from '../style/colors';
import { GLOBAL_STYLES } from '../utils/GLOBAL_STYLES';
import { 
    selectProfilePhoto, 
    selectAuthUserFirstName, 
    selectAuthUserLastName,
    logOut,
} from '../store/auth';
import { UserNames } from '../commons';

const mapStateToProps = (state) => ({
    firstName: selectAuthUserFirstName(state),
    lastName: selectAuthUserLastName(state),
    profilePhoto: selectProfilePhoto(state),
});

export const CustomDrawer = connect(mapStateToProps, {logOut})(({
    firstName,
    lastName,
    profilePhoto,
    logOut,
    navigation,
}) => {
    const logOutHandler = () => {
        logOut();
        navigation.navigate("HomeStack");
    }
    return (
        <View style = {styles.container}>
            <Image 
                source = {{uri: profilePhoto}}
                style = {styles.authorPhoto}
                resizeMode = "cover"
            />
            <UserNames 
                firstName = {firstName}
                lastName = {lastName}
                fontSize = {20}
                styleOfContainer = {styles.names}
            />
            
            <View style = {styles.row}>
                <MaterialIcons name="navigate-next" size={24} color = {COLORS.LINE_COLOR} />
                <DefText weight = "light" style = {{fontSize: 17, color: COLORS.LINE_COLOR}}>Saved items</DefText>
            </View>

            <View style = {styles.inner}> 
                <TouchableOpacity onPress = {() =>navigation.navigate("ToReadAddedToLaterBooks")} style = {styles.lines}>
                    <Ionicons style = {{paddingLeft: 4, paddingRight: 10}} name="ios-book" size={28} color = {COLORS.LINE_COLOR} />
                    <DefText style = {styles.lineText} >Books</DefText>
                </TouchableOpacity>

                <TouchableOpacity onPress = {() =>navigation.navigate("ToReadAddedToLaterBlogs")} style = {styles.lines}>
                    <Ionicons style = {{paddingLeft: 4, paddingRight: 10}} name="ios-paper" size={28} color = {COLORS.LINE_COLOR} />
                    <DefText style = {styles.lineText}>Blogs</DefText>
                </TouchableOpacity>
            </View>
            <TouchableOpacity onPress = {logOutHandler} style = {styles.logOutBtn}>
                <DefText style = {styles.lineText}>Log out</DefText>
            </TouchableOpacity>
            
        </View>
    )
});

const styles = StyleSheet.create({
    container: {
        backgroundColor: COLORS.BG_PRIMARY,
        flex: 1,
    },
    authorPhoto: {
        width: 100,
        height: 100,
        borderRadius: 50,
        alignSelf: "center",
        marginTop: 25,
    },
    names: {
        marginTop: 15,
        paddingHorizontal: 50,
        justifyContent: "center",
        maxWidth: "100%", 
    },
    row: {
        flexDirection: "row",
        paddingHorizontal: GLOBAL_STYLES.PADDING / 4,
        marginTop: 15,
        alignItems: "center",
        marginBottom: 5,
    },
    inner: {
        paddingLeft: GLOBAL_STYLES.PADDING * 1.5 ,
    },
    lines: {
        flexDirection: "row",
        alignItems: "center",
        marginVertical: 7,
        paddingVertical: 5,
        width: "80%",
        borderBottomWidth: 0.2,
        borderColor: COLORS.LINE_COLOR,
    },
    lineText: {
        color: COLORS.LINE_COLOR,
        fontSize: 16,
    },
    logOutBtn: {
        alignSelf: "center",
        marginTop: 10,
        paddingHorizontal: 10,
        paddingBottom: 5
    },

})