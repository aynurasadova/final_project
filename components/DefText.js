import React from 'react';
import { Text } from 'react-native';

const fontFamilies = {
    bold: "RobotoBold",
    medium: "RobotoMedium",
    regular: "RobotoRegular",
    light: "RobotoLight"
};

export const DefText = ({weight, style, ...rest}) => {
    return (
        <Text style = {[{fontFamily: fontFamilies[weight] || fontFamilies.regular, color: "white"}, style]} {...rest}></Text>
    )
}