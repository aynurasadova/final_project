import React, { useState, useEffect } from "react";
import { View, Image, TouchableWithoutFeedback } from "react-native";

import { ICONS } from "../style/icons";
import fbApp from "../utils/firebaseInit";

export const Rating = ({
  size,
  disabled = false,
  margin = 3,
  id,
  rate = 0,
  userID,
}) => {
  const [starCount, setStarCount] = useState(rate);

  useEffect(() => {
    const reference = fbApp.db.ref(`booksRating/${userID}/${id}`);
    starCount !== 0
      ? reference.update({
          rate: starCount,
        })
      : reference.remove();

    return reference.off();
  }, [starCount]);

  let ratingBar = [];
  for (let i = 1; i <= 5; i++) {
    ratingBar.push(
      <TouchableWithoutFeedback
        key={i}
        onPress={() => {
          if (!disabled) setStarCount(i);
        }}
      >
        <Image
          source={i <= starCount ? ICONS.starIconChoosen : ICONS.starIconWhite}
          style={[
            { width: size, height: size },
            i <= 4 ? { marginRight: margin } : "",
          ]}
        />
      </TouchableWithoutFeedback>
    );
  }
  const rerender = () => {
    forceUpdate();
  };
  return (
    <View style={{ flexDirection: "row" }} onPress={rerender}>
      {ratingBar}
    </View>
  );
};
