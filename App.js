import React, { useState } from 'react';
import { StatusBar } from 'react-native';
import { AppLoading } from 'expo';
import { Provider } from 'react-redux';

import { loadFonts } from './style/fonts';
import { BottomTabNavigator } from './navigation/BottomTabNavigatior';
import store, { persistor } from './store';
import { RootNav } from './navigation/RootNav';
import { PersistGate } from 'redux-persist/integration/react';

export default function App() {
  const [loaded, setLoaded] = useState(false);

  if(!loaded) {
    return(
      <AppLoading 
        startAsync = {loadFonts}
        onFinish = {() => setLoaded(true)}
        onError = {(error) => console.log("Font loading error", error)}
      />
    )
  }

  return (
    <Provider store = {store}>
      <StatusBar hidden = {true} />
      <PersistGate loading={null} persistor={persistor}>
        <RootNav />
      </PersistGate>
    </Provider>
  );
}