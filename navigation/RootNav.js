import React from "react";
import { NavigationContainer } from "@react-navigation/native";

import { connect } from "react-redux";
import { selectAuthStatus } from "../store/auth";
import { AuthScreen } from "../screens/AuthScreen";
import { BottomTabNavigator } from "./BottomTabNavigatior";

const mapStateToProps = (state) => ({
  auth: selectAuthStatus(state),
});

export const RootNav = connect(mapStateToProps)(({ auth }) => (
  <NavigationContainer>
    {auth ? <BottomTabNavigator /> : <AuthScreen />}
  </NavigationContainer>
));
