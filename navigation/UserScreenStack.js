import React from "react";
import { createDrawerNavigator } from "@react-navigation/drawer";

import { CustomDrawer } from "../components/CustomDrawer";

import { UserScreen } from "../screens";
import { EditProfilePart } from "../screens/UserScreen/EditProfilePart";

import { UploadedBlogsScreen } from "../screens/UserScreen/UploadedBlogsScreen";
import { AllUploadedBlogsScreen } from "../screens/UserScreen/UploadedBlogsScreen/AllUploadedBlogsScreen";
import { CreateBlogScreen } from "../screens/UserScreen/UploadedBlogsScreen/CreateBlogScreen";

import { AllEventsScreen } from "../screens/UserScreen/Reminder/AllEventsScreen";
import { AddEventScreen } from "../screens/UserScreen/Reminder/AddEventScreen";

import { ToReadBooks } from "../screens/UserScreen/ToReadBooks";
import { FavoriteBooksScreen } from "../screens/UserScreen/Favorites/FavoriteBooksScreen";
import { SingleBookScreen } from "../commonsForBooks/SingleBookScreen";

import { ToReadAndWriteBlogs } from "../screens/UserScreen/ToReadAndWriteBlogs";
import { FavoriteBlogsScreen } from "../screens/UserScreen/Favorites/FavoriteBlogsScreen";
import { SingleBlogScreen } from "../commonsForBlogs/SingleBlogScreen";
import { SingleAppUserScreen } from "../commonsForAppUsers/SingleAppUserScreen";
import { FollowersScreen } from "../commonsForAppUsers/FollowersScreen";
import { FollowingsScreen } from "../commonsForAppUsers/FollowingsScreen";

const { Navigator, Screen } = createDrawerNavigator();

export const UserScreenStack = () => (
  <Navigator
    headerMode="none"
    drawerContent={(props) => <CustomDrawer {...props} />}
  >
    <Screen name="User" component={UserScreen} />
    <Screen name="EditProfile" component={EditProfilePart} />

    <Screen name="UploadedBlogsScreen" component={UploadedBlogsScreen} />
    <Screen name="AllUploadedBlogsScreen" component={AllUploadedBlogsScreen} />
    <Screen name="CreateBlogScreen" component={CreateBlogScreen} />

    <Screen name="AllEventsScreen" component={AllEventsScreen} />
    <Screen
      name="AddEventScreen"
      component={AddEventScreen}
      options={{ unmountOnBlur: true }}
    />

    <Screen name="ToReadAddedToLaterBooks" component={ToReadBooks} />
    <Screen name="FavoriteBooksScreen" component={FavoriteBooksScreen} />
    <Screen name="SingleBookScreen" component={SingleBookScreen} />

    <Screen name="ToReadAddedToLaterBlogs" component={ToReadAndWriteBlogs} />
    <Screen name="FavoriteBlogsScreen" component={FavoriteBlogsScreen} />
    <Screen name="SingleBlogScreen" component={SingleBlogScreen} />

    <Screen name="SingleAppUserScreen" component={SingleAppUserScreen} />

    <Screen name="FollowersScreen" component={FollowersScreen} />
    <Screen name="FollowingsScreen" component={FollowingsScreen} />
  </Navigator>
);
