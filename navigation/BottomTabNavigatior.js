import React from "react";
import { Image } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import { ICONS } from "../style/icons";
import { COLORS } from "../style/colors";
import { HomeScreen, BooksScreen, BlogsScreen, UserScreen } from "../screens";
import { UserScreenStack } from './UserScreenStack';
import { BlogsScreenStack } from './BlogsScreenStack';
import { BookScreenStack } from "./BookScreenStack";
import { HomeScreenStack } from "./HomeScreenStack";
import { AppUsersScreenStack } from "./AppUsersScreenStack";

const { Navigator, Screen } = createBottomTabNavigator();

export const BottomTabNavigator = () => {
  return (
      <Navigator
        initialRouteName="Home"
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
            let iconName;

            if (route.name === "HomeStack") {
              iconName = focused ? ICONS.homeIconOnPress : ICONS.homeIcon;
            } else if (route.name === "BooksStack") {
              iconName = focused ? ICONS.bookIconOnPress : ICONS.bookIcon;
            } else if (route.name === "BlogsStack") {
              iconName = focused ? ICONS.blogIconOnPress : ICONS.blogIcon;
            } else if (route.name === "UserStack") {
              iconName = focused ? ICONS.userIconOnPress : ICONS.userIcon;
            } else if (route.name === "AppUsersStack") {
              iconName = focused ? ICONS.searchIconOnPress : ICONS.searchIconWhite;
            }

            // You can return any component that you like here!
            return (
              <Image source={iconName} style={{ width: size, height: size }} />
            );
          },
        })}
        tabBarOptions={{
          showLabel: false,
          style: {
            backgroundColor: COLORS.BOTTOM_TAB_NAV_BG_COLOR,
            borderTopColor: COLORS.MAIN_DARK,
          },
        }}
      >
        <Screen name="HomeStack" component={HomeScreenStack} />
        <Screen name="BooksStack" component={BookScreenStack} />
        <Screen name="BlogsStack" component={BlogsScreenStack} />
        <Screen name="UserStack" component={UserScreenStack} />
        <Screen name = "AppUsersStack" component = {AppUsersScreenStack} />
      </Navigator>
  );
};
