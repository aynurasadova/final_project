import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import { HomeScreen } from "../screens";
import { BookUploader } from "../bookUploader";
import { SingleBlogScreen } from "../commonsForBlogs/SingleBlogScreen";
import { SingleBookScreen } from "../commonsForBooks/SingleBookScreen";

const { Navigator, Screen } = createStackNavigator();

export const HomeScreenStack = () => (
  <Navigator headerMode="none" initialRouteName="HomeScreen">
    <Screen name="HomeScreen" component={HomeScreen} />
    <Screen name="SingleBookScreen" component={SingleBookScreen} />
    <Screen name="SingleBlogScreen" component={SingleBlogScreen} />
    <Screen name="BookUploading" component={BookUploader} />
  </Navigator>
);
