import React from 'react';
import { createStackNavigator } from "@react-navigation/stack";
import { AppUsers } from '../screens/AppUsers';
import { SingleAppUserScreen } from '../commonsForAppUsers/SingleAppUserScreen';
import { SingleBlogScreen } from '../commonsForBlogs/SingleBlogScreen';

const { Navigator, Screen } = createStackNavigator();

export const AppUsersScreenStack = () => (
    <Navigator headerMode = "none">
        <Screen name = "AllAppUsers" component = {AppUsers} />
        <Screen name = "SingleAppUserScreen" component = {SingleAppUserScreen} />
        <Screen name = "SingleBlogScreen" component = {SingleBlogScreen} />
    </Navigator>
)