import React from 'react';
import { createStackNavigator } from "@react-navigation/stack";
import { BlogsScreen } from '../screens';
import { SingleBlogScreen } from '../commonsForBlogs/SingleBlogScreen';

const { Navigator, Screen } = createStackNavigator();

export const BlogsScreenStack = () => (
    <Navigator headerMode = "none">
        <Screen name = "Blogs" component = {BlogsScreen} />
        <Screen name = "SingleBlogScreen" component = {SingleBlogScreen} />
    </Navigator>
)