import React from "react";
import { createStackNavigator, TransitionSpecs } from "@react-navigation/stack";

import { BooksScreen } from "../screens";
import { timing } from "react-native-reanimated";
import { PdfScreen } from "../screens/PdfScreen";
import { SingleBookScreen } from "../commonsForBooks/SingleBookScreen";

const { Navigator, Screen } = createStackNavigator();

export const BookScreenStack = () => (
  <Navigator headerMode="none" mode="modal">
    <Screen name="Books" component={BooksScreen} />
    <Screen
      name="SingleBookScreen"
      component={SingleBookScreen}
      options={{
        transitionSpec: {
          open: config,
          close: config,
        },
      }}
    />
    <Screen name="Pdf" component={PdfScreen} />
  </Navigator>
);

const config = {
  animation: "timing",
  config: {
    duration: 400,
  },
};
