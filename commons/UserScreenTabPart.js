import React from "react";
import { View, TouchableOpacity, Image, StyleSheet } from "react-native";
import { Ionicons } from '@expo/vector-icons';

import { DefText } from "../components";
import { ICONS } from "../style/icons";
import { COLORS } from "../style/colors";

export const UserScreenTabPart = ({
  headerText,
  onPressIcon,
  iconTitle,
  iconName,
  children,
  ionicon,
}) => {
  return (
    //This is used in four parts
    //UserScreen/Favorites, Reminder, ToReadAndWriteBlogs, ToReadBooks

    //You can provide Header Text for each tabBaR
    //Children will be :
    //books(That was added to add to later),
    //blogs(That was added to add to later),
    //reminder (that user sets),
    //and favorites - books/blogs
    //iconName must be the same as in style/icon.js
    <View style={styles.container}>
      <DefText style={styles.msgTextStyle}>{headerText}</DefText>
      {children}
      <TouchableOpacity onPress={onPressIcon} style={styles.pressedIcon}>
        <DefText style={styles.btnText}>{iconTitle}</DefText>
        {
          ionicon 
            ? 
            <Ionicons name = {`${iconName}`} size={78} color = {COLORS.MAIN_LIGHT} style = {styles.iconStyle} /> 
            :
            <Image source={ICONS[iconName]} style={styles.btnImg} />
        }
        
        
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingVertical: 15,
        paddingHorizontal: 25,
    },
    msgTextStyle: {
        fontSize: 25,
        color: COLORS.TEXT_SECONDARY,
        marginBottom: 10,
    },
    pressedIcon: {
        position: "absolute",
        top: "75%",
        left: "46%",
        alignItems: "center",
        justifyContent: "center"
    },
    btnText: {
        fontSize: 13,
        color: COLORS.TEXT_SECONDARY,
        marginBottom: 10,
    },
    btnImg: {
        width: 78,
        height: 78,
    },
});
