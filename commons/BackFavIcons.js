import React, { useState } from "react";
import { StyleSheet, View, TouchableOpacity, Image } from "react-native";

import { ICONS } from "../style/icons";

export const BackFavIcons = ({ onPressBack, onPressFav, favStatus }) => {
  return (
    <View style={styles.container}>
      <View style={styles.iconsRow}>
        <TouchableOpacity
          style={[styles.iconWrapper, { left: 20 }]}
          onPress={onPressBack}
        >
          <Image source={ICONS.backArrow} style={styles.icon} />
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.iconWrapper, { right: 20 }]}
          onPress={onPressFav}
        >
          <Image
            source={favStatus ? ICONS.heartIconChoosen : ICONS.heartIconGray}
            style={styles.icon}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    position: "absolute",
    zIndex: 1,
    width: "100%",
    marginTop: 30,
  },
  iconsRow: {
    justifyContent: "space-between",
    flexDirection: "row",
  },
  iconWrapper: {
    width: 56,
    height: 56,
    backgroundColor: "#fff",
    borderRadius: 30,
    justifyContent: "center",
    alignItems: "center",
  },
  icon: {
    width: "50%",
    height: "50%",
  },
});
