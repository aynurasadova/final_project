import React from 'react';
import { StyleSheet, TextInput, View, TouchableOpacity } from 'react-native';

import { DefText } from '../components';
import { COLORS } from '../style/colors';

export const ButtonwithLabel = ({label, title, onPress, btnColor, width, ...rest}) => {
    return(
        <View style = {{width:  width || "100%", marginTop: 20}}>
            <TouchableOpacity 
                style = {[styles.btn, {backgroundColor: btnColor || COLORS.MAIN_LIGHT}]} 
                onPress = {onPress} 
                {...rest}
            >
                <DefText style = {styles.titleTxt}>{title}</DefText>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    label: {
        fontSize: 18,
        marginVertical: 10,
        color: COLORS.TEXT_PRIMARY,
    },
    field: {
        borderRadius: 10,
        backgroundColor: "white",
        paddingHorizontal: 20
    },
    btn: {
        justifyContent: "center", 
        alignItems: "center",
        height: 40,
        borderRadius: 10,
    },
    titleTxt: {
        fontSize: 18,
    },
});
