import React, { useState } from "react";
import { StyleSheet, TextInput, View } from "react-native";

import { DefText } from "../components";
import { COLORS } from "../style/colors";

export const InputField = ({ placeholderText, maxHeight, ...rest }) => {
  const [height, setHeight] = useState(null);
  const setMaxHeight = height < maxHeight;
  return (
    <View style={styles.creationForm}>
      <TextInput
        multiline={true}
        style={[
          styles.field,
          { height: Math.max(40, setMaxHeight ? height : maxHeight) },
        ]}
        placeholder = {placeholderText}
        placeholderTextColor = {COLORS.LINE_COLOR}
        onContentSizeChange={(event) =>
          setHeight(event.nativeEvent.contentSize.height)
        }
        {...rest}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  creationForm: {
    width: "100%",
  },
  label: {
    fontSize: 18,
    color: COLORS.TEXT_PRIMARY,
  },
  field: {
    borderWidth: 0.8,
    color: COLORS.LINE_COLOR,
    borderColor: "transparent",
    borderBottomColor: COLORS.LINE_COLOR,
    paddingHorizontal: 15,
    fontFamily: "RobotoRegular",
    fontSize: 16,
    height: 40,
    marginVertical: 20,
  },
});
