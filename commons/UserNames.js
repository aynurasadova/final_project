import React from "react";
import { StyleSheet, View, Dimensions } from "react-native";
import { DefText } from "../components";
import { GLOBAL_STYLES } from "../utils/GLOBAL_STYLES";
import { COLORS } from "../style/colors";

export const UserNames = ({
  firstName,
  lastName,
  spaceFromLeft,
  fontSize,
  authorNameColor,
  weight,
  styleOfContainer,
}) => {
  return (
    <View
      style={[
        styles.userNames,
        { marginLeft: spaceFromLeft },
        { ...styleOfContainer },
      ]}
    >
      <DefText
        weight={weight}
        numberOfLines={1}
        style={{
          fontSize: fontSize,
          color: authorNameColor || COLORS.MAIN_DARK,
        }}
      >
        {firstName}
      </DefText>
      <DefText
        weight={weight}
        numberOfLines={1}
        style={{
          fontSize: fontSize,
          color: authorNameColor || COLORS.MAIN_DARK,
          paddingLeft: 5,
        }}
      >
        {lastName}
      </DefText>
    </View>
  );
};

const styles = StyleSheet.create({
    userNames: {
        flexDirection: "row",
        maxWidth: Dimensions.get("screen").width - GLOBAL_STYLES.PADDING * 3 - 75,
      },
      profileImg: {
        width: 75,
        height: 75,
        borderRadius: 50,
      },
});
